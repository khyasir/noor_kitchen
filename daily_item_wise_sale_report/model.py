#-*- coding:utf-8 -*-
########################################################################################
########################################################################################
##                                                                                    ##
##    OpenERP, Open Source Management Solution                                        ##
##    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved       ##
##                                                                                    ##
##    This program is free software: you can redistribute it and/or modify            ##
##    it under the terms of the GNU Affero General Public License as published by     ##
##    the Free Software Foundation, either version 3 of the License, or               ##
##    (at your option) any later version.                                             ##
##                                                                                    ##
##    This program is distributed in the hope that it will be useful,                 ##
##    but WITHOUT ANY WARRANTY; without even the implied warranty of                  ##
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   ##
##    GNU Affero General Public License for more details.                             ##
##                                                                                    ##
##    You should have received a copy of the GNU Affero General Public License        ##
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.           ##
##                                                                                    ##
########################################################################################
########################################################################################

from openerp import models, fields, api
from datetime import date
import re
import pandas as pd
import numpy as np
import psycopg2 as pg
import pandas.io.sql as psql
import getpass
from datetime import datetime, date, timedelta


class daily_item_wise_sale_report(models.AbstractModel):
    _name = 'report.daily_item_wise_sale_report.forward_report'


    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        form = docs.form
        to = docs.to
        entity = docs.entity
           
        company = self.env['res.company'].search([])

        
        # *******Establishing connection with DB START************
        user_name = getpass.getuser()       #getting current logged in user
        database_name = self._cr.dbname     #getting current db
        connection = pg.connect(database=database_name, user=user_name) #Connection
        sale_line = pd.read_sql_query('select * from "sale_order_line"', con=connection)
        sale_form = pd.read_sql_query('select * from "sale_order"', con=connection)
        prod_categ = pd.read_sql_query('select * from "pos_category_ecube"', con=connection)
        sale_form_df_summary = sale_form[['so_state','id','effective_date','psc_entity']]
        sale_form_df_summary = sale_form_df_summary.rename(columns={'id':'sale_form_id'})
        sale_df_merg = pd.merge(sale_line,sale_form_df_summary, how='left' , left_on='order_id', right_on="sale_form_id")

        f_sale = sale_df_merg.loc[(sale_df_merg['effective_date'] >= form) & (sale_df_merg['effective_date'] <= to) & (sale_df_merg['psc_entity']  == entity.id) & (sale_df_merg['so_state']  == 'done')]

        gr_prod_wise = f_sale.groupby(['product_id'],as_index = False).sum()
        # print prod_categ
        print gr_prod_wise

        main_data_list = []
        for index, cat in prod_categ.iterrows():
          categ = self.env['pos.category.ecube'].search([('id','=',cat['id'])])
          main_data = []
          for index, row in gr_prod_wise.iterrows():
            prod = self.env['product.product'].search([('id','=',row['product_id']),('pos_categ','=',cat['id'])])
            if prod.name:
              main_data.append({          
                'product':prod.name,
                'qty':row['product_uom_qty'],
                'price':prod.lst_price,
                'price_total':row['price_total'],
                'tax':row['price_total'] - row['price_subtotal'],
                'sub_total':row['price_subtotal'],
                })
            else:
              prod = self.env['product.product'].search([('id','=',row['product_id']),('pos_categ','=',cat['id']),('active','=',False)])
              if prod.name:
                main_data.append({          
                  'product':prod.name,
                  'qty':row['product_uom_qty'],
                  'price':prod.lst_price,
                  'price_total':row['price_total'],
                  'tax':row['price_total'] - row['price_subtotal'],
                  'sub_total':row['price_subtotal'],
                  })


          main_data_list.append({          
            'category':categ.name,
            'main_data':main_data,
            })

        # main_data_list = main_data_list.sort(key=lambda r: r.no)

        docargs = {
           'doc_ids': self.ids,
           'doc_model': self.model,
           'docs': docs,
           'form': form,
           'to': to,
           'entity': entity,
           'company': company,
           'main_data_list': main_data_list,
        }
        
        return self.env['report'].render('daily_item_wise_sale_report.forward_report', docargs)