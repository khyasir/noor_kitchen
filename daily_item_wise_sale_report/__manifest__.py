{
	'name': 'Daily Item Wise Sale Report', 

	'description': 'Daily Item Wise Sale Report', 
	
	'author': 'Rana Rizwan',
	
	'depends': ['base', 'report','psc_entity'], 
	
	'data': [
        'template.xml',
        'views/module_report.xml',
    ],
}