# -*- coding: utf-8 -*-

{
    'name': "Salary Sheet 1",

    'summary': "Salary Sheet 1",

    'description': "Salary Sheet 1",

    'author': "Muhammmad Kamran",
    'website': "http://www.bcube.com",

    # any module necessary for this one to work correctly
    'depends': ['base', 'report','account','hr','hr_payroll'],
    # always loaded
    'data': [
        'template.xml',
        'views/module_report.xml',
    ],
    'css': ['static/src/css/report.css'],
}