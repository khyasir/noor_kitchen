# #-*- coding:utf-8 -*-
####################################################################################
####################################################################################
##                                                                                ##
##    OpenERP, Open Source Management Solution                                    ##
##    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved   ##
##                                                                                ##
##    This program is free software: you can redistribute it and/or modify        ##
##    it under the terms of the GNU Affero General Public License as published by ##
##    the Free Software Foundation, either version 3 of the License, or           ##
##    (at your option) any later version.                                         ##
##                                                                                ##
##    This program is distributed in the hope that it will be useful,             ##
##    but WITHOUT ANY WARRANTY; without even the implied warranty of              ##
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               ##
##    GNU Affero General Public License for more details.                         ##
##                                                                                ##
##    You should have received a copy of the GNU Affero General Public License    ##
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.       ##
##                                                                                ##
####################################################################################
####################################################################################

from openerp import models, fields, api

class GenerateSalarySheet1(models.TransientModel):
	_name = "ecube.salary.sheet1"

	form = fields.Date("Start Date")
	to = fields.Date("End Date")
	resigned = fields.Boolean(default = True)

	tree_link = fields.Many2many('hr.payslip')
	batch = fields.Many2one('hr.payslip.run',string="Pay Slip Batch")

	@api.onchange('batch')
	def onchange_batch(self):
		if self.batch:
			payslips = []
			for x in self.batch.slip_lines:
				payslips.append(x.id)

			self.tree_link = payslips
			self.form = self.batch.date_start
			self.to = self.batch.date_end

	@api.multi
	def generate_report(self):
		data = {}
		data['form'] = self.read(['form','to','tree_link','batch'])[0]
		return self._print_report(data)

	def _print_report(self, data):
		data['form'].update(self.read(['form','to','tree_link','batch'])[0])
		return self.env['report'].get_action(self, 'salary_sheet_1.salary_sheet', data=data)

class SalaryRulesCategories(models.Model):
	_inherit = 'hr.salary.rule.category'

	sequance = fields.Char(string="Sequence")