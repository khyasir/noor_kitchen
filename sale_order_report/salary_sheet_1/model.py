#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api
from datetime import date
import re
import pandas as pd
import numpy as np
import psycopg2 as pg
import pandas.io.sql as psql
import getpass
import operator


class SampleDevelopmentReport(models.AbstractModel):
    _name = 'report.salary_sheet_1.salary_sheet'

    @api.model
    def render_html(self,docids, data=None):

        self.model = self.env.context.get('active_model')
        record_wizard = self.env[self.model].browse(self.env.context.get('active_id'))

        user_name = getpass.getuser()
        database_name = self._cr.dbname
        connection = pg.connect(database=database_name, user=user_name)

        if record_wizard.batch:
            records = record_wizard.batch.slip_lines
        else:
            records = record_wizard.tree_link

        to = record_wizard.to
        form = record_wizard.form
        resigned = record_wizard.resigned

        salary_rules = self.env['hr.salary.rule'].search([('active','=',True)])
        salary_categs = self.env['hr.salary.rule.category'].search([])

        if not resigned:
            slip_ids = []
            for ids in records:
                if not ids.employee_id.resigned:
                    slip_ids.append(ids.id)

        if resigned:
            slip_ids = []
            for ids in records:
                # if ids.employee_id.resigned:
                slip_ids.append(ids.id)


        pay_slip = pd.read_sql_query('select * from "hr_payslip"',con=connection)
        pay_slip = pay_slip[pay_slip['id'].isin(slip_ids)]

        pay_slip_line = pd.read_sql_query('select * from "hr_payslip_line"',con=connection)
        pay_slip_line = pay_slip_line[pay_slip_line['slip_id'].isin(slip_ids)]

        
        pay_slip_line = pd.read_sql_query('select * from "hr_payslip_line"',con=connection)
        hr_department = pd.read_sql_query('select * from "psc_entity"',con=connection)
        hr_employee = pd.read_sql_query('select * from "hr_employee"',con=connection)


        hr_employee = hr_employee[['id','name_related','entity']]
        hr_employee = hr_employee.rename(columns={'name_related': 'Employee Name','id': 'Employee ID'})
        hr_department = hr_department[['id','name']]
        hr_department = hr_department.rename(columns={'name': 'DepartmentName'})
        employee_department = pd.merge(hr_employee,hr_department,  how='left', left_on='entity', right_on ='id')
        print employee_department
        pay_slip_line = pay_slip_line[['code','category_id','total','slip_id']]
        pay_slip_line = pay_slip_line[pay_slip_line['slip_id'].isin(slip_ids)]
        pay_slip = pay_slip[['id','date_to','date_from','employee_id']]
        pay_slip = pay_slip[pay_slip['id'].isin(slip_ids)]
        final_frame = pd.merge(pay_slip, pay_slip_line,  how='left', left_on='id', right_on ='slip_id')
        final_frame_employee = pd.merge(final_frame, employee_department,  how='left', left_on='employee_id', right_on ='Employee ID')
        final_frame_employee = final_frame_employee[final_frame_employee['slip_id'].isin([1107,1108,1109,1110,1111,1112])]
        final_frame_employee["total"]= final_frame_employee["total"].astype(str)
        final_frame_employees = final_frame_employee.groupby('slip_id')['total'].apply('/'.join).reset_index()


        group_by_final = pd.merge(pay_slip, final_frame_employees,  how='left', left_on='id', right_on ='slip_id')
        # print group_by_final

        unique_departments = final_frame_employee.DepartmentName.unique()
        unique_cat = final_frame.category_id.unique()
        unique_rules = final_frame.code.unique()

        unique_cat_rec = []
        for sor in unique_cat:
            value = str(sor)
            if value != "nan":
                category_new = self.env['hr.salary.rule.category'].search([('id','=',sor)])
                unique_cat_rec.append(category_new)
        unique_cat_rec = sorted(unique_cat_rec, key=lambda x: x.sequance)

        main_data = []
        for rec in unique_cat_rec:
            rule_dict = []
            for rl in unique_rules:
                rule_name = self.env['hr.salary.rule'].search([('code','=',rl),('category_id','=',rec.id)])
                if rule_name:
                    rule_dict.append({
                        'rule_name':rule_name.name,
                        'rule_code':rule_name.code,
                        'rule_seq':rule_name.sequence,
                        })
            sorted_d = sorted(rule_dict.items(), key=operator.itemgetter(2))
            size = len(sorted_d)
            main_data.append({
                'category':rec.name,
                'rules': sorted_d,
                'size': size,
                })

        rules = []
        for lst in main_data:
            for key, value in lst.iteritems():
                if key == "rules":
                    for rl in value:
                        for keys,values in rl.iteritems():
                            if keys == 'rule_code':
                                rules.append(values)




        pay_slip = pd.read_sql_query('select * from "hr_payslip"',con=connection)
        pay_slip = pay_slip[pay_slip['id'].isin(slip_ids)]

        unique_departments = pd.merge(pay_slip, employee_department,  how='left', left_on='employee_id', right_on ='Employee ID')
        unique_departments = unique_departments.entity.unique()
        print unique_departments

        pay_slip_line = pd.read_sql_query('select * from "hr_payslip_line"',con=connection)
        pay_slip_line = pay_slip_line[pay_slip_line['slip_id'].isin(slip_ids)]


        pay_slip_line['coded'] = pd.Categorical(
                            pay_slip_line['code'], 
                            categories=rules, 
                            ordered=True
                        )
        payslip_line = pay_slip_line.sort_values(by='coded')


        payslip_line["total"]= payslip_line["total"].astype(str)
        payslip_line = payslip_line.groupby('slip_id')['total'].apply('/'.join).reset_index()
        group_by_finals = pd.merge(pay_slip, payslip_line,  how='left', left_on='id', right_on ='slip_id')

        # print group_by_finals
        dep_wise = []
        grand_total = []
        for dep in unique_departments:
            final_list = []
            sum_total_list = []
            for index, row in group_by_finals.iterrows():
                employee_ids = self.env['hr.payslip'].search([('id','=',row['slip_id'])])
                if employee_ids.employee_id.entity.id == dep:
                    sig = ' '
                    if employee_ids.payment_journal.type == 'bank':
                        sig = employee_ids.payment_journal.name 
                    totals = row['total']
                    totals_list = totals.split("/")
                    totals_list_float = [float(i) for i in totals_list]
                    sum_total_list.append(totals_list_float)
                    final_list.append({
                        'employee_id':employee_ids.employee_id.emp_code,
                        'employee_name':employee_ids.employee_id.name,
                        # 'employee_cnic':employee_ids.employee_id.cnic,
                        'employee_job':employee_ids.employee_id.job_id.name,
                        'employee_slip':employee_ids.number,
                        'employee_sig':sig,
                        'totals':totals_list
                        })
        
            summing = [sum(x) for x in zip(*sum_total_list)]

            depts_ids = self.env['psc.entity'].search([('id','=',dep)])

            dep_wise.append({
                'department':depts_ids.name,
                'final_list':final_list,
                'dep_total':summing

            })

            grand_total.append(summing)


            summing = []

        grand_total_summing = [sum(x) for x in zip(*grand_total)]

           



        # print payslip_line


        # payslip_data = []
        # for x in unique_departments:
        #     department_wise = final_frame_employee.loc[(final_frame_employee['DepartmentName'] == x)]
        #     # print department_wise

        #     unique_emp_department = department_wise.employee_id.unique()
        #     salary_data = []
        #     for emp in unique_emp_department:
        #         emp_data = final_frame_employee.loc[(final_frame_employee['employee_id'] == emp)]
                
        #         # for main in main_data:
        #         salary_amounts = []
        #         for rul in rules:

        #             emp_data_amount = emp_data.loc[(emp_data['code'] == rul)]
        #             salary_amounts.append(emp_data_amount['total'])

        #         salary_data.append({
        #             'name':emp,
        #             'amounts':salary_amounts,
        #             })
            
        #     payslip_data.append({
        #         'department':x,
        #         'salary_data':salary_data,
        #         })
        #     print payslip_data


        # print main_data


        line_columns = list(pay_slip_line.columns.values)
        # print line_columns
        # print pay_slip
        # print final_frame
        


        categs = []

        for x in salary_categs:
            for y in records:
                for z in y.line_ids:
                    if z.category_id.id == x.id and x not in categs:
                        categs.append(x)
        categs = sorted(categs, key=lambda x: x.sequance)

        rules = []
        rule_name = []
        for x in records:
            for y in x.line_ids:
                if y.name not in rule_name:
                    rule_name.append(y.name)
                    rules.append(y)

        allowance = []
        deduction = []
        advances  = []

        for x in salary_rules:
            if x.category_id.name == 'Allowance':
                allowance.append(x)

        for x in salary_rules:
            if x.category_id.name == 'Deduction':
                deduction.append(x)

        for x in salary_rules:
            if x.category_id.name == 'Advances To Employee ':
                advances.append(x)

        departments = []
        for x in records:
            if x.employee_id.entity not in departments:
                departments.append(x.employee_id.entity)


        if not resigned:


            datad = []
            for x in departments:
                emp = []
                employee = []
                for y in records:
                    sig = " "
                    if y.employee_id.entity.id == x.id and not y.employee_id.resigned:
                        employee.append(y)
                        if y.payment_journal.type == 'bank':
                            sig = y.payment_journal.name
                        emp.append({
                            'card':y.employee_id.emp_code,
                            'name':y.employee_id.name,
                            'cnic':y.employee_id.identification_id,
                            'job':y.employee_id.job_id.name,
                            'sig':sig,
                            'number':y.number,
                            'ids':y,
                            })


                datad.append({
                    'department':x.name,
                    'employee':emp,
                    })



            # employee = []
            # def collect_records(depart):
            #     del employee[:]
            #     for x in records:
            #         if x.employee_id.entity == depart and not x.employee_id.resigned:
            #             employee.append(x)

            


        if resigned:

            employee = []
            def collect_records(depart):
                del employee[:]
                for x in records:
                    if x.employee_id.entity == depart and x.employee_id.resigned:
                        employee.append(x)




        def depart_totale(rule):
            amount = 0
            for x in employee:
                for y in x.details_by_salary_rule_category:
                    if y.code == rule:
                        if y.amount:
                            amount = amount + y.amount
            return amount
            
        def get_amount(emp,rule):
            amount = 0
            for x in records:
                if x == emp:
                    for y in x.details_by_salary_rule_category:
                        if y.code == rule:
                            if y.amount:
                                amount = y.amount
            return amount
            
        def get_payslip(emp):
            for x in records:
                if x == emp:
                    return x.number
            
        def sig(emp):
            for x in records:
                if x == emp:
                    if x.payment_journal.type == 'bank':
                        return x.payment_journal.name

        if not resigned:

            def totaled(rule):
                amount = 0
                for x in records:
                    if not x.employee_id.resigned:
                        for y in x.details_by_salary_rule_category:
                            if y.code == rule:
                                if y.amount:
                                    amount = amount + y.amount
                return amount

        if resigned:
            def totaled(rule):
                amount = 0
                for x in records:
                    if x.employee_id.resigned:
                        for y in x.details_by_salary_rule_category:
                            if y.code == rule:
                                if y.amount:
                                    amount = amount + y.amount
                return amount


        def date_getter():
            month = int(form[5:7])
            months_in_words = {
             1:'January',
             2:'February',
             3:'March',
             4:'April',
             5:'May',
             6:'June',
             7:'July',
             8:'August',
             9:'September',
            10:'October',
            11:'November',
            12:'December',
            }

            month = months_in_words[month]
            return month

        def GetCategRules(attr):
            allRules = []
            allRulesName = []

            for x in records:
                for y in x.line_ids:
                    if y.category_id.id == attr.id and y.name not in allRulesName:
                        allRules.append(y)
                        allRulesName.append(y.name)

            allRules = sorted(allRules, key=lambda x: x.sequence)
            return allRules

        def GetCategSize(attr):
            lists = []
            lists_name = []


            for x in records:
                for y in x.line_ids:
                    if y.category_id.id == attr and y.name not in lists_name:
                        lists.append(y)
                        lists_name.append(y.name)

            return len(lists)

        docargs   = {
            'doc_ids': docids,
            'doc_model': 'hr.payslip',
            'docs': departments,
            'data': data,
            'allowance': allowance,
            'deduction': deduction,
            'advances': advances,
            'get_amount': get_amount,
            'date_getter': date_getter,
            'totaled': totaled,
            'depart_totale': depart_totale,
            'rules': rules,
            'salary_categs': salary_categs,
            'GetCategRules': GetCategRules,
            'GetCategSize': GetCategSize,
            'categs': categs,
            'get_payslip': get_payslip,
            'sig': sig,
            'main_data': main_data,
            'final_list': final_list,
            'dep_wise': dep_wise,
            'grand_total_summing': grand_total_summing,
        }

        return self.env['report'].render('salary_sheet_1.salary_sheet', docargs)