#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api
from num2words import num2words
from datetime import timedelta,datetime,date
from dateutil.relativedelta import relativedelta
try:
	import qrcode
except ImportError as ex:
	print str(ex)
import base64
import io

class sale_order_report(models.AbstractModel):
	_name = 'report.sale_order_report.demand_report_report'

	@api.model
	def render_html(self,docids, data=None):
		report_obj = self.env['report']
		report = report_obj._get_report_from_name('sale_order_report.demand_report_report')
		records = self.env['sale.order'].browse(docids)
		byte_img =False

		# if records.fbr_invoice_number:
		# 	qr = qrcode.QRCode(
		# 		version=2,
		# 		error_correction=qrcode.constants.ERROR_CORRECT_L,
		# 		box_size=10,
		# 		border=4,
		# 	)
		# 	qr.add_data(str(records.fbr_invoice_number))
		# 	qr.make(fit=True)

		# 	img = qr.make_image(fill_color="black", back_color="white")
			
		# 	new_img = img.resize((96, 96))  # x, y
		# 	buf = io.BytesIO()
		# 	new_img.save(buf, format='JPEG')
		# 	byte_im = buf.getvalue()
			
		# 	byte_img= base64.b64encode(byte_im)

		if records.order_time:
			print "fffffffffffffffffffff"
			start_date = datetime.strptime(records.order_time,"%Y-%m-%d %H:%M:%S")
			print_date = start_date + relativedelta(hours=5)
			print_date = str(print_date)
		else:
			print "ffffff"
			print records
			start_date = datetime.strptime(records.date_order,"%Y-%m-%d %H:%M:%S")
			print_date = start_date + relativedelta(hours=5)
			print_date = str(print_date)



		order_lines = []

		for x in records:
			for y in x.order_line:
				if y.product_uom_qty > 0:
					order_lines.append(y)

		prods = []
		for rec in order_lines:
			if rec.product_id not in prods:
				prods.append(rec.product_id)

		prod_detail = [] 
		for pro in prods:
			order_rec = self.env['sale.order.line'].search([('order_id.id','=',records.id),('product_id','=',pro.id)])

			qty = 0
			amt = 0
			rate = 0
			for com in order_rec:
				qty = qty + com.product_uom_qty
				amt = amt + com.price_subtotal
				rate = com.price_unit

			prod_deal = [] 
			if pro.is_deal and pro.deal_id:
				for deal in pro.deal_id:
					if deal.product_id.drinks == False:
						prod_deal.append({
							'name':deal.product_id.name,
							'qty':qty*deal.quantity,
							})

######################(Unique Product sy Merge kar sakty he  (For Future k lia))###############
				for odl in order_lines:
					if pro.id == odl.product_id.id:
						if odl.drink_detail:
							for dri in odl.drink_detail:
								prod_deal.append({
									'name':dri.product_id.name,
									'qty':dri.qty,
								})
				# drinks_tree = self.env['pos.drinks.tree'].search([('product_id.id','=',pro.id),('drinks_tree.id','=',records.id)])
				# if drinks_tree:
				#     for dri in drinks_tree:
				#         prod_deal.append({
				#             'name':dri.drinks_id.name,
				#             'qty':dri.qty,
				#             })



			prod_detail.append({
				'prod':pro.name,
				'qty':qty,
				'amt':amt,
				'rate':rate,
				'prod_deal':prod_deal,

				})

		tax_name = ''
		for x in records.order_line:
			if x.tax_id:
				for y in x.tax_id:
					tax_name = y.name 
					break  
			break  

		company = self.env['res.company'].search([])

		docargs = {
			'doc_ids': docids,
			'doc_model': 'sale.order',
			'docs': records,
			'doc': prod_detail,
			'qr_img': byte_img,
			'company': company,
			'print_date': print_date,
			'tax_name': tax_name,
			}

		for x in records:
			x.inv_time = datetime.now()
			x.so_state = 'invoiced'
			x.allowed = True
			x.inv_button = True
			x.clone_sync_bool = False
			x.sale_counter = x.sale_counter + 1
			for y in x.order_line:
				y.invoice_check = True

		return report_obj.render('sale_order_report.demand_report_report', docargs)