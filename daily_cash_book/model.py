#-*- coding:utf-8 -*-
########################################################################################
########################################################################################
##                                                                                    ##
##    OpenERP, Open Source Management Solution                                        ##
##    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved       ##
##                                                                                    ##
##    This program is free software: you can redistribute it and/or modify            ##
##    it under the terms of the GNU Affero General Public License as published by     ##
##    the Free Software Foundation, either version 3 of the License, or               ##
##    (at your option) any later version.                                             ##
##                                                                                    ##
##    This program is distributed in the hope that it will be useful,                 ##
##    but WITHOUT ANY WARRANTY; without even the implied warranty of                  ##
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   ##
##    GNU Affero General Public License for more details.                             ##
##                                                                                    ##
##    You should have received a copy of the GNU Affero General Public License        ##
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.           ##
##                                                                                    ##
########################################################################################
########################################################################################

from odoo import models, fields, api
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.exceptions import Warning

class PartnerLedgerReport(models.AbstractModel):
    _name = 'report.daily_cash_book.genral_ledger_report'

    @api.model
    def render_html(self,docids, data=None):

        report_obj = self.env['report']
        report = report_obj._get_report_from_name('daily_cash_book.genral_ledger_report')
        active_wizard = self.env['genral.ledger.daily'].search([])
        records = self.env['account.account'].browse(docids)
        emp_list = []
        for x in active_wizard:
            emp_list.append(x.id)
        emp_list = emp_list
        emp_list_max = max(emp_list) 

        record_wizard = self.env['genral.ledger.daily'].search([('id','=',emp_list_max)])
        record_wizard_del = self.env['genral.ledger.daily'].search([('id','!=',emp_list_max)])
        record_wizard_del.unlink()

        to = record_wizard.to
        form = record_wizard.form
        typed = record_wizard.entry_type
        account = record_wizard.account
        form_id = record_wizard.form_link


        print"****************************"
        print"****************************"
        print form_id.state
        print"****************************"
        print"****************************"

        def opening(bal):
            if typed == "all":
                opend = self.env['account.move.line'].sudo().search([('move_id.date','<',form),('account_id.id','=',bal.id)])

            if typed == "posted":
                opend = self.env['account.move.line'].sudo().search([('move_id.date','<',form),('account_id.id','=',bal.id),('move_id.state','=',"posted")])

            debit = 0
            credit = 0
            opening = 0
            for x in opend:
                debit = debit + x.debit
                credit = credit + x.credit

            if bal.nature == 'debit':
                opening = debit - credit

            if bal.nature == 'credit':
                opening = credit - debit

            return opening
        
        if typed == "all":
            entries = self.env['account.move.line'].sudo().search([('move_id.date','>=',form),('move_id.date','<=',to),('account_id.id','=',account.id),('debit','>',0)])
            entries1 = self.env['account.move.line'].sudo().search([('move_id.date','>=',form),('move_id.date','<=',to),('account_id.id','=',account.id),('credit','>',0)])

        if typed == "posted":
            entries = self.env['account.move.line'].sudo().search([('move_id.date','>=',form),('move_id.date','<=',to),('account_id.id','=',account.id),('move_id.state','=',"posted"),('debit','>',0)])
            entries1 = self.env['account.move.line'].sudo().search([('move_id.date','>=',form),('move_id.date','<=',to),('account_id.id','=',account.id),('move_id.state','=',"posted"),('credit','>',0)])


        # def get_act(attr):
        #     value = " "
        #     rec = self.env['account.move'].sudo().search([('id','=',attr)])
        #     for x in rec:
        #         for y in x.line_ids:
        #             if y.account_id.id != account.id:
        #                 if value == " ":
        #                     value = y.account_id.name
        #                 else:
        #                     value = str(value) +' - '+str(y.account_id.name)
                        

        #     return value

        def get_code(attr):
            value = " "
            result = " "
            result1 = " "
            rec = self.env['account.move'].sudo().search([('id','=',attr)])
            for x in rec:
                for y in x.line_ids:
                    if y.account_id.id != account.id:
                        if str(y.partner_id.name) not in value:
                            if value == " ":
                                value = y.partner_id.name
                            else:
                                value = str(value) +'-'+ str(y.partner_id.name)
                        if str(y.name) not in result1:
                            if result1 == " ":
                                result1 = y.name
                            else:
                                result1 = str(result1) +'-'+ str(y.partner_id.name)



            result = value.title()
                        
            return result,result1


        entered = []
        def get_consol(attr):
            del entered [:]
            rec = self.env['account.move'].sudo().search([('id','=',attr)])
            for x in rec:
                entered.append(x)


        entered1 = []
        def get_consol1(attr):
            del entered1 [:]
            rec = self.env['account.move'].sudo().search([('id','=',attr)])
            for x in rec:
                entered1.append(x)


        def get_credit(attr):
            credit = 0
            rec = self.env['account.move'].sudo().search([('id','=',attr)])
            for x in rec.line_ids:
                if x.account_id.id == account.id:
                    credit = credit + x.credit

          
            return credit

        def get_debit(attr):
            debit = 0
            rec = self.env['account.move'].sudo().search([('id','=',attr)])
            for x in rec.line_ids:
                if x.account_id.id == account.id:
                    debit = debit + x.debit

            return debit

        value_act = []
        def get_act(attr):
            del value_act [:]
            rec = self.env['account.move'].sudo().search([('id','=',attr)])
            for x in rec:
                for y in x.line_ids:
                    if y.account_id.id != account.id:
                        if y.account_id not in value_act:
                            value_act.append(y.account_id)

    
        docargs = {
            'doc_ids': docids,
            'doc_model': 'res.partner',
            'docs': account,
            'data': data,
            'form': form,
            'form_id': form_id,
            'to': to,
            'opening': opening,
            'entries': entries,
            'entries1': entries1,
            'get_act': get_act,
            'get_code': get_code,
            'get_credit': get_credit,
            'entered': entered,
            'get_consol': get_consol,
            'get_debit': get_debit,
            'entered1': entered1,
            'get_consol1': get_consol1,
            'value_act': value_act,
            'typed': typed,
        }

        return report_obj.render('daily_cash_book.genral_ledger_report', docargs)