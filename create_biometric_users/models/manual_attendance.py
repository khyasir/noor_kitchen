from openerp import models, fields, api
from openerp.exceptions import Warning
from odoo.exceptions import Warning, ValidationError
from odoo.exceptions import UserError
from openerp.exceptions import UserError
from zklib import zklib
from zklib import zkconst
import config
from datetime import datetime, timedelta , date
import time
from dateutil.relativedelta import relativedelta
from calendar import monthrange
import calendar

class EcubeMachineManualAteendance(models.Model):
	_name = 'manual.attendance'
	
	employee_id = fields.Many2one('hr.employee',string="Employee Name")
	department = fields.Many2one('hr.department',string="Department")
	raw_attendance_in = fields.Many2one('ecube.raw.attendance',string="Attendance in link")
	raw_attendance_out = fields.Many2one('ecube.raw.attendance',string="Attendance out link")
	entity = fields.Many2one('psc.entity',string="Entity")
	machine_id = fields.Char(string='Employee Code')
	
	datetimein = fields.Datetime(string='Date Time In')
	datetimeout = fields.Datetime(string='Date Time Out')
	# datetimein = fields.Datetime(string='Date Time In' , default=lambda *a: datetime.now())

	date_in = fields.Date(string='Date In')
	time_in = fields.Char(string='Time In')
	attendance_date_in = fields.Char('Attendance Date In')
	
	date_out = fields.Date(string='Date Out')
	time_out = fields.Char(string='Time Out')
	attendance_date_out = fields.Char('Attendance Date Out')

	state = fields.Selection([
		('draft', 'Draft'),
		('validate', 'Validated'),
		],string = "Stages", default = 'draft' ,track_visibility='onchange')



	@api.multi
	def draft(self):
		self.state = "draft"
		self.validate_to_draft()



	def validate_to_draft(self):
		if self.raw_attendance_in:
			if self.raw_attendance_in.attendence_done == True:
				actual_attendance = self.raw_attendance_in.tree_link
				self.raw_attendance_in.tree_link = None
				self.raw_attendance_in.attendence_done = False
				print actual_attendance
				print "........................."
				actual_attendance.calculate_raw_attendance()
				self.raw_attendance_in.unlink()

			else:
				self.raw_attendance_in.unlink()

		if self.raw_attendance_out:	
			if self.raw_attendance_out.attendence_done == True:
				actual_attendance_out = self.raw_attendance_out.tree_link
				self.raw_attendance_out.tree_link = None
				self.raw_attendance_out.attendence_done = False
				print actual_attendance_out
				print "........................."
				actual_attendance_out.calculate_raw_attendance()
				self.raw_attendance_out.unlink()
			else:
				self.raw_attendance_out.unlink()


	@api.multi
	def validate(self):
		self.state = "validate"
		self.create_raw_attendance_ecube()

	def create_raw_attendance_ecube(self):
		raw_attendance = self.env['ecube.raw.attendance']
		if self.datetimein:
			create_raw_attendance = raw_attendance.create({
					'machine_id': self.machine_id,
					'date':self.date_in,
					'employee_id': self.employee_id.id,
					'entity': self.entity.id,
					'machine_entity': self.entity.id,
					'department': self.department.id,
					'manual_date': self.datetimein,
					'attendance_date_time': self.datetimein,
					'attendance_date': self.attendance_date_in,
					'manual': True,
					'name': self.id,
					'time': self.time_in
					})
			self.raw_attendance_in = create_raw_attendance.id
		if self.datetimeout:
			create_raw_attendance_out = raw_attendance.create({
					'machine_id': self.machine_id,
					'date':self.date_out,
					'employee_id': self.employee_id.id,
					'entity': self.entity.id,
					'machine_entity': self.entity.id,
					'department': self.department.id,
					'manual_date': self.datetimeout,
					'attendance_date_time': self.datetimeout,
					'attendance_date': self.attendance_date_out,
					'manual': True,
					'name': self.id,
					'time': self.time_out
					})
			self.raw_attendance_out = create_raw_attendance_out.id




	@api.onchange('datetimein')
	def get_time_datein(self):
		if self.datetimein:
			start_date = datetime.strptime(self.datetimein,"%Y-%m-%d %H:%M:%S")
			last_date = start_date + relativedelta(hours=5)
			new_date = str(last_date)
			self.date_in = str(new_date[:10])
			self.attendance_date_in = str(new_date[:10])+" "+str(new_date[11:19])
			self.time_in = str(new_date[11:19])
		else:
			self.date_in = None
			self.time_in = ""
			self.attendance_date_in = ""

	

	@api.onchange('datetimeout')
	def get_time_dateout(self):
		if self.datetimeout:
			start_date = datetime.strptime(self.datetimeout,"%Y-%m-%d %H:%M:%S")
			last_date = start_date + relativedelta(hours=5)
			new_date = str(last_date)
			self.date_out = str(new_date[:10])
			self.attendance_date_out = str(new_date[:10])+" "+str(new_date[11:19])
			self.time_out = str(new_date[11:19])
		else:
			self.date_out = None
			self.time_out = ""
			self.attendance_date_out = ""


	@api.onchange('employee_id')
	def get_customer(self):
		if self.employee_id:
			self.department = self.employee_id.department_id.id
			self.machine_id = self.employee_id.emp_code


	@api.multi
	def unlink(self):
		if self.state == "validate":
			raise ValidationError('Cannot Delete Validated Entry')

		super(EcubeMachineManualAteendance, self).unlink()