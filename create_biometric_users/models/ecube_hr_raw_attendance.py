from openerp import models, fields, api
from openerp.exceptions import Warning
from odoo.exceptions import UserError
from openerp.exceptions import UserError
from datetime import datetime, timedelta , date
import time
from dateutil.relativedelta import relativedelta
from calendar import monthrange
import calendar


class EcubeRawAttendance(models.Model):
	_name = 'ecube.raw.attendance'
	_rec = 'EcubeRawAttendance'
	name = fields.Char('ERP Name')
	# machine_name = fields.Char('Machine Name')
	machine_id = fields.Char(string='Employee Code')
	date = fields.Date(string='Date')
	time = fields.Char(string='Attendance Time')
	employee_id = fields.Many2one('hr.employee',string="Employee Name")
	department = fields.Many2one('hr.department',string="Department")
	entity = fields.Many2one('psc.entity',string="Entity")
	machine_entity = fields.Many2one('psc.entity',string="Machine Entity")
	attendance_date = fields.Char('Attendance Date')
	attendence_done = fields.Boolean(string="Attendence Generated")
	manual = fields.Boolean(string="Manual")
	attendance_date_time = fields.Datetime(string='Attendance Date Time')
	manual_date = fields.Datetime(string='Manual Date' , default=lambda *a: datetime.now())
	attendance_type = fields.Selection([
		('checkin', 'Check In'),
		('checkout', 'Check Out'), 
		('breakin', 'Break In'), 
		('breakout', 'Break Out'), 
		])

	master_id = fields.Char(string="Master ID" ,copy= False)
	status = fields.Selection([
		('sync', 'Synched'),
		('no_sync', 'Not Synched'),
		], string="Status", default='no_sync' ,copy= False)


	@api.onchange('manual_date')
	def get_time_date(self):
		if self.manual_date:
			start_date = datetime.strptime(self.manual_date,"%Y-%m-%d %H:%M:%S")
			last_date = start_date + relativedelta(hours=5)
			new_date = str(last_date)
			self.date = str(new_date[:10])
			self.attendance_date = str(new_date[:10])+" "+str(new_date[11:19])
			self.time = str(new_date[11:19])


	@api.onchange('employee_id')
	def get_customer(self):
		if self.employee_id:
			self.department = self.employee_id.department_id.id
			self.machine_id = self.employee_id.emp_code


	# @api.multi
	# def write(self, vals):
	# 	"""" check if write in this form if write call self function  """
	# 	before=self.write_date
	# 	super(EcubeRawAttendance, self).write(vals)
	# 	after = self.write_date
	# 	if before != after:
	# 		print self.attendance_date_time
	# 		print "............"

	# 	return True

# class EmployeeAdvancesholiday_Ecube(models.Model): 
# 	_inherit = 'hr.holidays'

# 	month = fields.Char(string = "Month Name" , readonly=True)

# 	@api.onchange('date_from')
# 	def update_month_days(self):
# 		if self.date_from:
# 			year  = int(self.date_from[:4])
# 			month = int(self.date_from[5:7])
# 			day = int(self.date_from[8:10])
# 			new_days = monthrange(year, month)
# 			req_days = new_days[-1]
# 			self.month = str(calendar.month_name[month])+' '+str(year)

# 		else:
# 			self.month = False