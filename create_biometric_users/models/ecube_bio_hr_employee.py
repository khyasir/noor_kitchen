# -*- coding: utf-8 -*-

from zklib import zklib
from zklib import zkconst
from datetime import datetime , timedelta
import time
import xmlrpclib
from openerp import models, fields, api
from openerp.exceptions import Warning
from odoo.exceptions import UserError
from openerp.exceptions import UserError
import config
import os
import numpy as np


class hr_create_user_bio_machine(models.Model):
	_inherit = 'hr.employee'


	product_ids=fields.One2many('bio.machine_info','partner_id')

	_sql_constraints= [('card_no','unique(card_no)','this Card No is already exist')]

	@api.multi
	def update_actual_atendance(self):
		self.env["actual.attendence.wizard"].attend_get()


	@api.model
	def updateAttendanceAll_web(self):
		print "start update attendance function"
		machine_list=self.env['machine.info'].search([])
		if not machine_list:
			machine_list=self.env['machine.info'].create({
				'db' : 'data base',
				})
		data_base=machine_list.db
		login=machine_list.odooLogin
		odoopwd=machine_list.odooPasswd
		ip_list=[]
		for x in machine_list.product_ids:
			if not x.status=="no":
				if x.status_web==True:
					ip=x.ip
					print ip
					print "................."
					zk = zklib.ZKLib(ip, int(config.key['port']))
					result=zk.connect()
					print result
					if not result == False:
						self.updateAttendance(ip,data_base,login,odoopwd)

	def updateAttendance(self,ip,data_base,login,odoopwd):
		zk = zklib.ZKLib(ip, int(config.key['port']))
		print ip
		print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
		# common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % config.key['odooserver'])
		# common.version()
		# uid = common.authenticate(data_base, login, odoopwd, {})
		# api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % config.key['odooserver'])
		res = zk.connect()
		zk.enableDevice()
		zk.disableDevice()
		info = []
		attendance = zk.getAttendance()
		# print attendance
		print "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
		actualServerTime = str(datetime.now())
		requiredServerTime = actualServerTime.split('.')
		requiredServerDate = requiredServerTime[0].split(' ')
		if (attendance):
			for lattendance in attendance:
				time_att = str(lattendance[2].date()) + ' ' +str(lattendance[2].time())
				atten_time1 = datetime.strptime(str(time_att), '%Y-%m-%d %H:%M:%S')
				atten_time = atten_time1
				atten_time = datetime.strftime(atten_time,'%Y-%m-%d %H:%M:%S')
				attenDate = str(atten_time).split(' ')
				# if (requiredServerDate[0] == attenDate[0]):
				data = {
				'user_id' :lattendance[0],
				'Date' : str(lattendance[2].date()),
				'Time' : str(lattendance[2].time()),
				'M_date' : attenDate[0],
				'M_time' : attenDate[1],
				'DateTime' : atten_time
					}

				info.append(data)
			# print info
			print "................................................"
			for rec in info:
				user_id_name =rec['user_id']
				machine_date= rec['DateTime']
				employee_id_raw =self.env['hr.employee'].search([('card_no','=',user_id_name)])
				# raw_attendence=self.env['ecube.raw.attendance'].search([('employee_id','=',employee_id_raw.id),('attendance_date','=',str(machine_date))])
				raw_attendence=self.env['ecube.raw.attendance'].search([('employee_id','=',employee_id_raw.id),('date','=',rec['M_date']),('time','=',str(rec['M_time']))])

				if not raw_attendence:
					print "attendance create "
					self.env['ecube.raw.attendance'].create({
						'employee_id': employee_id_raw.id,
						'department': employee_id_raw.department_id.id,
						'attendance_date': machine_date,
						'name': ip,
						'machine_id': str(user_id_name),
						'date': rec['M_date'],
						'time': rec['M_time'],
						})
		else:
			print "attendance Not Found In ERP"
			print ip





	@api.model
	def clearAttendence(self):
		print "start clear attendance function"
		machine_list=self.env['machine.info'].search([])
		if not machine_list:
			machine_list=self.env['machine.info'].create({
				'db' : 'data base',
				})
		data_base=machine_list.db
		login=machine_list.odooLogin
		odoopwd=machine_list.odooPasswd
		ip_list=[]
		for x in machine_list.product_ids:
			if not x.status=="no":
				if x.status_web==True:
					ip=x.ip
					zk = zklib.ZKLib(ip, int(config.key['port']))
					result=zk.connect()
					if not result == False:
						print "function call update attendance"
						self.updateAttendance(ip,data_base,login,odoopwd)
						zk.enableDevice()
						zk.disableDevice()
						attendance = zk.getAttendance()
						if attendance:
							Attend_list = []
							for x in attendance:
								Attend_list.append(str(x[0])+str(x[2]))

							record_raw=[]
							raw_attendence=self.env['ecube.raw.attendance'].search([])
							for rec in raw_attendence:
								record_raw.append(str(rec.machine_id)+str(rec.attendance_date))
							
							missing_ntn = np.setdiff1d(Attend_list,record_raw)
							print "1111111111111111111111111111111"
							print len(missing_ntn)
							print ".............................."
							if len(missing_ntn) == 0:
								print "here we clear attendnace"
								zk.clearAttendance()
								zk.enableDevice()
								zk.disconnect()
								zk.refreshData()


	# def _attendance_error(self,ip):
	# 	record=self.env['ecube.attendence.error'].search([('date','=',time.strftime("%d/%m/%Y"))])
	# 	if not record:
	# 		record=self.env['ecube.attendence.error'].create({
	# 			'date': time.strftime("%d/%m/%Y"),
	# 			})
	# 	for x in record:
	# 		x.product_ids.create({
	# 			'machine_ip_error': ip,
	# 			'time': (datetime.now() + timedelta(hours=5)).strftime("%H:%M:%S"),
	# 			'partner_id': x.id,
	# 			})




	# @api.multi
	# def createBioUsers(self):
	# 	self.product_ids.unlink()
	# 	hr_records=self.env['bio.machine_info']
	# 	records=self.env['machine.info.tree'].search([])
	# 	for x in records:
	# 		hr_records.create({
	# 			'ip': x.ip,
	# 			'des': x.des,
	# 			'partner_id': self.id,
	# 			})



# from zklib import zklib
# from zklib import zkconst
# zk = zklib.ZKLib('192.168.20.24', 4370)
# res = zk.connect()
# print res
# zk.enableDevice()
# zk.disableDevice()
# zk.getUser()
# attendance = zk.getAttendance()
