# -*- coding: utf-8 -*-
import xmlrpclib
from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.exceptions import ValidationError
from openerp.exceptions import UserError
import config
import os
from zk import ZK, const
import time
import datetime
from datetime import date, datetime, timedelta



class hr_create_user_bio_machine_not_web(models.Model):
	_inherit = 'hr.employee'

	@api.multi
	def updateAttendanceAll_not_web(self):
		machine_list=self.env['machine.info'].search([])
		if not machine_list:
			machine_list=self.env['machine.info'].create({
				'db' : 'data base',
				})
		data_base=machine_list.db
		login=machine_list.odooLogin
		odoopwd=machine_list.odooPasswd
		ip_list=[]
		for x in machine_list.product_ids:
			if not x.status=="no":
				if x.status_web==False:
					ip=x.ip
					connect = None
					zk = ZK(ip, port=4370, timeout=10)
					try:
						connect = zk.connect()
						connect.disable_device()
						attendances = connect.get_attendance()
						print attendances
						info = []
						for attendance in attendances:
							data = {
							'user_id' :attendance.user_id,
							'Timestamp' : str(attendance.timestamp - timedelta(minutes=300)),
							'Real_Timestamp' : str(attendance.timestamp),
							'Status' : attendance.status
							}
							info.append(data)
						users = connect.get_users()
						for record in info:
							real_date=record['Real_Timestamp'].split(' ')
							today_date= date.today()
							user_id_name=record['user_id']
							machine_date=record['Real_Timestamp']
							employee_id_raw =self.env['hr.employee'].search([('card_no','=',user_id_name)])
							raw_attendence=self.env['ecube.raw.attendance'].search([('employee_id','=',employee_id_raw.id),('attendance_date','=',machine_date),('machine_id','=',user_id_name),('name','=',ip),('department','=',employee_id_raw.department_id.id)])
							if not raw_attendence:
								print "create attendance"
								self.env['ecube.raw.attendance'].create({
												'employee_id': employee_id_raw.id,
												'department': employee_id_raw.department_id.id,
												'attendance_date': machine_date,
												'name': ip,
												'machine_id': str(user_id_name),
												'date': real_date[0],
												'time': real_date[1],
										})

					except Exception, e:
						print "Process terminate not connnect ......... : {}".format(e)
					finally:
						if connect:
							connect.enable_device()
							connect.disconnect()
	
	@api.multi
	def clearAttendence_not_web(self):
		self.updateAttendanceAll_not_web()
		machine_list=self.env['machine.info'].search([])
		for x in machine_list.product_ids:
			if not x.status=="no":
				if x.status_web==False:
					ip=x.ip
					connect = None
					zk = ZK(ip, port=4370, timeout=10)
					connect = zk.connect()
					connect.disable_device()
					clear_attende = connect.clear_attendance()
					connect.enable_device()
					connect.disconnect()


# Steps that required for this F*****G machine
# Install https://github.com/dnaextrim/python_zklib this library
# Install Selenium
# Download Gekodriver
# Export path of Geckodriver
# Install sudo apt-get install xvfb
#install sudo pip install pyvirtualdisplay

# sudo apt-get install python-pip
# sudo pip install git+https://github.com/ehtishamfaisal/zklib.git
# sudo pip install pyvirtualdisplay
# sudo pip install selenium
# sudo apt-get install xvfb


# sudo pip install -U git+https://github.com/kurenai-ryu/pyzk.git
# sudo pip install git+https://github.com/ehtishamfaisal/zklib.git
# sudo pip install openpyxl
# sudo pip install pyexcel
# sudo pip install xlrd
















