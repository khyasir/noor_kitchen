# -*- coding: utf-8 -*-
{
    'name': "ECUBE Session Module",

    'summary': """
        Ecube""",
    'author': "Jaffar Raza",
    'website': "http://www.ecube.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','web_readonly_bypass','sale','psc_entity'],

    # always loaded
    'data': [
        'views/ecube_session_view.xml',
    ],

}
