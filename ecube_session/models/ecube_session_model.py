# -*- coding: utf-8 -*-
from openerp import models, fields, api
from datetime import timedelta,datetime,date
from odoo.exceptions import Warning, ValidationError
from datetime import date

class ecube_session(models.Model):
	_name = "ecube.session"
	_rec_name = 'rec_name_field'

	# user = fields.Many2one('res.users', string="User", _default={
	# 	'current_uid':lambda self, cr, uid, ctx=None: uid
	# 	})
	def _default_user(self):
		return self._uid

	sequence = fields.Char(string="Sequence")
	rec_name_field = fields.Char(string="Rec Name")
	session_date = fields.Date(string="Session Date")
	user = fields.Many2one('res.users', string="User", default=_default_user)
	product_consumtion_cr = fields.Many2one('account.account', string="Consumtion CR Account")
	product_consumtion_dr = fields.Many2one('account.account', string="Consumtion DR Account")
	consumtion_entry = fields.Many2one('account.move', string="Consumtion Entry" , readonly=True)
	psc_entity = fields.Many2one('psc.entity', string="Entity",required=True)
	entry_id = fields.Many2one('account.move', string="Cash Entry")
	start_date = fields.Datetime(string="Start Date")
	end_date = fields.Datetime(string="End Date")

	opening_balance = fields.Float(string="Float Balance")
	cash_collected = fields.Float(string="Cash Collected")
	expense = fields.Float(string="Expense")
	closing_balance = fields.Float(string="Balance With Float")
	floating_balance = fields.Float(string="Cash After Expense")
	cash_in_hand = fields.Float(string="Cash In Hand")
	difference = fields.Float(string="Difference")
	charging_amount = fields.Float(string="Charging Amount")
	pending_amount = fields.Float(string="Pending Table Amount")

	cash_sales = fields.Float(string="Cash Sales")
	cash_dis = fields.Float(string="Cash Discount")
	cash_net = fields.Float(string="Cash Net")

	cr_sales = fields.Float(string="Cr Sales")
	cr_dis = fields.Float(string="Cr Discount")
	cr_net = fields.Float(string="Cr Net")
	cr_tax_amount = fields.Float(string="Tax Amount")

	t_sales = fields.Float(string="Total Sales")
	cash_delivery_charges = fields.Float(string="Cash Delivery Charges")
	credit_deliver_charges = fields.Float(string="Credit Delivery Charges")
	t_dis = fields.Float(string="Total Discount")
	t_net = fields.Float(string="Total Net")
	tax_amount = fields.Float(string="Tax Amount")
	product_consumtion = fields.Float(string="Consumtion Amount")
	voucher_amount = fields.Float(string="Voucher Amount online")
	voucher_amount_cod = fields.Float(string="Voucher Amount COD")
	credit_card = fields.Float(string="Credit Card")
	fp_voucher_amount = fields.Float(string="FP Voucher Amount")
	t_sale_tax = fields.Float()
	t_untaxed_amount = fields.Float()
	t_sale_tax = fields.Float()
	t_delivery_income = fields.Float()
	amount_charged_cash = fields.Float()
	amount_charged_online = fields.Float()

	five_thousand = fields.Integer('5000')
	one_thousand = fields.Integer('1000')
	five_hundred = fields.Integer('500')
	one_hundred = fields.Integer('100')
	fifty = fields.Integer('50')
	twenty = fields.Integer('20')
	ten = fields.Integer('10')
	five = fields.Integer('5')
	two = fields.Integer('2')
	one = fields.Integer('1')
	total = fields.Integer('Total Cash',readonly=True)

	tree_link = fields.One2many('ecube.overhead.tree','link')
	tree_link_product = fields.One2many('product.consumtion','link')
	parent_db_status = fields.Char(string="Parent db status")
	

	foodpanda_commission = fields.Many2one('account.account', string="Foodpanda Commission")
	sale_act = fields.Many2one('account.account', string="Sale (excluding delivery charges)")
	cash_act = fields.Many2one('account.account', string="Cash Account")
	delivery_income = fields.Many2one('account.account', string="delivery income")
	sale_tax_payable = fields.Many2one('account.account', string="sale tax payable")
	delivery_charges_payable_wap_rider = fields.Many2one('account.account', string="delivery cahrges payable (wap rider )")
	rider_delivery_charges = fields.Many2one('account.account', string="rider delivery charges")
	dis_act = fields.Many2one('account.account', string="Discount Account")
	cash_account = fields.Many2one('account.account', string="Cash Account")
	advance_salary_of_employee_partner = fields.Many2one('account.account', string=" Advance salary (employee partner a/c)")
	director_entertainment = fields.Many2one('account.account', string="Directors entertainment")
	complimentry_food_expense = fields.Many2one('account.account', string="Complimentry food expense")
	other_income = fields.Many2one('account.account', string="Other Income")
	
	sale_cc = fields.Float(string='Sales')
	tax_cc = fields.Float(string='Taxes')
	total_cc = fields.Float(string='Total')
	session_charging_emp = fields.Many2one('hr.employee', string="Charging Employee")

	branch_user = fields.Char(string="Branch User")
	branch_seq = fields.Char(string="Branch Sequence")
	receipe_id = fields.Many2one('stock.picking',copy=False,readonly=True)



	@api.multi
	def session_wise_raw_material_consume(self):

		if self.receipe_id:
			self.receipe_id.action_cancel()
			self.receipe_id = False


		cust_loc = self.env['stock.location'].search([('usage','=','customer')],limit=1)
		current_date = fields.date.today()

		create_recorder = self.env['stock.picking'].create({
			'min_date':self.session_date,
			'location_dest_id': cust_loc.id,
			'location_id': self.psc_entity.stock_location.id,
			'picking_type_id':self.psc_entity.picking_type.id,
			'type':'branch_consump',
			'psc_entity':self.psc_entity.id,
			'origin':self.rec_name_field,
			'desc':"Branch Sale Receipy Consumption",
			'receiving_date':self.session_date,
		})
		create_recorder.type = 'branch_consump'

		sale_count = self.env['sale.order.line'].search([('order_id.so_state','=','done'),('order_id.session.id','=',self.id)])

		self.receipe_id = create_recorder.id
		for z in sale_count:
			if z.product_id.recipe_id:
				for y in z.product_id.recipe_id:
					if y.product_id:
						issuance_now = self.env['stock.move'].search([('picking_id.id','=',create_recorder.id),('product_id.id','=',y.product_id.id)])
						if issuance_now:
							issuance_now.product_uom_qty = issuance_now.product_uom_qty + (z.product_uom_qty*y.quantity)
						else:
							if y.product_id:
								create_tree = self.env['stock.move'].create({
									'product_id': y.product_id.id,
									'product_uom_qty': z.product_uom_qty*y.quantity,
									'product_uom': y.product_id.uom_id.id,
									'location_id': self.psc_entity.stock_location.id,
									'name': y.product_id.name,
									'location_dest_id': cust_loc.id,
									'picking_id': create_recorder.id
									})

			elif z.product_id.deal_id:
				for j in z.product_id.deal_id:
					if j.product_id.recipe_id:
						for y in j.product_id.recipe_id:
							if y.product_id:
								issuance_now = self.env['stock.move'].search([('picking_id.id','=',create_recorder.id),('product_id.id','=',y.product_id.id)])
								if issuance_now:
									issuance_now.product_uom_qty = issuance_now.product_uom_qty + ((z.product_uom_qty*j.quantity)*y.quantity)
								else:
									if y.product_id:
										create_tree = self.env['stock.move'].create({
											'product_id': y.product_id.id,
											'product_uom_qty': ((z.product_uom_qty*j.quantity)*y.quantity),
											'product_uom': y.product_id.uom_id.id,
											'location_id': self.psc_entity.stock_location.id,
											'name': y.product_id.name,
											'location_dest_id': cust_loc.id,
											'picking_id': create_recorder.id
											})

					else:
						raise ValidationError('Product ' + j.product_id.name + " receipe is not set.")

			else:
				raise ValidationError('Product ' + z.product_id.name + " receipe or " + z.product_id.name +" deal is not set.")
				


		create_recorder.action_assign()
		create_recorder.action_confirm()
		create_recorder.force_assign()
	   
		for lines in create_recorder.pack_operation_product_ids:
			lines.qty_done = lines.product_qty

		create_recorder.do_new_transfer()
		create_recorder.min_date = current_date
		self.prouct_consumtiom_amount()

	@api.multi
	def prouct_consumtiom_amount(self):

		if not self.product_consumtion_dr or not self.product_consumtion_cr:

			raise ValidationError("Set The Consumtion Debit or Credit.")

		# sale_count = self.env['sale.order.line'].search([('order_id.id','=',1714)])
		sale_count = self.env['sale.order.line'].search([('order_id.session','=',self.id),('order_id.so_state','=','done')])
		receipe_amount = 0
		deal_amount = 0
		self.tree_link_product.unlink()
		for z in sale_count:
			receipe_amount_1 = 0
			deal_amount_1 = 0
			total = 0
			if z.product_id.recipe_id:
				for y in z.product_id.recipe_id:
					if y.product_id:
						receipe_amount = receipe_amount + (z.product_uom_qty * y.quantity * y.product_id.branch_price)
						receipe_amount_1 = receipe_amount_1 + (z.product_uom_qty * y.quantity * y.product_id.branch_price)


			elif z.product_id.deal_id:
				for j in z.product_id.deal_id:
					if j.product_id.recipe_id:
						for y in j.product_id.recipe_id:
							if y.product_id:
								deal_amount = deal_amount + (z.product_uom_qty * j.quantity * y.quantity * y.product_id.branch_price)
								deal_amount_1 = deal_amount_1 + (z.product_uom_qty * j.quantity * y.quantity * y.product_id.branch_price)
			
			# total = receipe_amount_1 + deal_amount_1
			# consumtion = self.env['product.consumtion'].create({
			# 	'product_id':z.product_id.id,
			# 	'product_id_chr':z.product_id.name,
			# 	'qty':  z.product_uom_qty,
			# 	'amount':total,
			# 	'link' : self.id,
			# 	})
		self.product_consumtion =  receipe_amount + deal_amount

		if self.product_consumtion > 0:
			journal = self.env['account.journal'].search([('type','=','cash')],limit=1)
			if self.consumtion_entry:
				self.consumtion_entry.unlink()
			move_id = self.env['account.move'].create({
				'journal_id':journal.id,
				'psc_entity':self.psc_entity.id,
				'date':self.session_date,
				'ref' : self.sequence,
				'narration' : self.id,
				})

			self.consumtion_entry = move_id.id

			debit = self.create_entry_lines(self.product_consumtion_dr.id,self.product_consumtion,0,str(self.sequence)+','+str(self.user.name),move_id.id)
			credit = self.create_entry_lines(self.product_consumtion_cr.id,0,self.product_consumtion,str(self.sequence)+','+str(self.user.name),move_id.id)
	
	@api.multi
	def write(self, vals):
		before=self.write_date
		super(ecube_session, self).write(vals)
		after = self.write_date
		if before != after:
			if self.state == 'closed':
				if self.status == 'sync' and self.master_id:
					raise ValidationError("session is closed,  please contact your administrator ")
			self.get_rec_name()

		return True

	@api.onchange('session_date','psc_entity')
	def get_rec_name(self):
		if not self.session_date:
			self.session_date = self.start_date
		self.rec_name_field = str(self.psc_entity.name) + ' - ' + str(self.session_date) 


	state = fields.Selection([
		('draft', 'Draft'),
		('open', 'Open'),
		('closed', 'Closed'),
		],
		default = "draft")

	# ============== Buttons Portion START ==================



	@api.multi
	def goto_quotation(self):
		rec = self.env['sale.order'].search([('session','=',self.id),('so_state','=','done'),('payment_type','=','cod')]).ids

		domain = [('id','in',rec)]


		return {
		'type': 'ir.actions.act_window',
		'name': ('Quotations'),
		'res_model': 'sale.order',
		'view_type': 'form',
		'view_mode': 'pivot',
		'view_id ref="sale.view_sale_order_pivot"': '',
		'target': 'current',
		'domain': domain,
		}





	@api.multi
	def goto_quotation_online(self):
		rec = self.env['sale.order'].search([('session','=',self.id),('so_state','=','done'),('payment_type','=','online_pay')]).ids

		domain = [('id','in',rec)]


		return {
		'type': 'ir.actions.act_window',
		'name': ('Quotations'),
		'res_model': 'sale.order',
		'view_type': 'form',
		'view_mode': 'pivot',
		'view_id ref="sale.view_sale_order_pivot"': '',
		'target': 'current',
		'domain': domain,
		}




	@api.multi
	def pos_je(self):
		self.set_data()
		sale_count = self.env['sale.order'].search([('session','=',self.id),('so_state','=','done')])
		foodpandan_rec = self.env['res.partner'].search([('foodpanda','=',True)],limit=1)
		eat_mubarak_rec = self.env['res.partner'].search([('eat_mubarak','=',True)],limit=1)
		
		rider_deliver_charges_amount = 45
		sale_label = 'WAP Sale delivery'
		foodpanda_label = 'Food Panda Commission'
		sale  = 0
		delivery_charges_amount  = 0
		sale_tax_amount  = 0
		foodpanda_commision_amount  = 0
		wap_sale_delivery_discount  = 0
		FP_commision_sale  = 0
		

		# Food Panda Online Payment, delivery by WAP
		foodpanda_online_deliver_wap = 'FP Online Payment,delivery by WAP'
		f_online_sale  = 0
		f_online_delivery_charges  = 0
		f_online_sale_tax_amount  = 0
		

		# Food Panda Online Payment, delivery by WAP
		foodpanda_cod_deliver_wap = 'FP COD Payment,delivery by WAP'
		f_cod_sale  = 0
		f_cod_delivery_charges  = 0
		f_cod_sale_tax_amount  = 0
		f_cod_sale_cash  = 0
		

		# Food Panda Online Delivery by FoodPanda
		foodpanda_online_deliver_foodpandna = 'FP Online Delivery by FP'
		f_online_sale_DBFP  = 0
		f_online_delivery_charges_DBFP  = 0
		f_online_sale_tax_amount_DBFP  = 0
		f_online_delivery_expense_DBFP  = 0
		trade_payable_foodpanda_DBFP  = 0
		rider_delivery_charges_DBFP = 0
		

		# Food Panda Delivery COD Food Panda
		foodpanda_COD_label = 'FP Payment COD Delivery by FP'
		f_COD_sale  = 0
		f_COD_delivery_charges = 0
		f_COD_ST  = 0
		f_COD_delivery_delivery_expense  = 0
		f_COD_delivery_trade_payable_foodpanda  = 0
		f_COD_delivery_rider_delivery_charges = 0
		

		foodpanda_charge_label = 'Charge to food panda/ food panda rider'
		f_ChargeTo_sale  = 0
		ChargeTo_Foodpandan_DC = 0
		ChargeTo_Foodpandan_ST  = 0
		f_ChargeTo_delivery_delivery_expense  = 0
		f_ChargeTo_delivery_trade_payable_foodpanda  = 0
		f_ChargeTo_delivery_rider_delivery_charges = 0
		

		foodpanda_charge_label_wap = 'Charge to food panda/ WAP rider'
		f_ChargeTo_sale_DBWAP  = 0
		ChargeTo_Foodpandan_DC_DBWAP = 0
		ChargeTo_Foodpandan_ST_DBWAP  = 0
		f_ChargeTo_delivery_delivery_expense_DBWAP  = 0
		f_ChargeTo_delivery_trade_payable_foodpanda_DBWAP  = 0
		f_ChargeTo_delivery_rider_delivery_charges_DBWAP = 0
		

		wap_sale_Dine_take_label = 'WAP Sale Dine in and take away'
		wap_Dine_take_sale  = 0
		wap_Dine_take_DC = 0
		wap_Dine_take_ST  = 0
		sale_and_tax  = 0
		wap_sale_discount  = 0
		total_cash_dine_in_takeaway  = 0
		

		eat_mubrak_label = 'eat mubarik delivery '
		eat_mubrak_sale  = 0
		eat_mubrak_DC = 0
		eat_mubrak_ST  = 0
		eat_mubrak_discount  = 0
		sale_tax_and_delivery  = 0
		

		# P_online_eat_mubrak_label = 'eat mubarik online delivery '
		# P_online_eat_mubrak_sale  = 0
		# P_online_eat_mubrak_DC = 0
		# P_online_eat_mubrak_ST  = 0
		# P_online_sale_tax_and_delivery  = 0
		# P_online_sale_tax_and_delivery  = 0
		

		ChargeTO_Eat_mubarak_label = 'Charge to eat mubarik '
		chargeto_eat_mubrak_sale  = 0
		chargeto_eat_mubrak_DC = 0
		chargeto_eat_mubrak_ST  = 0
		chargeto_sale_tax_and_delivery  = 0
		cahrgeto_eat_total  = 0
		chargeto_eat_total_line  = 0
		chargeto_eat_total  = 0
		chargeto_eat_discount  = 0
		chargeto_eat_voucher_amount  = 0
		

		ChargeTO_WAP_label = 'Charging To WAP outlet employee '
		ChargeTO_WAP_sale  = 0
		ChargeTO_WAP_DC = 0
		ChargeTO_WAP_ST  = 0
		ChargeTO_WAP_discount  = 0
		ChargeTO_WAP_voucher_amount  = 0
		

		ChargeTO_WAP_label_cash_diff = 'Charging To WAP outlet employee Cash diff'
		excess_cash = 'excess Cash (till plus)'
		
		
		foodpanda_delivery_expense = 0
		eat_mubarak_delivery_expense = 0
		wap_rider_delivery_expense = 0

		journal = self.env['account.journal'].search([('type','=','cash')],limit=1)
		if self.entry_id:
			self.entry_id.unlink()
		
		if not self.session_date:
			raise ValidationError("Enter The Session Date.")

		move_id = self.env['account.move'].create({
			'journal_id':journal.id,
			'psc_entity':self.psc_entity.id,
			'date':self.session_date,
			'ref' : self.sequence,
			'narration' : self.id,
			})

		self.entry_id = move_id.id
		

		if self.entry_id:
			for so in sale_count:

				if so.so_type == "cc" and so.charge_type.name == 'Self Delivery' and so.no_cash==False:
					print "WAP Sale delivery"
					# WAP Sale delivery

					sale = sale + so.amount_untaxed
					delivery_charges_amount = delivery_charges_amount + so.delivery_charges
					sale_tax_amount = sale_tax_amount + so.amount_tax
					wap_sale_delivery_discount = wap_sale_delivery_discount + so.discount_amount

					label = ""
					label = str(sale_label)+ ' - '+str(so.rider_id.name)+ ' - ' +str(so.name)
					
					debit = self.create_entry_lines(self.dis_act.id,so.discount_amount,0,label,move_id.id)
					credit = self.create_entry_lines_session(self.delivery_charges_payable_wap_rider.id,so.rider_id.partner_id.id,0,rider_deliver_charges_amount,label,move_id.id)
					debit = self.create_entry_lines_session(self.rider_delivery_charges.id,so.rider_id.partner_id.id,rider_deliver_charges_amount,0,label,move_id.id)
				
				if so.so_type == "foodpanda":
					print "Food Panda Commission"
					# Food Panda Commission

					FP_commision_sale = FP_commision_sale + so.amount_untaxed
					foodpanda_commision_line_by_line = so.amount_untaxed * (float(foodpandan_rec.foodpanda_commision)/100)




					label = ""
					label = str(foodpanda_label)+ ' - '+ str(so.name)
					credit = self.create_entry_lines_session(foodpandan_rec.property_account_payable_id.id,foodpandan_rec.id,0,foodpanda_commision_line_by_line,label,move_id.id)

				if so.so_type == "foodpanda" and so.payment_type == "online_pay" and so.charge_type.name == 'Self Delivery':
					# Food Panda Online Payment, delivery by WAP
					print 'Food Panda Online Payment, delivery by WAP'
					f_online_sale = f_online_sale + so.amount_untaxed
					f_online_delivery_charges = f_online_delivery_charges + so.delivery_charges
					f_online_sale_tax_amount = f_online_sale_tax_amount + so.amount_tax
					
					trade_payable_foodpanda = so.amount_untaxed + so.delivery_charges + so.amount_tax
					
					label = ""
					label = str(foodpanda_online_deliver_wap)+ ' - '+str(so.rider_id.name)+ ' - ' +str(so.name)
					
					debit = self.create_entry_lines_session(foodpandan_rec.property_account_payable_id.id,foodpandan_rec.id,trade_payable_foodpanda,0,label,move_id.id)
					
					credit = self.create_entry_lines_session(self.delivery_charges_payable_wap_rider.id,so.rider_id.partner_id.id,0,rider_deliver_charges_amount,label,move_id.id)
					debit = self.create_entry_lines_session(self.rider_delivery_charges.id,so.rider_id.partner_id.id,rider_deliver_charges_amount,0,label,move_id.id)

				if so.so_type == "foodpanda" and so.payment_type == "online_pay" and so.charge_type.name == 'Food Panda':
					# Food Panda Online Delivery by FoodPanda
					print "Food Panda Online Delivery by FoodPanda"
					f_online_sale_DBFP = f_online_sale_DBFP + so.amount_untaxed
					f_online_delivery_charges_DBFP = f_online_delivery_charges_DBFP + so.delivery_charges
					f_online_sale_tax_amount_DBFP = f_online_sale_tax_amount_DBFP + so.amount_tax
					

					trade_payable_foodpanda_DBFP = so.amount_untaxed + so.delivery_charges + so.amount_tax
					f_online_delivery_expense_DBFP = so.amount_untaxed * (foodpandan_rec.foodpanda_delivery_expense/100)
					rider_delivery_charges_DBFP = so.amount_untaxed * (foodpandan_rec.foodpanda_delivery_expense/100)

					
					label = ""
					label = str(foodpanda_online_deliver_foodpandna)+ ' - ' +str(so.name)

					debit = self.create_entry_lines_session(foodpandan_rec.property_account_payable_id.id,foodpandan_rec.id,trade_payable_foodpanda_DBFP,0,label,move_id.id)
					credit = self.create_entry_lines_session(foodpandan_rec.property_account_payable_id.id,foodpandan_rec.id,0,f_online_delivery_expense_DBFP,label,move_id.id)
					debit = self.create_entry_lines_session(self.rider_delivery_charges.id,foodpandan_rec.id,rider_delivery_charges_DBFP,0,label,move_id.id)
				
				if so.so_type == "foodpanda" and so.payment_type == "cod" and so.no_cash == False and so.charge_type.name == 'Food Panda':
					# Food Panda Delivery COD Food Panda
					print "Food Panda Delivery COD Food Panda"
					f_COD_sale = f_COD_sale + so.amount_untaxed
					f_COD_delivery_charges = f_COD_delivery_charges + so.delivery_charges
					f_COD_ST = f_COD_ST + so.amount_tax
					

					trade_payable_foodpanda_COD = so.amount_untaxed + so.delivery_charges + so.amount_tax
					rider_delivery_charges_COD = so.amount_untaxed * (foodpandan_rec.foodpanda_delivery_expense/100)
					
					label = ""
					label = str(foodpanda_COD_label)+ ' - ' +str(so.name)

					
					debit = self.create_entry_lines_session(foodpandan_rec.property_account_payable_id.id,foodpandan_rec.id,trade_payable_foodpanda_COD,0,label,move_id.id)
					credit = self.create_entry_lines_session(foodpandan_rec.property_account_payable_id.id,foodpandan_rec.id,0,rider_delivery_charges_COD,label,move_id.id)
					debit = self.create_entry_lines_session(self.rider_delivery_charges.id,foodpandan_rec.id,rider_delivery_charges_COD,0,label,move_id.id)
				if so.so_type == "foodpanda" and so.no_cash == True and so.sale_vendor.foodpanda == True and so.charge_type.name == 'Food Panda':
					# Charge to food panda/ food panda rider
					print "Charge to food panda/ food panda rider"
					f_ChargeTo_sale = f_ChargeTo_sale + so.amount_untaxed
					ChargeTo_Foodpandan_DC = ChargeTo_Foodpandan_DC + so.delivery_charges
					ChargeTo_Foodpandan_ST = ChargeTo_Foodpandan_ST + so.amount_tax
					

					trade_payable__chargeTOfoodpanda = so.amount_untaxed + so.delivery_charges + so.amount_tax
					
					rider_delivery_chargeTO_Foodpandan = so.amount_untaxed * (foodpandan_rec.foodpanda_delivery_expense/100)
					
					label = ""
					label = str(foodpanda_charge_label)+ ' - ' +str(so.name)

					debit = self.create_entry_lines_session(foodpandan_rec.property_account_payable_id.id,foodpandan_rec.id,trade_payable__chargeTOfoodpanda,0,label,move_id.id)
					credit = self.create_entry_lines_session(foodpandan_rec.property_account_payable_id.id,foodpandan_rec.id,0,rider_delivery_chargeTO_Foodpandan,label,move_id.id)
					debit = self.create_entry_lines_session(self.rider_delivery_charges.id,foodpandan_rec.id,rider_delivery_chargeTO_Foodpandan,0,label,move_id.id)

				if so.so_type == "foodpanda" and so.no_cash ==True and so.sale_vendor.foodpanda == True and so.charge_type.name == 'Self Delivery':

					# Charge to food panda/ WAP rider
					print "Charge to food panda/ WAP rider"
					f_ChargeTo_sale_DBWAP  = f_ChargeTo_sale_DBWAP + so.amount_untaxed
					ChargeTo_Foodpandan_DC_DBWAP = ChargeTo_Foodpandan_DC_DBWAP + so.delivery_charges
					ChargeTo_Foodpandan_ST_DBWAP = ChargeTo_Foodpandan_ST_DBWAP + so.amount_tax
					

					trade_payable_chargeTOfoodpanda_DBWAP = so.amount_untaxed + so.delivery_charges + so.amount_tax
					
					label = ""
					label = str(foodpanda_charge_label_wap)+ ' - '+ str(so.rider_id.name)+ ' - ' +str(so.name)

					debit = self.create_entry_lines_session(foodpandan_rec.property_account_payable_id.id,foodpandan_rec.id,trade_payable_chargeTOfoodpanda_DBWAP,0,label,move_id.id)
					
					credit = self.create_entry_lines_session(self.delivery_charges_payable_wap_rider.id,so.rider_id.partner_id.id,0,rider_deliver_charges_amount,label,move_id.id)
					debit = self.create_entry_lines_session(self.rider_delivery_charges.id,so.rider_id.partner_id.id,rider_deliver_charges_amount,0,label,move_id.id)

				if so.so_type == "foodpanda" and so.no_cash ==False and  so.payment_type == "cod" and so.charge_type.name == 'Self Delivery':

					# FP COD Payment,delivery by WAP
					print "FP COD Payment,delivery by WAP"
					f_cod_sale  = f_cod_sale + so.amount_untaxed
					f_cod_delivery_charges = f_cod_delivery_charges + so.delivery_charges
					f_cod_sale_tax_amount = f_cod_sale_tax_amount + so.amount_tax
					
					
					label = ""
					label = str(foodpanda_cod_deliver_wap)+ ' - '+ str(so.rider_id.name)+ ' - ' +str(so.name)
					
					credit = self.create_entry_lines_session(self.delivery_charges_payable_wap_rider.id,so.rider_id.partner_id.id,0,rider_deliver_charges_amount,label,move_id.id)
					debit = self.create_entry_lines_session(self.rider_delivery_charges.id,so.rider_id.partner_id.id,rider_deliver_charges_amount,0,label,move_id.id)
				
				if so.so_type in "so" or so.so_type == "take_away":
					if so.no_cash == False:
						# 'WAP Sale Dine in and take away'
						print "WAP Sale Dine in and take away"
						wap_Dine_take_sale  = wap_Dine_take_sale + so.amount_untaxed
						wap_Dine_take_ST = wap_Dine_take_ST + so.amount_tax
						
						sale_and_tax = sale_and_tax + (so.amount_untaxed + so.amount_tax)
						wap_sale_discount  = wap_sale_discount + so.discount_amount
						
						label = ""
						label = wap_sale_Dine_take_label+' - '+so.name
						
						debit = self.create_entry_lines(self.dis_act.id,so.discount_amount,0,label,move_id.id)
				
				if so.charge_type.name == 'Eat Mubarik' and so.no_cash ==False :
					# 'eat mubarik COD delivery '
					print "eat mubarik COD delivery "
					eat_mubrak_sale  = eat_mubrak_sale + so.amount_untaxed
					eat_mubrak_DC = eat_mubrak_DC + so.delivery_charges
					eat_mubrak_ST = eat_mubrak_ST + so.amount_tax
					eat_mubrak_discount = eat_mubrak_discount + so.discount_amount
					

					sale_tax_and_delivery = sale_tax_and_delivery + (so.amount_untaxed + so.amount_tax + so.delivery_charges) - so.discount_amount
					eat_mubarak_rider_charge = eat_mubarak_rec.eat_mubarak_delivery_charges
					
					label = ""
					label = eat_mubrak_label+' - '+so.name

					credit = self.create_entry_lines_session(eat_mubarak_rec.property_account_payable_id.id,eat_mubarak_rec.id,0,eat_mubarak_rider_charge,label,move_id.id)
					
					debit = self.create_entry_lines(self.rider_delivery_charges.id,eat_mubarak_rider_charge,0,label,move_id.id)
					debit = self.create_entry_lines(self.dis_act.id,eat_mubrak_discount,0,label,move_id.id)


				# if so.charge_type.name == 'Eat Mubarik' and so.payment_type == "online_pay":
				# 	# 'eat mubarik Online delivery '
				# 	P_online_eat_mubrak_sale  = P_online_eat_mubrak_sale + so.amount_untaxed
				# 	P_online_eat_mubrak_DC = P_online_eat_mubrak_DC + so.delivery_charges
				# 	P_online_eat_mubrak_ST = P_online_eat_mubrak_ST + so.amount_tax
					

				# 	P_online_sale_tax_and_delivery = P_online_sale_tax_and_delivery + (so.amount_untaxed + so.amount_tax + so.delivery_charges)
				# 	eat_mubarak_rider_charge = eat_mubarak_rec.eat_mubarak_delivery_charges
					
				# 	total_payment = (so.amount_untaxed + so.amount_tax + so.delivery_charges)
				# 	label = ""
				# 	label = P_online_eat_mubrak_label+' - '+so.name

				# 	debit = self.create_entry_lines_session(foodpandan_rec.property_account_payable_id.id,foodpandan_rec.id,total_payment,0,label,move_id.id)
				# 	credit = self.create_entry_lines_session(eat_mubarak_rec.property_account_payable_id.id,eat_mubarak_rec.id,0,eat_mubarak_rider_charge,label,move_id.id)
					
				# 	debit = self.create_entry_lines(self.rider_delivery_charges.id,eat_mubarak_rider_charge,0,label,move_id.id)
					
				if 	so.charge_type.name == 'Eat Mubarik' and so.no_cash ==True and so.sale_vendor.eat_mubarak == True:
					# 'Charge to eat mubarik'
					print "eat mubarik COD delivery "
					chargeto_eat_mubrak_sale  = chargeto_eat_mubrak_sale + so.amount_untaxed
					chargeto_eat_mubrak_DC = chargeto_eat_mubrak_DC + so.delivery_charges
					chargeto_eat_mubrak_ST = chargeto_eat_mubrak_ST + so.amount_tax
					chargeto_eat_discount = chargeto_eat_discount + so.discount_amount
					chargeto_eat_voucher_amount = chargeto_eat_voucher_amount + so.voucher_amount
					

					chargeto_eat_total = chargeto_eat_total + (so.amount_untaxed + so.amount_tax + so.delivery_charges)
					chargeto_eat_rider_charge = eat_mubarak_rec.eat_mubarak_delivery_charges
					
					chargeto_eat_total_line = (so.amount_untaxed + so.amount_tax + so.delivery_charges) - so.discount_amount - so.voucher_amount
					label = ""
					label = ChargeTO_Eat_mubarak_label+' - '+so.name

					debit = self.create_entry_lines_session(eat_mubarak_rec.property_account_payable_id.id,eat_mubarak_rec.id,chargeto_eat_total_line,0,label,move_id.id)
					credit = self.create_entry_lines_session(eat_mubarak_rec.property_account_payable_id.id,eat_mubarak_rec.id,0,chargeto_eat_rider_charge,label,move_id.id)
					debit = self.create_entry_lines_session(self.rider_delivery_charges.id,eat_mubarak_rec.id,chargeto_eat_rider_charge,0,label,move_id.id)
					debit = self.create_entry_lines(self.dis_act.id,chargeto_eat_discount,0,label,move_id.id)
					
					debit = self.create_entry_lines_session(foodpandan_rec.property_account_payable_id.id,foodpandan_rec.id,so.voucher_amount,0,label,move_id.id)
					
				if  so.sale_vendor and so.no_cash == True:
					if not so.sale_vendor.eat_mubarak == True and not so.sale_vendor.foodpanda == True:
						# 'Charging To WAP outlet employee'
						print "eat mubarik COD delivery "
						ChargeTO_WAP_sale  = ChargeTO_WAP_sale + so.amount_untaxed
						ChargeTO_WAP_DC = ChargeTO_WAP_DC + so.delivery_charges
						ChargeTO_WAP_ST = ChargeTO_WAP_ST + so.amount_tax
						ChargeTO_WAP_discount = ChargeTO_WAP_discount + so.discount_amount
						ChargeTO_WAP_voucher_amount = ChargeTO_WAP_voucher_amount + so.voucher_amount
						

						chargeto_WAP_total = sale_tax_and_delivery + (so.amount_untaxed + so.amount_tax + so.delivery_charges)
						
						chargeto_WAP_total_line = (so.amount_untaxed + so.amount_tax + so.delivery_charges) - so.voucher_amount - so.discount_amount
						label = ""
						label = ChargeTO_WAP_label+' - '+so.name+' ( '+so.sale_vendor.name+' )'

						debit = self.create_entry_lines(self.dis_act.id,so.discount_amount,0,label,move_id.id)
						debit = self.create_entry_lines_session(self.advance_salary_of_employee_partner.id,so.sale_vendor.id,chargeto_WAP_total_line,0,label,move_id.id)
						debit = self.create_entry_lines_session(foodpandan_rec.property_account_payable_id.id,foodpandan_rec.id,so.voucher_amount,0,label,move_id.id)
						
						if so.rider_id:
							credit = self.create_entry_lines_session(self.delivery_charges_payable_wap_rider.id,so.rider_id.partner_id.id,0,rider_deliver_charges_amount,label,move_id.id)
							debit = self.create_entry_lines_session(self.rider_delivery_charges.id,so.rider_id.partner_id.id,rider_deliver_charges_amount,0,label,move_id.id)

			# "................consolidated entries......................"
			
			if self.difference > 0:
				# difference is positve :
				label = ""
				label = str(excess_cash)+str(' ('+ self.sequence + ') ')
				debit = self.create_entry_lines(self.cash_account.id,self.difference,0,label,move_id.id)
				credit = self.create_entry_lines(self.other_income.id,0,self.difference,label,move_id.id)
			
			if self.difference < 0:
				# difference is negative :
				label = ""
				label = str(ChargeTO_WAP_label_cash_diff)+str(' ('+ self.sequence + ') ')+str(self.session_charging_emp.name)
				credit = self.create_entry_lines_session(self.cash_account.id,self.session_charging_emp.id,0,abs(self.difference),label,move_id.id)
				debit = self.create_entry_lines_session(self.advance_salary_of_employee_partner.id,self.session_charging_emp.id,abs(self.difference),0,label,move_id.id)
				

					
			# # 'Charging To WAP outlet employee'
			credit = self.create_entry_lines(self.sale_act.id,0,ChargeTO_WAP_sale,ChargeTO_WAP_label,move_id.id)
			credit = self.create_entry_lines(self.delivery_income.id,0,ChargeTO_WAP_DC,ChargeTO_WAP_label,move_id.id)
			credit = self.create_entry_lines(self.sale_tax_payable.id,0,ChargeTO_WAP_ST,ChargeTO_WAP_label,move_id.id)
			

			# # 'Charge to eat mubarik'
			credit = self.create_entry_lines(self.sale_act.id,0,chargeto_eat_mubrak_sale,ChargeTO_Eat_mubarak_label,move_id.id)
			credit = self.create_entry_lines(self.sale_tax_payable.id,0,chargeto_eat_mubrak_ST,ChargeTO_Eat_mubarak_label,move_id.id)
			credit = self.create_entry_lines(self.delivery_income.id,0,chargeto_eat_mubrak_DC,ChargeTO_Eat_mubarak_label,move_id.id)

					
			

			# eat mubarik COD delivery
			credit = self.create_entry_lines(self.sale_act.id,0,eat_mubrak_sale,eat_mubrak_label,move_id.id)
			credit = self.create_entry_lines(self.delivery_income.id,0,eat_mubrak_DC,eat_mubrak_label,move_id.id)
			credit = self.create_entry_lines(self.sale_tax_payable.id,0,eat_mubrak_ST,eat_mubrak_label,move_id.id)
			debit = self.create_entry_lines(self.cash_account.id,sale_tax_and_delivery,0,eat_mubrak_label,move_id.id)

					
			

			# eat mubarik Online delivery
			# credit = self.create_entry_lines(self.sale_act.id,0,P_online_eat_mubrak_sale,P_online_eat_mubrak_label,move_id.id)
			# credit = self.create_entry_lines(self.delivery_income.id,0,P_online_eat_mubrak_DC,P_online_eat_mubrak_label,move_id.id)
			# credit = self.create_entry_lines(self.sale_tax_payable.id,0,P_online_eat_mubrak_ST,P_online_eat_mubrak_label,move_id.id)
			

			# 'WAP Sale Dine in and take away'
			credit = self.create_entry_lines(self.sale_act.id,0,wap_Dine_take_sale,wap_sale_Dine_take_label,move_id.id)
			credit = self.create_entry_lines(self.sale_tax_payable.id,0,wap_Dine_take_ST,wap_sale_Dine_take_label,move_id.id)
			
			total_cash_dine_in_takeaway = sale_and_tax - wap_sale_discount
			debit = self.create_entry_lines(self.cash_account.id,total_cash_dine_in_takeaway,0,wap_sale_Dine_take_label,move_id.id)

			

			# 'WAP Sale delivery'
			credit = self.create_entry_lines(self.sale_act.id,0,sale,sale_label,move_id.id)
			credit = self.create_entry_lines(self.sale_tax_payable.id,0,sale_tax_amount,sale_label,move_id.id)
			credit = self.create_entry_lines(self.delivery_income.id,0,delivery_charges_amount,sale_label,move_id.id)
			
			total_cash_sale = sale + sale_tax_amount + delivery_charges_amount - wap_sale_delivery_discount
			
			debit = self.create_entry_lines(self.cash_account.id,total_cash_sale,0,sale_label,move_id.id)

			
			# 'Food Panda Online Payment, delivery by WAP'
			credit = self.create_entry_lines(self.sale_act.id,0,f_online_sale,foodpanda_online_deliver_wap,move_id.id)

			credit = self.create_entry_lines(self.sale_tax_payable.id,0,f_online_sale_tax_amount,foodpanda_online_deliver_wap,move_id.id)
			credit = self.create_entry_lines(self.delivery_income.id,0,f_online_delivery_charges,foodpanda_online_deliver_wap,move_id.id)



			# Food Panda Online Delivery by FoodPanda
			credit = self.create_entry_lines(self.sale_act.id,0,f_online_sale_DBFP,foodpanda_online_deliver_foodpandna,move_id.id)
			credit = self.create_entry_lines(self.delivery_income.id,0,f_online_delivery_charges_DBFP,foodpanda_online_deliver_foodpandna,move_id.id)
			credit = self.create_entry_lines(self.sale_tax_payable.id,0,f_online_sale_tax_amount_DBFP,foodpanda_online_deliver_foodpandna,move_id.id)


			# Food Panda Delivery COD Food Panda
			credit = self.create_entry_lines(self.sale_act.id,0,f_COD_sale,foodpanda_COD_label,move_id.id)
			credit = self.create_entry_lines(self.delivery_income.id,0,f_COD_delivery_charges,foodpanda_COD_label,move_id.id)
			credit = self.create_entry_lines(self.sale_tax_payable.id,0,f_COD_ST,foodpanda_COD_label,move_id.id)


			# Charge to food panda/ food panda rider
			credit = self.create_entry_lines(self.sale_act.id,0,f_ChargeTo_sale,foodpanda_charge_label,move_id.id)
			credit = self.create_entry_lines(self.delivery_income.id,0,ChargeTo_Foodpandan_DC,foodpanda_charge_label,move_id.id)
			credit = self.create_entry_lines(self.sale_tax_payable.id,0,ChargeTo_Foodpandan_ST,foodpanda_charge_label,move_id.id)


			# Charge to food panda/ WAP rider
			credit = self.create_entry_lines(self.sale_act.id,0,f_ChargeTo_sale_DBWAP,foodpanda_charge_label_wap,move_id.id)
			credit = self.create_entry_lines(self.delivery_income.id,0,ChargeTo_Foodpandan_DC_DBWAP,foodpanda_charge_label_wap,move_id.id)
			credit = self.create_entry_lines(self.sale_tax_payable.id,0,ChargeTo_Foodpandan_ST_DBWAP,foodpanda_charge_label_wap,move_id.id)


			# Food Panda Commission
			foodpanda_commision_amount = FP_commision_sale * (float(foodpandan_rec.foodpanda_commision)/100)
			debit = self.create_entry_lines(self.foodpanda_commission.id,foodpanda_commision_amount,0,foodpanda_label,move_id.id)


			# # FP COD Payment,delivery by WAP
			credit = self.create_entry_lines(self.sale_act.id,0,f_cod_sale,foodpanda_cod_deliver_wap,move_id.id)
			credit = self.create_entry_lines(self.delivery_income.id,0,f_cod_delivery_charges,foodpanda_cod_deliver_wap,move_id.id)
			credit = self.create_entry_lines(self.sale_tax_payable.id,0,f_cod_sale_tax_amount,foodpanda_cod_deliver_wap,move_id.id)


			f_cod_sale_cash = f_cod_sale + f_cod_delivery_charges + f_cod_sale_tax_amount
			
			debit = self.create_entry_lines(self.cash_account.id,f_cod_sale_cash,0,foodpanda_cod_deliver_wap,move_id.id)



			


	# @api.multi
	# def open_pressed(self):
	# 	rec = self.env['ecube.session'].search([('user.id','=',self.user.id),('state','=','open'),('psc_entity.id','=',self.psc_entity.id)])
	# 	if rec:
	# 		raise ValidationError("Already a session is open with the same user. Close that first")
	# 	else:
	# 		self.state = 'open'

	# 	sale_order = self.env['sale.order'].search([('so_state','in',['received','kot','invoiced']),('psc_entity','=',self.psc_entity.id)])
	# 	if sale_order:
	# 		for s in sale_order:
	# 			s.session = self.id
	# 	self.session_date = date.today()
	# 	self.get_rec_name()
	
	@api.multi
	def close_pressed(self):
		self.set_data()
		self.end_date = date.today()
		self.pending_table_amount()
		self.state = 'closed'

	@api.multi
	def set_data_so(self):
		all_so = self.env['sale.order'].search([('so_state','=','done')])
		for x in all_so:
			if x.session:
				if x.session.session_date:
					x.effective_date = x.session.session_date
				else:
					x.session_date = x.session.start_date
					x.effective_date = x.session.start_date


	@api.multi
	def set_data(self):
		session = self.env['ecube.session'].search([('id','=',self.id)])
		for ses in session:
			ses.cash_collected = 0
			ses.expense = 0
			ses.closing_balance = 0
			ses.difference = 0
			ses.charging_amount = 0
			ses.pending_amount = 0
			ses.cash_sales = 0
			ses.cash_dis = 0
			ses.cash_net = 0
			ses.cr_sales = 0
			ses.cr_dis = 0
			ses.cr_net = 0
			ses.cr_tax_amount = 0
			ses.t_sales = 0
			ses.t_dis = 0
			ses.t_net = 0
			ses.voucher_amount_cod = 0
			ses.voucher_amount = 0
			ses.credit_deliver_charges = 0
			ses.cash_delivery_charges = 0
			ses.tax_amount = 0
			ses.t_sale_tax = 0
			ses.t_sale_tax = 0
			ses.t_delivery_income = 0
			ses.amount_charged_cash = 0
			ses.amount_charged_online = 0
			ses.sale_cc = 0
			ses.tax_cc = 0
			ses.total_cc = 0
			sales = self.env['sale.order'].search([('session.id','=',ses.id),('so_state','=','done')])
			if sales:
				for sal in sales:
					if sal.no_cash == False:
						if not sal.payment_type == 'online_pay':
							sal.session.cash_collected = sal.session.cash_collected + sal.amount_total
							sal.session.cash_sales = sal.session.cash_sales + sal.amount_untaxed
							sal.session.tax_amount = sal.session.tax_amount + sal.amount_tax
							sal.session.cash_dis = sal.session.cash_dis + sal.discount_amount
							sal.session.voucher_amount_cod = sal.session.voucher_amount_cod + sal.voucher_amount
							sal.session.cash_delivery_charges = sal.session.cash_delivery_charges + sal.delivery_charges
							sal.session.cash_net = sal.session.cash_net + sal.amount_untaxed +  sal.amount_tax + sal.delivery_charges
							# sal.session.cash_net = sal.session.cash_net + sal.amount_untaxed +  sal.amount_tax + sal.delivery_charges - sal.discount_amount - sal.voucher_amount
						else:
							sal.session.cr_sales = sal.session.cr_sales + sal.amount_untaxed
							sal.session.cr_tax_amount = sal.session.cr_tax_amount + sal.amount_tax
							sal.session.cr_dis = sal.session.cr_dis + sal.discount_amount
							sal.session.voucher_amount = sal.session.voucher_amount + sal.voucher_amount
							sal.session.credit_deliver_charges = sal.session.credit_deliver_charges + sal.delivery_charges
							sal.session.cr_net = sal.session.cr_net + sal.amount_untaxed +  sal.amount_tax + sal.delivery_charges
							# sal.session.cr_net = sal.session.cr_net + sal.amount_untaxed +  sal.amount_tax + sal.delivery_charges - sal.discount_amount - sal.voucher_amount
					
					else:
						if not sal.payment_type == 'online_pay':
							sal.session.amount_charged_cash = sal.session.amount_charged_cash + sal.amount_total
						else:
							sal.session.amount_charged_online = sal.session.amount_charged_online + sal.amount_total
					if sal.so_type in ('so','take_away') and sal.payment_type == 'online_pay':
							sal.session.sale_cc = sal.session.sale_cc + sal.amount_untaxed
							sal.session.tax_cc = sal.session.tax_cc + sal.amount_tax
							sal.session.total_cc = sal.session.sale_cc + sal.session.tax_cc 

			self.get_expenses()
			self.get_close()
			self.get_diff()
			self.get_total_fp_voucher()
			self.get_total_untaxed_amount()
			self.get_total_sale_tax()
			self.get_total_delivery_charges()
			self.get_tot_net()


	def get_total_fp_voucher(self):
		self.fp_voucher_amount = self.voucher_amount_cod + self.voucher_amount

	def get_total_untaxed_amount(self):
		self.t_untaxed_amount = self.cash_sales + self.cr_sales

	def get_total_sale_tax(self):
		self.t_sale_tax = self.tax_amount + self.cr_tax_amount

	def get_total_delivery_charges(self):
		self.t_delivery_income = self.cash_delivery_charges + self.credit_deliver_charges


	@api.onchange('tree_link')
	def get_expenses(self):
		if self.tree_link:
			amt = 0
			for x in self.tree_link:
				amt = amt + x.amount

			self.expense = amt
			self.get_close()
		else:
			self.expense = 0

	def pending_table_amount(self):
		sale_order = self.env['sale.order'].search([('session','=',self.id),('so_state','in',['received','kot','invoiced'])])
		pending_amt = 0
		if sale_order:
			for pt in sale_order:
				pending_amt = pending_amt + pt.amount_total
		self.pending_amount = pending_amt
				
	@api.onchange('five_thousand','one_thousand','five_hundred','one_hundred','fifty','twenty','ten','five','two','one')
	def get_tot(self):
		five_thousand = 0
		one_thousand = 0
		five_hundred = 0
		one_hundred = 0
		fifty = 0
		twenty = 0
		ten = 0
		five = 0
		two = 0
		one = 0
		five_thousand = 5000 * self.five_thousand
		one_thousand = 1000 * self.one_thousand
		five_hundred = 500 * self.five_hundred
		one_hundred = 100 * self.one_hundred
		fifty = 50 * self.fifty
		twenty = 20 * self.twenty
		ten = 10 * self.ten
		five = 5 * self.five
		two = 2 * self.two
		one = 1 * self.one
		self.total = five_thousand + one_thousand + five_hundred + one_hundred + fifty + twenty + ten + five + two + one


	@api.onchange('cash_collected','expense','opening_balance')
	def get_close(self):
		self.closing_balance = (self.opening_balance + self.cash_collected) - self.expense
		self.floating_balance = self.cash_collected - self.expense

	@api.onchange('cash_in_hand','closing_balance')
	def get_diff(self):

		print "get diff"
		print self.floating_balance
		print self.cash_in_hand
		self.difference = (self.cash_in_hand - self.floating_balance)
		print self.difference
		print "dddddddddddddddddddd"
		self.charging_amount = (self.cash_in_hand - self.floating_balance)

	@api.onchange('cash_sales','cash_dis')
	def get_cash_net(self):
		pass
		# self.cash_net = (self.cash_sales + self.tax_amount) - self.cash_dis + self.cash_delivery_charges - self.voucher_amount_cod

	@api.onchange('cr_sales','cr_dis')
	def get_cr_net(self):
		pass
		# self.cr_net = (self.cr_sales + self.cr_tax_amount) - self.cr_dis  + self.credit_deliver_charges - self.voucher_amount

	@api.onchange('cash_sales','cr_sales')
	def get_tot_sales(self):
		self.t_sales = self.cash_sales + self.cr_sales

	@api.onchange('cash_dis','cr_dis')
	def get_tot_dis(self):
		self.t_dis = self.cash_dis - self.cr_dis

	@api.onchange('cash_net','cr_net')
	def get_tot_net(self):
		self.t_net = self.t_untaxed_amount + self.t_sale_tax + self.t_delivery_income

	@api.model
	def create(self, vals):
		vals['sequence'] = self.env['ir.sequence'].next_by_code('ecube.session.seq')
		new_record = super(ecube_session, self).create(vals)

		return new_record

	@api.multi
	def create_entry(self):
		journal = self.env['account.journal'].search([('type','=','cash')],limit=1)
		if not self.entry_id:
			move_id = self.env['account.move'].create({
				'journal_id':journal.id,
				})

			self.entry_id = move_id.id

			payment_debit = self.create_entry_lines(self.sale_act.id,self.cash_sales,0,str(self.sequence)+','+str(self.user.name),move_id.id)
			if self.cash_dis:
				payment_dis = self.create_entry_lines(self.dis_act.id,0,self.cash_dis,str(self.sequence)+','+str(self.user.name),move_id.id)
				payment_credit = self.create_entry_lines(self.cash_act.id,0,self.cash_net,str(self.sequence)+','+str(self.user.name),move_id.id)
			else:
				payment_credit = self.create_entry_lines(self.cash_act.id,0,self.cash_net,str(self.sequence)+','+str(self.user.name),move_id.id)



	def create_entry_lines(self,account,debit,credit,label,move_id):
		if debit > 0 or credit > 0:
			self.env['account.move.line'].create({
				'account_id':account,
				'name':label,
				'debit':debit,
				'credit':credit,
				'move_id':move_id,
				})
	
	def create_entry_lines_session(self,account,partner,debit,credit,label,move_id):
		if debit > 0 or credit > 0:
			self.env['account.move.line'].create({
				'account_id':account,
				'partner_id':partner,
				'name':label,
				'debit':debit,
				'credit':credit,
				'move_id':move_id,
				})

class OverheadTree(models.Model):
	_name = 'ecube.overhead.tree'

	expense_type = fields.Many2one('ecube.expense.type',string="Expense Type",required=True)
	descrip = fields.Char(string="Description",required=True)
	amount = fields.Float(string="Amount",required=True)
	link = fields.Many2one('ecube.session')

class product_con_tree(models.Model):
	_name = 'product.consumtion'

	product_id = fields.Many2one('product.product',string="Product",required=True)
	product_id_chr = fields.Char(string="Product")
	qty = fields.Float(string="QTY",required=True)
	amount = fields.Float(string="Amount",required=True)
	link = fields.Many2one('ecube.session')
	

class ExpenseTypeClass(models.Model):
	_name = 'ecube.expense.type'
	_rec_name = 'name'

	name = fields.Char(string="Name",required=True)

	# ============== Buttons Portion ENDS ===================



	