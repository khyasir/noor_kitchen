# -*- coding: utf-8 -*-
{
    'name': "Manual Database Backup to Amazon S3",

    'summary': """
        Database Backup to Amazon s3 bucket""",

    'description': """
        Backup your database using the dependent module and add backup of the database file to S3 bucket directly
    """,

    'author': "Hamza Azeem Qureshi ,Enterprise Cube (pvt) ltd",
    'website': "https://ecube.pk",
    'sequence': '10',
    'category': 'Backup',
    'version': '0.1',
    'external_dependencies': {'python': ['boto']},

    # any module necessary for this one to work correctly
    'depends': ['base', 'auto_backup'],

    # always loaded
    'data': [
        'views/s3_backup.xml',
    ],

    # only loaded in demonstration mode
    'demo': [

    ],

    'auto_install': False,
    'installable': True,
    # 'images': ['static/description/storage.JPG'],
}
