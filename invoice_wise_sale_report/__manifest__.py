{
	'name': 'Invoice Wise Sale Report', 

	'description': 'Invoice Wise Sale Report', 
	
	'author': 'Rana Rizwan',
	
	'depends': ['base', 'report','psc_entity'], 
	
	'data': [
        'template.xml',
        'views/module_report.xml',
    ],
}