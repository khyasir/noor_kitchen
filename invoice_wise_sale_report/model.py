#-*- coding:utf-8 -*-
########################################################################################
########################################################################################
##                                                                                    ##
##    OpenERP, Open Source Management Solution                                        ##
##    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved       ##
##                                                                                    ##
##    This program is free software: you can redistribute it and/or modify            ##
##    it under the terms of the GNU Affero General Public License as published by     ##
##    the Free Software Foundation, either version 3 of the License, or               ##
##    (at your option) any later version.                                             ##
##                                                                                    ##
##    This program is distributed in the hope that it will be useful,                 ##
##    but WITHOUT ANY WARRANTY; without even the implied warranty of                  ##
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   ##
##    GNU Affero General Public License for more details.                             ##
##                                                                                    ##
##    You should have received a copy of the GNU Affero General Public License        ##
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.           ##
##                                                                                    ##
########################################################################################
########################################################################################

from openerp import models, fields, api
from datetime import date
import re
import pandas as pd
import numpy as np
import psycopg2 as pg
import pandas.io.sql as psql
import getpass
from datetime import datetime, date, timedelta


class invoice_wise_sale_report(models.AbstractModel):
    _name = 'report.invoice_wise_sale_report.forward_report'


    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        form = docs.form
        to = docs.to
        entity = docs.entity
           
        company = self.env['res.company'].search([])

        sale_order = self.env['sale.order'].search([('effective_date','>=',form),('effective_date','<=',to),('psc_entity','=',entity.id),('so_state','=','done')])
        main_data = []
        for x in sale_order:
          tp = ''
          if x.so_type == 'cc':
            tp = 'Delivery'
          if x.so_type == 'so':
            tp = 'Table'
          if x.so_type == 'foodpanda':
            tp = 'FOODPANDA'
          if x.so_type == 'take_away':
            tp = 'Take Away'
          main_data.append({
            'tab_name':x.sequence,
            'so_type':tp,
            'gross_sale':x.amount_total,
            'taxes':x.amount_tax,
            'discount':x.discount_percent,
            'untax_amt':x.amount_untaxed,
            'discount_amt':x.discount_amount,
            })


        docargs = {
           'doc_ids': self.ids,
           'doc_model': self.model,
           'docs': docs,
           'form': form,
           'to': to,
           'entity': entity,
           'company': company,
           'main_data': main_data,
        }
        
        return self.env['report'].render('invoice_wise_sale_report.forward_report', docargs)