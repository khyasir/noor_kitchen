# #-*- coding:utf-8 -*-
# ##############################################################################
# #
# #    OpenERP, Open Source Management Solution
# #    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved
# #
# #    This program is free software: you can redistribute it and/or modify
# #    it under the terms of the GNU Affero General Public License as published by
# #    the Free Software Foundation, either version 3 of the License, or
# #    (at your option) any later version.
# #
# #    This program is distributed in the hope that it will be useful,
# #    but WITHOUT ANY WARRANTY; without even the implied warranty of
# #    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# #    GNU Affero General Public License for more details.
# #
# #    You should have received a copy of the GNU Affero General Public License
# #    along with this program.  If not, see <http://www.gnu.org/licenses/>.
# #
# ##############################################################################
from odoo import models, fields, api
from datetime import datetime,date
from dateutil.relativedelta import relativedelta

class InvoiceWiseSaleReport(models.TransientModel):
	_name = "invoice.wise.sale.report"

	form = fields.Date(string="From",required=True,default=date.today())
	to = fields.Date(string="To",required=True,default=date.today())
	entity = fields.Many2one('psc.entity',string='Entity',default=1)

	@api.multi
	def generate_report(self):
	    data = {}
	    data['form'] = self.read(['form','to','entity'])[0]
	    return self._print_report(data)

	def _print_report(self, data):
	    data['form'].update(self.read(['form','to','entity'])[0])
	    return self.env['report'].get_action(self, 'invoice_wise_sale_report.forward_report', data=data)
