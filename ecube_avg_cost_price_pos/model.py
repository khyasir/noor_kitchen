# -*- coding: utf-8 -*- 
from odoo import models, fields, api
from openerp.exceptions import Warning, ValidationError, UserError
import datetime
import datetime as dt

class product_extension(models.Model): 
    _inherit = 'product.product'

    # history_link = fields.One2many('product.history','history_id' , readonly=True)
    deal_id = fields.One2many('product.deal','deal_tree',track_visibility='onchange')
    inventory_value = fields.Float(string='Inventory Value' ,track_visibility='onchange' , readonly=True)
    branch_price = fields.Float(string='Branch Price' ,track_visibility='onchange')
    is_deal = fields.Boolean(string="Is A Deal" ,track_visibility='onchange')
    finish = fields.Boolean(string="Finish")
    drinks = fields.Boolean(string="Drinks")
    allow_edit = fields.Boolean(string="Allow Edit" ,track_visibility='onchange')
    recipe_id = fields.One2many('recipe.recipe','recipe_tree_prod')
    brand = fields.Many2many('ecube.brand' ,track_visibility='onchange')
    drink_size = fields.Many2one('ecube.drink' ,track_visibility='onchange')
    other_tax = fields.Many2one('account.tax' ,track_visibility='onchange')

class product_deal_class(models.Model):
    _name = 'product.deal'

    product_id = fields.Many2one('product.product',string='Product',required=True)
    quantity = fields.Float(required=True)
    deal_tree = fields.Many2one('product.product')

class recipe_recipe(models.Model):
    _name = 'recipe.recipe'

    product_id = fields.Many2one('product.product',string='Product',required=True)
    product_uom = fields.Many2one('product.uom',string="Uom")
    quantity = fields.Float(digits=(16,3))
    price = fields.Float(digits=(16,3) ,compute='get_price')
    cost = fields.Float(digits=(16,3) ,compute='get_total_cost')
    recipe_tree = fields.Many2one('product.template')
    recipe_tree_prod = fields.Many2one('product.product')

    # @api.onchange('quantity')
    # def get_cost(self):
    #     if self.product_id:
    #         self.price = self.product_id.branch_price
    #         self.cost = self.price * self.quantity


    @api.one
    def get_price(self):
        if self.product_id:
            if self.recipe_tree_prod.pos_categ:
                self.price = self.product_id.branch_price
            else:
                self.price = self.product_id.standard_price
    @api.one
    def get_total_cost(self):
        if self.product_id:
            if self.recipe_tree_prod.pos_categ:
                self.cost = self.price * self.quantity
            else:
                self.cost = self.price * self.quantity


    @api.onchange('product_id')
    def recipe_recipe_product(self):
        if self.product_id:
            self.product_uom = self.product_id.uom_id.id


    @api.onchange('quantity','price')
    def recipe_recipe_cost(self):
        self.cost = self.quantity * self.price

# class product_history(models.Model):
#   _name = 'product.history'
    
#   date        = fields.Date(string="From Date")
#   end_date    = fields.Date(string="To Date")
#   qty         = fields.Float(string="Quantity")
#   unit_price  = fields.Float(string="Unit Price")
#   pre_qty     = fields.Float(string="Previous Quantity")
#   adjustment  = fields.Float(string="Adjustment")
#   net         = fields.Float(string="Net")
#   pre_price   = fields.Float(string="Previous Average Price")
#   avg_price   = fields.Float(string="Average Price")
#   po_qty      = fields.Float(string="PO Quantity")
#   inventory_value = fields.Float(string="Inventory Value")
#   po_no       = fields.Many2one('purchase.order')
#   inv_no      = fields.Many2one('account.invoice')
#   history_id  = fields.Many2one('product.product')
#   supplier_id = fields.Many2one('res.partner', domain="[('supplier','=',True)]")
#   po_line     = fields.Many2one('purchase.order.line')
#   stock_id    = fields.Many2one('stock.pack.operation')


# class product_deal_class(models.Model):
#     _name = 'product.deal'

#     product_id = fields.Many2one('product.product',string='Product',required=True)
#     quantity = fields.Float(required=True)
#     deal_tree = fields.Many2one('product.product')


# class stock_picking_own(models.Model):
#   _inherit        = 'stock.picking'

#   receiving_date  = fields.Date(string="Transfer Date",copy=False ,track_visibility='onchange')


#   def do_new_transfer(self):
#       total_qty = 0
#       done_qty = 0
#       for x in self.pack_operation_product_ids:
#           total_qty = total_qty + x.product_qty
#           done_qty = done_qty + x.qty_done
#       if total_qty == done_qty or total_qty > done_qty:
#           self.get_current_stock()
#       new_record = super(stock_picking_own, self).do_new_transfer()
#       if total_qty == done_qty or total_qty > done_qty:
#           self.update_receiving_date()


#       return new_record

#   def get_current_stock(self):
#       for pack in self.pack_operation_product_ids:
#           pack.qty_available = pack.product_id.qty_available 


#   def update_receiving_date(self):
#       current_date = fields.date.today()
#       if not self.receiving_date:
#           self.receiving_date = current_date


# class stock_pack_extension(models.Model):
#   _inherit    = 'stock.pack.operation'

#   qty_available = fields.Float(string="Qty Available" , readonly=True )


# class InvoicePaymentExtension_AvgPrice(models.Model):
#   _inherit = 'account.invoice'

    

#   @api.multi
#   def action_invoice_open(self):
#       res = super(InvoicePaymentExtension_AvgPrice, self).action_invoice_open()
#       if self.move_id:
#           self.move_id.psc_entity = self.psc_entity.id
#       if self.type == "in_invoice":
#           self.update_product_history()

#       return res


#   def update_product_history(self):
#       # pickings = self.env['stock.picking'].search([('origin','=',self.source_document),('state','=',"done")])
#       # if not pickings:
#       #   raise ValidationError('Shipment has not been received yet')
#       # history = self.env['product.history'].search([('inv_no','=',self.id)])
#       # if history:
#       #   for x in history:
#       #       x.unlink()

#       for line in self.invoice_line_ids:
#       #   last_history = []
#       #   history = self.env['product.history'].search([('history_id','=',line.product_id.id),('date','<',pickings.receiving_date)])
#       #   if history:
#       #       last_history = history.sorted(key=lambda r: r.date)[-1]
#       #   if last_history:
#       #       previous_price = last_history.avg_price
#       #       last_history.end_date = pickings.receiving_date
#       #   if not last_history:
#       #       previous_price = line.product_id.standard_price
#       #   for pack in pickings.pack_operation_product_ids:
#       #       if line.product_id.id == pack.product_id.id:
#       #           previous_quantity = pack.qty_available
#       #   purchase_order = self.env['purchase.order'].search([('name','=',self.source_document)])
#       #   average_price = (((previous_quantity) * previous_price) + (line.quantity * line.price_unit))/(line.product_id.qty_available)    
#       #   create_history_line = history.create({
#       #                   'date':pickings.receiving_date,
#       #                   'pre_qty':previous_quantity,
#       #                   'pre_price':previous_price,
#       #                   'qty':line.quantity,
#       #                   'unit_price':line.price_unit,
#       #                   'avg_price':average_price,
#       #                   'history_id':line.product_id.id,
#       #                   'supplier_id':self.partner_id.id,
#       #                   'inventory_value':(line.product_id .qty_available * average_price),
#       #                   'po_no':purchase_order.id,
#       #                   'inv_no':self.id
#       #                   })
#           line.product_id.standard_price = line.price_unit
#           # line.product_id.inventory_value = (line.product_id .qty_available * average_price)

class drinks_ecube_class(models.Model):
    _name = "ecube.drink"

    name = fields.Char(string="Name")
