{
	'name': 'Product AVG Cost Price only for POS', 
	'description': 'Product AVG Cost Price', 
	'author': 'Yasir Rauf', 
	'depends': ['base','account'], 
	'application': True,
	'data': [
	'views/template.xml'
	],
    'css': ['static/css/style.css'],
}
