# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import ValidationError

class CharofAccounts(models.Model):
	_inherit = 'account.account'
	account_show = fields.Boolean(string="Show Account")
	show_all_accounts = fields.Boolean(string="Show All Accounts" , default = True)
	bank = fields.Boolean(string="Bank")
	tax_bank = fields.Boolean(string="Tax Bank")
	cash = fields.Boolean(string="Cash")
	related_user = fields.Many2one('res.users',string="Related User")
	active = fields.Boolean(default = True)

	
	def duplicate_cash(self):
		print "xxxxxxxxxxxxxxxxxxxxxxxxxxxx"
		if self.related_user:
			duplicate_record = self.env['account.account'].search([('related_user','=',self.related_user.id),('id','!=',self.id)])
			if duplicate_record:
				raise ValidationError("User already associated to other cash account")
	def duplicate_code(self):
		print "xxxxxxxxxxxxxxxxxxxxxxxxxxxx"
		if self.code:
			duplicate_record = self.env['account.account'].search([('code','=',self.code),('id','!=',self.id)])
			if duplicate_record:
				raise ValidationError("This code is already associated to an other account.")

	@api.model
	def create(self, vals):
		new_record = super(CharofAccounts, self).create(vals)
		new_record.duplicate_cash()
		new_record.duplicate_code()
		



		
		return new_record

	@api.multi
	def write(self, vals):

		
		super(CharofAccounts, self).write(vals)
		self.duplicate_cash()
		self.duplicate_code()

		return True
