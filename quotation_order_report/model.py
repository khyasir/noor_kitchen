#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api

class QuotationOrderReport(models.AbstractModel):
    _name = 'report.quotation_order_report.project_payment'

    @api.model
    def render_html(self,docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('quotation_order_report.project_payment')
        records = self.env['sale.order'].browse(docids)

        # project = self.env['project.payment.schedule'].search([('project_link','=',records.name)])

        company = self.env['res.company'].search([])

        docargs = {
            'doc_ids': docids,
            'doc_model': 'alif.project',
            'docs': records,
            'company': company,
            # 'project':project,
            }

        return report_obj.render('quotation_order_report.project_payment', docargs)