# -*- coding: utf-8 -*-
{
    'name': "Quotation Print",

    'summary': "Quotation Print",

    'description': "Quotation Print",

    'author': "EnterpriseCube(Noman)",
    'website': "http://www.ecube.com",

    # any module necessary for this one to work correctly
    'depends': ['base'],
    # always loaded
    'data': [
        'template.xml',
        'views/module_report.xml',
    ],
    'css': ['static/src/css/report.css'],
}
