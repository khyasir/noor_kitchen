#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api
from num2words import num2words
from datetime import timedelta,datetime,date
from dateutil.relativedelta import relativedelta


class kot_sale_order_report(models.AbstractModel):
    _name = 'report.kot_sale_order_report.demand_report_report'

    @api.model
    def render_html(self,docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('kot_sale_order_report.demand_report_report')
        records = self.env['sale.order'].browse(docids)
        print records
        print "dddddddddddddddddd"
        print "dddddddddddddddddd"
        records.adjust_deal_drinks()
        if records.order_time:
            print "fffffffffffffffffffff"
            start_date = datetime.strptime(records.order_time,"%Y-%m-%d %H:%M:%S")
            print_date = start_date + relativedelta(hours=5)
            print_date = str(print_date)
        else:
            print "ffffff"
            print records
            start_date = datetime.strptime(records.date_order,"%Y-%m-%d %H:%M:%S")
            print_date = start_date + relativedelta(hours=5)
            print_date = str(print_date)


        order_lines = []
        for x in records:
            for y in x.order_line:
                if not y.kot_check and y.product_uom_qty > 0:
                    order_lines.append(y)

        prods = []
        for rec in order_lines:
            if rec.product_id not in prods:
                prods.append(rec.product_id)
        prod_detail = [] 
        for pro in prods:
            order_rec = self.env['sale.order.line'].search([('order_id.id','=',records.id),('product_id','=',pro.id),('kot_check','=',False),('product_uom_qty','>',0)])

            qty = 0
            for com in order_rec:
                qty = qty + com.product_uom_qty

            prod_deal = [] 
            if pro.is_deal and pro.deal_id:
                for deal in pro.deal_id:
                    if not deal.product_id.drinks:
                        prod_deal.append({
                            'name':deal.product_id.name,
                            'qty':qty*deal.quantity,
                            })

                for odl in order_lines:
                    if pro.id == odl.product_id.id:
                        if odl.drink_detail:
                            for dri in odl.drink_detail:
                                prod_deal.append({
                                    'name':dri.product_id.name,
                                    'qty':dri.qty,
                                })


                # drinks_tree = self.env['pos.drinks.tree'].search([('product_id.id','=',pro.id),('drinks_tree.id','=',records.id)])
                # if drinks_tree:
                #     for dri in drinks_tree:
                #         prod_deal.append({
                #             'name':dri.drinks_id.name,
                #             'qty':dri.qty,
                #             })


            prod_detail.append({
                'prod':pro.name,
                'qty':qty,
                'prod_deal':prod_deal,
                })

        void_prods = []
        for void in records.order_line:
            if void.void_qty > 0:
                if void.product_id not in void_prods:
                    void_prods.append(void.product_id)          
        void_detail = []
        for q in void_prods:
            void_qty = 0
            for p in records.order_line:
                if p.product_id.id == q.id and p.void_qty > 0:
                    void_qty = void_qty + p.void_qty
            if void_qty > 0:
                void_detail.append({
                    'prod':q.name,
                    'qty':void_qty,
                    })

        docargs = {
            'doc_ids': docids,
            'doc_model': 'sale.order',
            'docs': records,
            'doc': order_lines,
            'print_date': print_date,
            'prod_detail': prod_detail,
            'void_detail': void_detail,
            }

        for x in records:
            if x.so_type == "foodpanda":
                x.so_state = 'invoiced'
                x.inv_button = True
            else:
                x.so_state = 'kot'
                x.kot_button = True

            x.kot_time = datetime.now()
            x.kot_counter = x.kot_counter + 1
            
            x.allowed = True
            x.clone_sync_bool = False
            
            for y in x.order_line:
                y.kot_check = True
                y.kot = True

        # records.so_return_kanban_view()

        return report_obj.render('kot_sale_order_report.demand_report_report', docargs)



