# -*- coding: utf-8 -*-
{
    'name': "KOT Invoice Report",

    'summary': "KOT Invoice Report",

    'description': "KOT Invoice Report",

    'author': "Rana Rizwan",
    'website': "http://www.bcube.com",

    # any module necessary for this one to work correctly
    'depends': ['base', 'report','sale','so_extension'],
    # always loaded
    'data': [
        'template.xml',
        'views/module_report.xml',
    ],
    'css': ['static/src/css/report.css'],
}
