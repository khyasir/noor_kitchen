# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo import http
from odoo.http import request
import json
import requests

#FBR POS Integration
class FBRIntegrationPos(models.Model):
	_inherit = 'sale.order'


	fbr_invoice_number = fields.Char("FBR Invoice#")

	@api.multi
	def fbr_invoice_number_generate(self):
		company_rec = self.env['res.company'].sudo().search([],limit=1)
		s=requests.session()

		URL = "http://"+str(company_rec.local_ip)+":8524/api/IMSFiscal/GetInvoiceNumberByModel"
		  
		# location given here
		total_qty=0.0
		for x in self.order_line:
			total_qty += x.product_uom_qty
					
		# defining a params dict for the parameters to be sent to the API
		items=[{
		"ItemCode": rec.product_id.id, 
		"ItemName": str(rec.name),
		"Quantity": rec.product_uom_qty,
		"PCTCode":str(company_rec.pctcode),
		"SaleValue":rec.price_subtotal,
		"TotalAmount":rec.price_total,
		"TaxCharged":rec.price_tax,
		"TaxRate":(rec.price_tax*100)/rec.price_subtotal,
		# "Discount":0.0,
		# "FurtherTax":0.0,
		"InvoiceType":1}for rec in self.order_line]

			
		# "POSID":954460,
		params={
		"InvoiceNumber":"",
		"POSID":int(company_rec.posid),
		"USIN":str(company_rec.usin),
		"DateTime":(self.order_time),
		"TotalBillAmount":self.amount_total,
		"TotalQuantity":float(sum(rec.product_uom_qty for rec in self.order_line)),
		"TotalTaxCharged":float(self.amount_tax),
		"TotalSaleValue":float(self.amount_untaxed),
		"Discount":float(self.discount_amount),
		# "FurtherTax":0.0,
		"PaymentMode":1,
		"InvoiceType":1,
		"Items":items}
		print params

		# sending post request and saving the response as response object
		headers={'Content-type':'application/json;charset=utf-8'}
		r = s.post(url = URL,data=json.dumps(params),headers=headers)
		print r
		# extracting data in json format
		data = r.json()
		print data
		print data.get('InvoiceNumber')
		self.fbr_invoice_number=data.get('InvoiceNumber')
		# json_formatted_str = json.dumps(data, indent=2)
 	# 	print json_formatted_str
		  
		# "BuyerNTN":"1234567-8",
		# "BuyerCNIC":"12345-1234567-8",
		# "BuyerName":self/,
		# "BuyerPhoneNumber":"0000-0000000",

class Companyfbrdetails(models.Model):
    _inherit = 'res.company'

    pctcode = fields.Char("PCTCode")
    posid = fields.Char("POSID")
    usin = fields.Char("USIN")
    local_ip = fields.Char("localhost")


# class FbrPosIntegration(models.Model):
#     _inherit = 'sale.order'

#     fbr_invoice_number = fields.Char("FBR Invoice#")