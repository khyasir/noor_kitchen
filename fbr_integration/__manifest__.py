# -*- coding: utf-8 -*-
{
    'name': "FBR Integration",

    'summary': """
       FBR POS CONFIGURATION""",

    'description': """
            The new ECUBE FBR POS Integration for YOUR POS .FBR to POS integration is compulsory for all retailers,irrespective of the items they are selling or dealing in. All retailers across Pakistan, whether they are dealing in footwear, textile or grocery, or leather items, or any other items are required by Pakistan law to integrate their POS systems with FBR's system. 

   
    """,

   
    'author': "Hamza Azeem Qureshi ,Enterprise Cube (pvt) ltd",
    'website': "https://ecube.pk",
    'sequence': '10',
    'category': 'FBR',
    'version': '0.1',
 

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list


    # any module necessary for this one to work correctly
    'depends': ['base','sale','so_extension'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}