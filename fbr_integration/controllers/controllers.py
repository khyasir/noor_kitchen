# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import json
from openerp import models, fields, api
import datetime as dt
from openerp.exceptions import Warning
import xml.etree.ElementTree as ET

import pandas as pd
import numpy as np
import pandas.io.sql as psql
from operator import itemgetter

class FbrinvoiceController(http.Controller):

	@http.route('http://localhost:8524/api/IMSFiscal/GetInvoiceNumberByModel',type='json', auth='user')
	def get_demand_records(self, **kw):
		demand_list = []
		demand_records = request.env['demand.note.tree'].search([('link_it','!=',False),('balance_qty','>',0)])
		for records in demand_records:
			demand_list.append({
				'product_id':records.product_id.id,
				'product_name':records.product_id.name,
				'requisite_qty':records.local_qty,
				'ordered_qty':records.order_qty,
				'remaining_qty':records.balance_qty,
				'type':records.p_type,
				'record_id':records.id,
				"diss_type_options": [{"key":"no_diss","value": "No Discount"},{"key":"percentage", "value": "Discount Percentage"},{"key":"fixed_amount","value": "Fixed Amount"}],
				})

		return demand_list

	@http.route('/getVendors/',type='json', auth='user')
	def get_vendor_records(self, **kw):
		vendor_list = []
		vendor_records = request.env['res.partner'].search([('loc_supplier','=',True)])
		for records in vendor_records:
			vendor_list.append({
				'vendor_id':records.id,
				'vendor_name':records.name,
				})

		return vendor_list

	# createSalesOrder


	# mobile_no
	
	@http.route('/getCustomers/',type='json', auth='user')
	def get_customers_records(self, **kw):


		customer_list = []
		customer_records = request.env['res.partner'].search([('customer','=',True)])
		car_make = request.env['product.attribute.value'].search([])
		car_make_list=[{'id':idx['id'],'display_name,':idx['display_name'],}for idx in car_make]
		model_year = request.env['product.attribute.value'].search([])
		model_year_list=[{'id':idx['id'],'display_name,':idx['display_name'],}for idx in car_make]


		for records in customer_records:
			# vehicle_detail=[{'date':idx['date'],'car_make':idx['car_make'],'model_years':idx['model_years'],'registration':idx['registration'],'name':idx['the_display'],}for idx in records.vehical_tree]
				
			customer_list.append({
				'customer_id':records.id,
				'customer_name':records.name,
				'mobile_no': records.mobile1,
				'vehicle_detail':[{'id':idx['id'],'name': idx['the_display']}for idx in records.vehical_tree]
				})

		data={
		'customer_list':customer_list,
		'car_make':car_make_list,
		'model_year':model_year_list

		}
		return data

	@http.route('/createCustomer/',type='json', auth='user')
	def create_customer(self, **kw):						
		customer_record = request.env['res.partner'].create({
			'customer':True,
			'name':(kw.get('customer_name')),
			'mobile1':(kw.get('mobile_no')),			
			})
		data = {'status':200,'response':customer_record.id, 'message':"Customer Created Successfully"}

		# {
		# 	"jsonrps":2.0,"params":{"demand_id":"1","vendor_id":"1","rate":"100","qty":"2"}
		# } ,

		return data

	"""TODO: add sale order lines data, cars list"""
	@http.route('/getOrders/', type='json', auth='user')
	def get_sale_orders(self, **kw):
		orders = []
		records_data = request.env['sale.order'].search(
			[('pos_state', '=', 'draft')])
		for records in records_data:
			orders.append({
				'id': records.id,
				'mobile_no': records.p_no,
				'customer_name': records.c_name,
				'partner_id': records.partner_id.id,
				'vehical_info':records.vehical_info.id,
				'vehical_detail':records.vehical_info.the_display,
				'grand_total':records.grand_of_total,
				'order_line':[{'line_id':idx['id'],'product_id':idx['product_id'].id,'product':idx['product_id'].name,'product_uom_qty': idx['product_uom_qty'],'labour': idx['labour'],'scanning': idx['scanning'],'scan_qty': idx['scan_qty'],'discount':idx['discount'],'price_subtotal':idx['price_subtotal']}for idx in records.order_line]

						
			})
		return orders

	"""TODO: Return Sale Order Lines against the request. Kamran"""
	@http.route('/getOrderLines/', type='json', auth='user')
	def get_sale_order_lines(self, **kw):
		lines = []
		records_data = request.env['sale.order'].search(
			[('id', '=', int(kw.get('order_id')))])
		for records in records_data.order_line:
			orders.append({
				'line_id':idx['id'],
				'product_id':idx['product_id'].id,
				'product':idx['product_id'].name,
				'product_uom_qty': idx['product_uom_qty'],
				'labour': idx['labour'],
				'scanning': idx['scanning'],
				'scan_qty': idx['scan_qty'],
				'discount':idx['discount'],
				'price_subtotal':idx['price_subtotal']
			})
		grand_total = records_data.grand_of_total

		final_record = {
			'total': grand_total,
			'lines': lines
		}

		return final_record

	@http.route('/CreateOrder/', type='json', auth='user')
	def update_sale_orders(self, **kw):
		records_data = request.env['sale.order'].create({
			'p_no':int(kw.get('mobile_no')),
			'c_name':str(kw.get('customer_name')),
			'partner_id':int(kw.get('partner_id')),
			'is_pos':True,
		})

		return records_data.id
	
	"""TODO: update sale order """
	@http.route('/editOrderLine/', type='json', auth='user')
	def update_sale_ordersline(self, **kw):

		line_id = kw.get('line_id')
		records_data = request.env['sale.order.line'].search(
			[('id', '=',line_id )])
			
		if kw.get('product_id'):
			records_data.product_id = int(kw.get('product_id'))
		if kw.get('scan_qty'):
						
			records_data.scan_qty = int(kw.get('scan_qty'))
		if kw.get('discount'):
			
			records_data.discount = int(kw.get('discount'))
		
		if kw.get('labour'):
			records_data.labour = int(kw.get('labour'))
		if kw.get('product_uom_qty'):
		
			records_data.product_uom_qty = int(kw.get('product_uom_qty'))
		if kw.get('price_subtotal'):	
			records_data.price_subtotal = int(kw.get('price_subtotal'))
		
		records_data.OnChangingTheProduct()
		records_data._compute_amount()
		records_data.FittingStatus()
		records_data.UpdateingtheScanQty()
		
		return records_data.id

	@http.route('/removeOrderLine/', type='json', auth='user')
	def remove_sale_ordersline(self, **kw):
		
		line_id = kw.get('line_id')
		records_data = request.env['sale.order.line'].search(
			[('id', '=',line_id )])
			
		records_data.unlink()
		
		return records_data.id
	
	@http.route('/getPurchaseLines/',type='json', auth='user')
	def get_purchase_lines(self, **kw):
		demand_line_id = int(kw.get('demand_id'))

		purchase_lines = []
		purchase_line_records = request.env['demand.note.tree'].search([('id','=',demand_line_id)])
		for records in purchase_line_records.import_tree:
			purchase_lines.append({
				'vendor_id':records.vendor.id,
				'vendor_name':records.vendor.name,
				'rate':records.rate,
				'qty':records.qty,
				'diss_type': dict(records._fields['diss_type'].selection).get(records.diss_type),
				'discount_value':records['discount_value'],
				"diss_type_options": [{"key":"no_diss","value": "No Discount"},{"key":"percentage", "value": "Discount Percentage"},{"key":"fixed_amount","value": "Fixed Amount"}],
				'total':records.total,
				'record_id':records.id,
			})

		return purchase_lines

	@http.route('/createPurchaseLines/',type='json', auth='user')
	def create_purchase_lines(self, **kw):
		demand_line_id = int(kw.get('demand_id'))
		purchase_line_records = request.env['demand.note.tree'].search([('id','=',demand_line_id)])
		demand_for_vendors_id = purchase_line_records.id
		vendor_line_records = request.env['demand.vendors.tree'].create({
			'vendor':int(kw.get('vendor_id')),
			'rate':float(kw.get('rate')),
			'qty':int(kw.get('qty')),
			'diss_type':kw.get('diss_type'),
			'discount_value':float(kw.get('discount_value')),
			'import_link_demand':demand_for_vendors_id,
			})
		vendor_line_records._onchange_vendors_id()
		vendor_line_records.get_total()
		# vendor_line_records.effect_of_discount()

		purchase_line_records.save_rec()
		request.env['create.demand.lines'].create_po()

		data = {'status':200,'response':vendor_line_records.id, 'message':"Record Created Successfully"}

		return data

	@http.route('/editPurchaseLines/',type='json', auth='user')
	def edit_purchase_lines(self, **kw):
		record_id = int(kw.get('record_id'))
		vendor_line_records = request.env['demand.vendors.tree'].search([('id','=',record_id)])
		vendor_line_records.vendor = int(kw.get('vendor_id'))
		vendor_line_records.qty = int(kw.get('qty'))
		vendor_line_records.diss_type = kw.get('diss_type')
		vendor_line_records.discount_value = float(kw.get('discount_value'))

		vendor_line_records.rate = int(kw.get('rate'))

		vendor_line_records._onchange_vendors_id()
		vendor_line_records.get_total()
		# vendor_line_records.tree_link.remain_qty()

		data = {'status':200,'response':vendor_line_records.id, 'message':"Record Updated Successfully"}

		return data

	@http.route('/getMainMenus/',type='json', auth='user')
	def getMainMenus(self, **kw):
		
		menuitems = request.env['ir.ui.menu'].sudo().search([('parent_id','=',False)],order='sequence')

		menus = [{'name':idx['name'],'sequence':idx['sequence'],'icon':idx['icon_image'],'id':idx['id'],}for idx in menuitems]

		data = {'status':200,'response':menus, 'message':"Success"}
		return data
	
	@http.route('/getSubMenus/',type='json', auth='user')
	def getSubMenus(self, **kw):
		menu_id = kw.get('menu_id')		

		menuitems_no_action = request.env['ir.ui.menu'].sudo().search([('parent_id.id','=',int(menu_id)),('action','=',False)],order='sequence')
		submenus = [{'name':idx['name'],'sequence':idx['sequence'],'id':idx['id'],'action':str(idx['action']),'model':False,}for idx in menuitems_no_action ]

		menuitems_with_action = request.env['ir.ui.menu'].sudo().search([('parent_id.id','=',int(menu_id)),('action','!=',False)],order='sequence')

		model_action = [{'name':idx['name'],'sequence':idx['sequence'],'id':idx['id'],'action':str(idx['action']),'model':idx['action'].type,'model':idx['action'].res_model,}for idx in menuitems_with_action if idx['action'].type == 'ir.actions.act_window']

		menus_list = submenus + model_action
		
		data = {'status':200,'response':sorted(menus_list, key = lambda i: (i['sequence'])), 'message':"Success"}
		return data

	@http.route('/createRecord/',type='json', auth='user')
	def create_data(self,model_name,**kw):
		if kw.get('model_name') == True:
			model_name = kw.get('model_name')


		all_fields = kw.get('data')
		configs =request.env['mobile.app.config'].sudo().search([('model_id.model','=',model_name)])
		if not configs:
			data = {'status':402,'response':"The view is not available. Please contact your system administrator ", 'message':"Success"}

		print configs
		if configs:
			for setting in configs:
				# id = models.execute_kw(setting.model_id.model, 'create', [all_fields])
				# id = request.env(setting.model_id.model, 'create', [all_fields])
				the_data =request.env[setting.model_id.model].sudo().create(all_fields)
				print the_data
			
				# request.env.cr.commit()
			data = {'status':200,'response':the_data.id,'message':"Success"}
		return data

	@http.route('/editRecord/',type='json', auth='user')
	def edit_data(self,model_name,**kw):
		if kw.get('model_name') == True:
			model_name = kw.get('model_name')


		rec_id = kw.get('id')
		all_fields = kw.get('data')
		configs =request.env['mobile.app.config'].sudo().search([('model_id.model','=',model_name)])
		if not configs:
			data = {'status':402,'response':"The view is not available. Please contact your system administrator ", 'message':"Success"}

		print configs
		if configs:
			for setting in configs:
				# id = models.execute_kw(setting.model_id.model, 'create', [all_fields])
				# id = request.env(setting.model_id.model, 'create', [all_fields])
				the_data =request.env[setting.model_id.model].sudo().search([('id', '=', rec_id)])
				the_data.write(all_fields)
			
				# request.env.cr.commit()
			data = {'status':200,'response':the_data.id,'message':"Success"}
		return data
	@http.route('/callButton/',type='json', auth='user')
	def call_buttons(self,model_name,**kw):
		if kw.get('model_name') == True:
			model_name = kw.get('model_name')
		rec_id = kw.get('id')

		function = kw.get('function')

		data="data=request.env['"+str(model_name)+"'].sudo().search([('id', '=',"+str(rec_id)+")])\n"+"data."+str(function)+"()"
		exec(data)
		data = {'status':200,'response':"Button Pressed",'message':"Success"}
		return data

	@http.route('/barcodecheck/',type='json', auth='user')
	def checkProduct(self, barcode,**kw):
		if kw.get('barcode'):
			 barcode= kw.get('barcode')

		product_record =request.env['product.product'].sudo().search([('barcode','=',barcode)])

		product_data = [{'second_name':idx['second_name'],'image':idx['image_medium'],'lst_price':idx['lst_price'],'list_price':idx['list_price_own'],'disscount_value':idx['disscount_value'],'warrenty':idx['warrenty'],'SKU':idx['inetrnal_seq'],'uom':dict(idx._fields['uom'].selection).get(idx.uom),'barcode':idx['barcode'],'labour_charges':idx['labour_charges'],'paint_charges':idx['paint_charges'],'total_prod_qty':request.env['product.location.stock'].sudo().search([("location_id",'=',15),("stock_location_tree",'=',idx['id'])]).quantity,'shop_shelf':idx['shop_shelf']}  for idx in product_record ]

		data = {'status':200,'response':product_data,'message':"Success"}
		return data

	@http.route('/getsolines/',type='json', auth='user')
	def GetSoLines_mark(self, **kw):

		if kw.get('so_id'):
			so_id = kw.get('so_id')

			data = "data=request.env['sale.order'].sudo().search([('id', '='," + str(so_id) + ")])"
			exec(data)

			lines = []

			for records in data.order_line:
				lines.append({
					'line_id': records['id'],
					'product_id': records['product_id'].id,
					'product': records['product_id'].name,
					'product_uom_qty': records['product_uom_qty'],
					'labour': records['labour'],
					'scanning': records['scanning'],
					'scan_qty': records['scan_qty'],
					'discount': records['discount'],
					'price_subtotal': records['price_subtotal']
				})
			grand_total = data.grand_of_total 

			final_record = {
				'total': grand_total,
				'lines': lines
			}

			data = {'status':200,'response':final_record,'message':"Success"}
			return data

		else:
			return False


	@http.route('/sendToScanning/',type='json', auth='user')
	def send_to_scanning(self,**kw):
		so_id = kw.get('so_id')
		sale_order = request.env['sale.order'].browse(int(so_id))
		sale_order.scan_it()

		data = {'status':200,'response':so_id,'message':"Success"}
		return data

	@http.route('/sendForDispatch/',type='json', auth='user')
	def send_for_dispatch(self,**kw):
		po_id = kw.get('id')
		sale_order = request.env['stock.picking'].browse(po_id)
		sale_order.dispatch_bool=True

		data = {'status':200,'response':po_id,'message':"Success"}
		return data




	@http.route('/barcodescan/',type='json', auth='user')
	def callbarcodescan(self, barcode,**kw):
		
		if kw.get('barcode'):
			 barcode= kw.get('barcode')

		qty = 0
		if kw.get('quantity'):
			qty = float(kw.get('quantity'))

		so_id = kw.get('so_id')

		# sales=request.env['sale.order'].sudo().search([('id', '=',int(so_id))]).scan_it()

		data = "data=request.env['sale.order'].sudo().search([('id', '=',"+str(so_id)+")])\n"+"data."+str("so_barcode")+"(barcode, so_id, 'scan')"
		exec(data)

		product_one = request.env['product.product'].search([('barcode', '=', barcode)])

		if not product_one:
			product_two = request.env['product.product'].search([('second_barcode', '=', barcode)])
			if not product_two:
				raise ValidationError("Product Not Found.")
			else:
				product_id = product_two
		else:
			product_id = product_one

		order_line = request.env['sale.order.line'].search([('order_id','=',data.id),('product_id','=',product_id.id)])

		if qty > 0:
			order_line.product_uom_qty += (qty - 1.0)
		
		lines = []
		records_data = request.env['sale.order'].search(
			[('id', '=', data.id)])
		for records in records_data.order_line:
			lines.append({
				'line_id': records['id'],
				'product_id': records['product_id'].id,
				'product': records['product_id'].name,
				'product_uom_qty': records['product_uom_qty'],
				'labour': records['labour'],
				'scanning': records['scanning'],
				'scan_qty': records['scan_qty'],
				'discount': records['discount'],
				'price_subtotal': records['price_subtotal']
			})
		grand_total = records_data.grand_of_total

		final_record = {
			'total': grand_total,
			'lines': lines
		}

		data = {'status':200,'response':final_record,'message':"Success"}
		return data

	@http.route('/deleteRecord/',type='json', auth='user')
	def delete_data(self,model_name,**kw):
		if kw.get('model_name') == True:
			model_name = kw.get('model_name')


		rec_id = kw.get('id')
		# all_fields = kw.get('data')
		configs =request.env['mobile.app.config'].sudo().search([('model_id.model','=',model_name)])
		if not configs:
			data = {'status':402,'response':"The view is not available. Please contact your system administrator ", 'message':"Success"}

		print configs
		if configs:
			for setting in configs:
				# id = models.execute_kw(setting.model_id.model, 'create', [all_fields])
				# id = request.env(setting.model_id.model, 'create', [all_fields])
				the_data =request.env[setting.model_id.model].sudo().search([('id', '=', rec_id)])
				the_data.unlink()
			
				# request.env.cr.commit()
			data = {'status':200,'response':the_data.id,'message':"Success"}
		return data

	@http.route('/getRecords/',type='json', auth='user')
	def getRecords(self,model_name, **kw):
		# description= kw.get('description')
		if kw.get('model_name') == True:
			model_name = kw.get('model_name')
		menu_id = kw.get('menu_id')
		if menu_id:
			configs =request.env['mobile.app.config'].sudo().search([('menu_id.id','=',menu_id)])
		else:
			configs =request.env['mobile.app.config'].sudo().search([('model_id.model','=',model_name)])
		if not configs:
			data = {'status':402,'response':"The view is not available. Please contact your system administrator ", 'message':"Success"}

		print configs
		if configs:
			liste = []
			view=request.env['ir.ui.view'].search([('model','=',model_name)])
			for rec in view:
				# print rec._apply_group(model, node, modifiers, fields)
				# print rec
				root = ET.fromstring(rec.arch_base)
				print root 
				for node in root:
					print node.tag
					if node.tag == 'header' or  node.tag == 'xpath' :

						if node.get('expr') != None:
							if node.get('expr').find('//header') != -1:
	

								for data in node.iter('xpath'):
									# print "OLO"

									for n in  data.findall('button'):
										# print "LOL"
										liste.append({
											'function':n.get("name"),
											'attrs':n.get("attrs"),
											'model':model_name,
											'name':n.get("string"),
											})

						for x in node.iter('header'):
							for n in  x.findall('button'):
								liste.append({
											'function':n.get("name"),
											'attrs':n.get("attrs"),
											'model':model_name,
											'name':n.get("string"),
											})


			seen = set()
			buttons = []
			for d in liste:
				t = tuple(d.items())
				if t not in seen:
					seen.add(t)
					buttons.append(d)

			print buttons
			# raise ValidationError("	lolo")
			for setting in configs:
				table_name = setting.model_id.model.replace('.','_')
				all_fields = [line.field_id.name for line in setting.line_ids if line.display ==True]

				"""fields Types"""
				field_types = []

				field_types=[{'key':lines.field_id.name,'tree':lines.tree_view,'form':lines.form_view,'kanban':lines.kanban,'related_model':lines.field_id.relation,'type':lines.field_id.ttype,'name':lines.name.replace(' ','_').replace('(','_').replace(')','_').replace("'",'_').replace('#','_').lower()}for lines in setting.line_ids
					if lines.display == True]

				fields_names = [line.name.replace(' ','_').replace('(','_').replace(')','_').replace("'",'_').replace('#','_')for line in setting.line_ids if line.display ==True ]
				# button_names = [{'name':line.name,'function':line.function}for line in setting.line_button_ids if line.display ==True ]

				many2ones_list= filter(lambda Many2ones: Many2ones['type'] == 'many2one',field_types )
				
				one2manys_list= filter(lambda One2manys: One2manys['type'] == 'one2many',field_types )

				# print table_name
				# print all_fields
				# print fields_names
				if len(all_fields) == len(fields_names):

					var = 0
					string = ""
					for x in all_fields:
						string += (str(x) + ' AS ' + str(fields_names[var])) if string == '' else (', ' + str(x) + ' AS ' + str(fields_names[var]))

						var += 1

					# the_query = request.env.cr.execute("SELECT %s FROM %s" % (string,table_name))
					# the_query = request.env.cr.dictfetchall()
					the_data =request.env[setting.model_id.model].sudo().search_read([], all_fields)
					print the_data
					print "======================"
					# print request.env['ir.ui.view']._apply_group(setting.model_id.model,node)
					print "======================"
					print "======================"
					# print the_query

					# print the_query

				else:
					raise ValidationError("No of fields and their names are not equal")
			many2one=self.get_many2ones(many2ones_list)
			one2many=self.get_one2manys(one2manys_list)

			print "--------------------------"
			print field_types
			data = {'status':200,'response':the_data,'button':buttons,'fields':all_fields,'many2one':many2one,'one2many':one2many,'field_type':field_types, 'message':"Success"}
		return data



	@http.route('/getCustomersDetail/',type='json', auth='user')
	def getCustomersDetail(self,number, **kw):
		# description= kw.get('description')
		if kw.get('number') == True:
			number = kw.get('number')
			pass
		# request.env['sale.order'].
		# menu_id = kw.get('menu_id')
		# if menu_id:

	@http.route('/getdata/',type='json', auth='user')
	def get_data(self,model):
		# field= *
		# var = 0
		# string = ""
		# for x in fields:
		# 	string += (str(x) + ' AS ' + str(fields_names[var])) if string == '' else (', ' + str(x) + ' AS ' + str(fields_names[var]))

		# 	var += 1
		print model
		model_data =request.env[(model)].sudo().search([])
		model_data_list= [{'id':data.id,'name':data.name_get()[0][1] }for data in model_data]
		# model_data = request.env.cr.execute("SELECT id AS id FROM %s" % (model))
		# model_data = request.env.cr.dictfetchall()
	

		return model_data_list

	

	def get_many2ones(self,many2ones_list):

		# all_many2ones_records={}

		# for many2one in many2ones_list:
		# model_related = many2one['related_model']
		# for many2one in many2ones_list:
		# 	all_many2ones_records.update({many2one['name']:self.get_data(many2one['related_model'])})
		all_many2ones_records={many2one['name']:self.get_data(many2one['related_model']) for many2one in many2ones_list}
		# for many2one in many2ones_list:
		# 	all_many2ones_records.update({many2one['name']:self.get_data(many2one['related_model'])})

			# model_related = many2one['related_model'].replace('.','_')
			# self.get_data(model_related)

			# string += (str(many2one) + ' AS ' + str(many2ones_list[var])) if string == '' else (', ' + str(many2one) + ' AS ' + str(many2ones_list[var]))

			# var += 1
		# print many2one
		print "all_many2ones_records"
		print all_many2ones_records
		return all_many2ones_records
	

	def get_one2manys(self,one2manys_list):

		# all_one2manys_records={one2many['name']:self.get_data_o2m(one2many['related_model']) for one2many in one2manys_list}
		print "all_one2manys_records"
		print one2manys_list
		print one2manys_list
		print one2manys_list

		all_one2manys_records={one2many['name']:self.getRecords(one2many['related_model']) for one2many in one2manys_list}


		print all_one2manys_records
		return all_one2manys_records
		print "all_one2manys_records"
		print "all_one2manys_records"
		print "all_one2manys_records"
		# print all_one2manys_records
		# return all_one2manys_records
		# the_query = request.env.cr.execute("SELECT %s FROM %s" % (string,table_name))
		# the_query = request.env.cr.dictfetchall()

	@http.route('/getPurchaseOrders/', type='json', auth='user')
	def get_PurchaseOrders(self, **kw):
		# records_data = request.env['purchase.order'].search(
			# [('state', '!=', 'complete')])

		orders = [{
				'name': records.name,
				'id': records.id,
				'partner_id': records.partner_id_loc.id,
				'customer_name': records.partner_id_loc.name,
				'picking_type_id': records.picking_type_id.id,
				'picking_type_name': records.picking_type_id.name,
				'date_order':records.date_order,
				'date_planned':records.date_planned,
				'vendor_bill_id':records.vendor_bill_id,
				'demand_ref':records.demand_ref.dm_no,
				'total_amount':records.ecube_total_amount,
				'state': dict(records._fields['state'].selection).get(records.state),
				'discount_amount':records.ecube_total_discount,
				'shipping_way':dict(records._fields['shipping_way'].selection).get(records.shipping_way),
				'order_line':[{'line_id':idx['id'],'product_id':idx['product_id'].id,'product':idx['product_id'].name,'product_qty': idx['product_qty'],'uom': dict(idx._fields['uom'].selection).get(idx.uom),'qty_hand': idx['qty_hand'],'product_qty': idx['product_qty'],'net_unit_price': idx['net_unit_price'],'unit_price': idx['ecube_unit_price'],'diss_type': dict(idx._fields['diss_type'].selection).get(idx.diss_type),'discount_value':idx['discount_value'],'price_subtotal':idx['ecube_subtotal']}for idx in records.order_line]}for records in request.env['purchase.order'].sudo().search([('state', 'not in', ['complete','cancel'])])]
		return orders
		# for records in records_data:
		# 	orders.append({
  #  				'name': records.name,
  #  				'id': records.id,
  #               'partner_id': records.partner_id_loc.id,
  #               'customer_name': records.partner_id_loc.name,
  #               'picking_type_id': records.picking_type_id.id,
  #               'picking_type_name': records.picking_type_id.name,
  #               'date_order':records.date_order,
  #               'date_planned':records.date_planned,
  #               'vendor_bill_id':records.vendor_bill_id,
  #               'demand_ref':records.demand_ref.dm_no,
  #               'total_amount':records.ecube_total_amount,
  #               'state': dict(records._fields['state'].selection).get(records.state),
  #               'discount_amount':records.ecube_total_discount,
  #               'shipping_way':dict(records._fields['shipping_way'].selection).get(records.shipping_way),
  #               'order_line':[{'line_id':idx['id'],'product_id':idx['product_id'].id,'product':idx['product_id'].name,'product_qty': idx['product_qty'],'uom': dict(idx._fields['uom'].selection).get(idx.uom),'qty_hand': idx['qty_hand'],'product_qty': idx['product_qty'],'net_unit_price': idx['net_unit_price'],'unit_price': idx['ecube_unit_price'],'diss_type': dict(idx._fields['diss_type'].selection).get(idx.diss_type),'discount_value':idx['discount_value'],'price_subtotal':idx['ecube_subtotal']}for idx in records.order_line]

						
  #           })

	@http.route('/confirmPurchaseOrder/',type='json', auth='user')
	def confirmPObuttons(self,**kw):
		rec_id = kw.get('id')

		record=request.env['purchase.order'].sudo().search([('id', '=',rec_id)])
		record.button_confirm()
		orders=[{
				'name': record.name,
				'id': record.id,
				'partner_id': record.partner_id_loc.id,
				'customer_name': record.partner_id_loc.name,
				'picking_type_id': record.picking_type_id.id,
				'picking_type_name': record.picking_type_id.name,
				'date_order':record.date_order,
				'date_planned':record.date_planned,
				'vendor_bill_id':record.vendor_bill_id,
				'demand_ref':record.demand_ref.dm_no,
				'total_amount':record.ecube_total_amount,
				'state': dict(record._fields['state'].selection).get(record.state),
				'discount_amount':record.ecube_total_discount,
				'shipping_way':dict(record._fields['shipping_way'].selection).get(record.shipping_way),
				'order_line':[{'line_id':idx['id'],'product_id':idx['product_id'].id,'product':idx['product_id'].name,'product_qty': idx['product_qty'],'uom': dict(idx._fields['uom'].selection).get(idx.uom),'qty_hand': idx['qty_hand'],'product_qty': idx['product_qty'],'net_unit_price': idx['net_unit_price'],'unit_price': idx['ecube_unit_price'],'diss_type': dict(idx._fields['diss_type'].selection).get(idx.diss_type),'discount_value':idx['discount_value'],'price_subtotal':idx['ecube_subtotal']}for idx in record.order_line]}]
		# return orders
		
		data = {'status':200,'response':orders,'message':"Success"}
		return data


	@http.route('/discountPurchaseOrder/',type='json', auth='user')
	def discountPurchaseOrder(self,**kw):
		rec_id = kw.get('id')
		discount_amount = kw.get('discount_amount')

		record=request.env['purchase.order'].sudo().search([('id', '=',rec_id)])
		record.write({'fixed_discount':float(discount_amount)})
		record.get_shippment_weight()

		order=[{
				'name': record.name,
				'id': record.id,
				'partner_id': record.partner_id_loc.id,
				'customer_name': record.partner_id_loc.name,
				'picking_type_id': record.picking_type_id.id,
				'picking_type_name': record.picking_type_id.name,
				'date_order':record.date_order,
				'date_planned':record.date_planned,
				'vendor_bill_id':record.vendor_bill_id,
				'demand_ref':record.demand_ref.dm_no,
				'total_amount':record.ecube_total_amount,
				'state': dict(record._fields['state'].selection).get(record.state),
				'discount_amount':record.ecube_total_discount,
				'shipping_way':dict(record._fields['shipping_way'].selection).get(record.shipping_way),
				'order_line':[{'line_id':idx['id'],'product_id':idx['product_id'].id,'product':idx['product_id'].name,'product_qty': idx['product_qty'],'uom': dict(idx._fields['uom'].selection).get(idx.uom),'qty_hand': idx['qty_hand'],'product_qty': idx['product_qty'],'net_unit_price': idx['net_unit_price'],'unit_price': idx['ecube_unit_price'],'diss_type': dict(idx._fields['diss_type'].selection).get(idx.diss_type),'discount_value':idx['discount_value'],'price_subtotal':idx['ecube_subtotal']}for idx in record.order_line]}]
		data = {'status':200,'response':order,'message':"Success"}
		return data

	@http.route('/getbackorders/',type='json', auth='user')
	def get_back_orders(self, **kw):


		list_lines = []
		picking_records = request.env['stock.picking'].search([('type','=','receipts'),('backorder_id','!=',False),('state','not in',['cancel','done'])])
		
		for records in picking_records:
			# vehicle_detail=[{'date':idx['date'],'car_make':idx['car_make'],'model_years':idx['model_years'],'registration':idx['registration'],'name':idx['the_display'],}for idx in records.vehical_tree]
				
			list_lines.append({
				'id':records.id,
				'name':records.name,
				'partner_id':records.partner_id.id,
				'partner_name':records.partner_id.name,
				'origin': records.origin,
				'create_date': records.create_date,
				'order_line':[{'id':idx['id'],'product_id': idx['product_id'].id,'product_name': idx['product_id'].name,'product_qty': idx['product_qty']}for idx in records.pack_operation_product_ids]
				})
		return list_lines


		# class ControllerTest(http.Controller):
#     @http.route('/controller_test/controller_test/', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('/controller_test/controller_test/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('controller_test.listing', {
#             'root': '/controller_test/controller_test',
#             'objects': http.request.env['controller_test.controller_test'].search([]),
#         })

#     @http.route('/controller_test/controller_test/objects/<model("controller_test.controller_test"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('controller_test.object', {
#             'object': obj
#         })
