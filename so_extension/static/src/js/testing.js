odoo.define('so_extension.testing', function (require){
"use strict";
var form_widget = require('web.form_widgets');
var core = require('web.core');
var _t = core._t;
var QWeb = core.qweb;
var Model = require('web.Model');
var session = require('web.session');

form_widget.WidgetButton.include({
    on_click: function() {
         if(this.node.attrs.custom === "click"){
            (new Model('sale.order')).call('prods').then(function (products) { 
         console.log(products);
         data=products;
         setData();
    });



            
            return;
         }
         this._super();
    },
});
var data;
var items = '';
var cartObject = {}
            //////////////////
            /// page ready all JS work after that page
            //////////////////
function setData(){
            var markup = '';
            
            function addCategories(data) {
                $.each(data, function(key, value) {
                    console.log(key + ": " + value);
                    markup += '<td><button class="catbutton" data-id = ' + key + ' >' + key + '</button></td>';
                    $.each(value, function(key, v) {
                        items += '<button class="itemButton" data-price=' + v.price + ' data-sr=' + v.sr + ' data-name=' + key + ' class="ui-state-default">' + key + '</button>';
                    });
                });

				
					$('#categories').append(markup);
				    $('#items').append(items);
				
                
            }
            addCategories(data);
            setItemsEvent();
            //// event on category buttons
            $('.catbutton').each(function() {
                var $this = $(this);
                $this.on("click", function() {
                    console.log('hi');
                    console.log(data[$(this).data('id')]);
                    showCategoryItems(data[$(this).data('id')]);

                });
            });

          
            // $disc = $('.disc');

            // $disc.on('change', () => {
            //     $(".netAmount").val(calculatePrice($disc.val(), $(".tAmount").val()));
            // });

};
function showCategoryItems(value) {
    $('#items').html('');
    $.each(value, function(key, v) {
        // items += 
        $('#items').append('<button class="itemButton" data-price=' + v.price + ' data-sr=' + v.sr + ' data-name=' + key + ' class="ui-state-default">' + key + '</button>');


    });
    setItemsEvent();
};
function calculatePrice(percentage, price) {

    var calcPrice = price - ((price / 100) * percentage),
        discountPrice = calcPrice.toFixed(2);
    return discountPrice;
}
function addListItem() {
    $('.cartTable').html('<thead><tr><th>    Serial No    </th><th>    Item    </th><th>     Quantity     </th><th>     Amount    </th></tr></thead>');
    $.each(cartObject, function(sr, value) {
        $('.cartTable').append('<tr class = "listItem" ><td>' + sr + ' </td> <td> ' + cartObject[sr].name + ' </td> <td> <input type="number" value=' + cartObject[sr].count + '  pattern="[0-9]" name="itemCount"></td> <td> ' + cartObject[sr].price + ' </td></tr>');
    });
};
function setItemsEvent() {
                $('.itemButton').each(function() {
                    var $this = $(this);
                    $this.on("click", function() {

                        if (cartObject.hasOwnProperty($(this).data('sr'))) {
                            cartObject[$(this).data('sr')].count = cartObject[$(this).data('sr')].count + 1;
                            cartObject[$(this).data('sr')].price = cartObject[$(this).data('sr')].price + $(this).data('price');
                        } else {
                            cartObject[$(this).data('sr')] = {
                                'price': $(this).data('price'),
                                'name': $(this).data('name'),
                                'count': 1
                            }
                        };
                        addListItem();
                        (new Model('sale.order')).call('update_stocks',[$(this).data('price'),$(this).data('name')]);
                        var amountTotal = $(".tAmount").val();
                        amountTotal = parseInt(amountTotal) + parseInt($(this).data('price'));
                        $(".tAmount").val(amountTotal);
                        var discountPer = $(".disc").val();
                        var netPrice = calculatePrice(discountPer, amountTotal)
                        $(".netAmount").val(netPrice);
                    });
                });
};

	$(document).ready(function() {
		data = {
	                'parathas': {
	                    'nautela paratha': {
	                        'sr': '1',
	                        'price': 210
	                    },
	                    'bbq paratha': {
	                        'sr': '2',
	                        'price': 230,
	                    },
	                    'haramasala paratha': {
	                        'sr': '3',
	                        'price': 270,
	                    },
	                    'pizza paratha': {
	                        'sr': '4',
	                        'price': 250,
	                    },
	                    'meetha paratha': {
	                        'sr': '5',
	                        'price': 100,
	                    }
	                },
	                'wraps': {
	                    'pizza wrap': {
	                        'sr': '6',
	                        'price': '190'
	                    },
	                    'bbq wrap': {
	                        'sr': '7',
	                        'price': '180'
	                    },
	                    'malaiboti wrap': {
	                        'sr': '8',
	                        'price': '150'
	                    },
	                    'haramasala wrap': {
	                        'sr': '10',
	                        'price': '250'
	                    },
	                }
	};

		// setTimeout(function(){
		// 	$( ".bbttn" ).trigger( "click" );
		// },3000);
 		setTimeout(function(){
			$( ".bbttn" ).ready( function(){
				$( ".bbttn" ).trigger( "click" );
			});
		},3000);
	});


        // });
});







