odoo.define('so_extension.pro', function (require) {
"use strict";

var core = require('web.core');
var data = require('web.data');
var form_common = require('web.form_common');
var formats = require('web.formats');
var Model = require('web.DataModel');
var time = require('web.time');
var utils = require('web.utils');
var session = require('web.session');
var products = new Model('product.product');
var orders = new Model('sale.order');
var ActionManager = require('web.ActionManager');
  var Dialog = require('web.Dialog');
    var Widget = require('web.Widget');

var QWeb = core.qweb;
var _t = core._t;
var data;
var ProductsWidget = form_common.FormWidget.extend(form_common.ReinitializeWidgetMixin, {
    template: 'ProductsWidget',
    events: {
        "click #container": "product_clickeddd",
    },
    init: function() {
        this._super.apply(this, arguments);

    },
    start: function() {
        this._super();
        var self = this;




       // ===================================================
        var firstTime = true;
        orders.call('entity_waiter',[],{}).then(
            function(results) {
                var $select=document.getElementById("waiter");
                for (var val in results) {
                    
                    // var $option = '<option value="' + results[val].id + '">' + results[val].name + '</option>';
                   var o = new Option(results[val].name, results[val].id);
                   // $(o).html(results[val].name);
                    $select.append(o);
                }

                $("#waiter").on('change', function() {
                  
                    orders.call('add_waiter',[self.field_manager.datarecord.id,this.value],{}).then(
                        function(){
                        self.view.on_button_save();

                        }
                        );
                });
            });
        orders.call('entity_rider',[],{}).then(
            function(results) {
                var $select=document.getElementById("rider");
                for (var val in results) {
                    
                    // var $option = '<option value="' + results[val].id + '">' + results[val].name + '</option>';
                   var o = new Option(results[val].name, results[val].id);
                   // $(o).html(results[val].name);
                    $select.append(o);
                }

                $("#rider").on('change', function() {
                  
                    orders.call('add_rider',[self.field_manager.datarecord.id,this.value],{}).then(
                        function(){
                        self.view.on_button_save();

                        }
                        );
                });
            });

             // orders.call('so_type',[],{}).then(
             //            function(results) {
             //                    console.log('so_type returned ------ '+results);
             //                });
            
// var so_type = false;
//             if(so_type){
//                 document.getElementById("printingKot").style.display = "none";
//             }

        products.call('get_all_products',[],{}).then(
            function(results) {

               var sorted = {};

Object
    .keys(results).sort(function(a, b){
        console.log(a);
        console.log("=================================");
        console.log(b);
        return a.split('-')[1] - b.split('-')[1];
    })
    .forEach(function(key) {
        sorted[key] = results[key];
    });

// console.log(sorted);
                data = sorted;
                var allBtn = $(QWeb.render('CatWidget', {widget: "ALL"}));
                $(".cat-container").append(allBtn);
                        allBtn.click(
                            function(){
                                setAllCat(data);
                            }
                        );
                setAllData(sorted);
                
                        setevent();
            }
            );
        function setevent(){
                $(window).focus(function() {
                   $("#searchItems").val('');
                   if(document.getElementById('waiter'))
                        document.getElementById('waiter').value='None';
                   $("#change").val('');
                    $("#cash").val('');
                });
            $("#searchItems").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $(".product-container div").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)

                });
            });

            $("#cash").on("keyup", function() {
                var value = $(this).val();
                var totalValue = $('.totalAmount')[0].textContent.split('R')[0];
                console.log(value);
                console.log('============================');
                console.log(totalValue);
                var number = Number(totalValue.replace(/[^0-9.-]+/g,""));
                var final = value - number;
                 console.log(final);
                 if(value == 0 || value =='')
                 {
                    $("#change").val(0);
                 }else
                    $("#change").val(final);
                orders.call('cash_update',[self.field_manager.datarecord.id,value],{}).then(
                    function(){
                        self.view.on_button_save();

                    }
                    );
            });

        
           


       };

        

       
        
        function setAllCat(results){
             $(".product-container").html('');
                
             _.each(results, function(result,key){
                // var sorted = {};

                // Object
                // .keys(results).sort(function(a, b){
                //     console.log(a);
                //     console.log("=================================");
                //     console.log(b);
                //     return a.split('-')[0] - b.split('-')[0];
                // })
                // .forEach(function(key) {
                //     sorted[key] = results[key];
                // });

                _.each(result,function(item){
                        // console.log(results);
                    var button = $(QWeb.render('ProductWidget', {widget: item}));
                    $(".product-container").append(button);
                    button.click(
                        function() {
                            orders.call('add_product_line',[self.field_manager.datarecord.id,item.id],{}).then(
                            function(){
                            self.view.on_button_save();

                            }
                            );

                        }
                    );
                });
            });
        }
        function setAllData(results){
            _.each(results, function(result,key){
                        // console.log(results);
                            var catbutton = $(QWeb.render('CatWidget', {widget: key.split('-')[0]}));
                            $(".cat-container").append(catbutton);
                            catbutton.click(
                                function(){
                                    setItems(key);
                                }
                            );

                            _.each(result,function(item){
                                // console.log(results);
                            var button = $(QWeb.render('ProductWidget', {widget: item}));
                            $(".product-container").append(button);
                            button.click(
                                function() {
                                    orders.call('add_product_line',[self.field_manager.datarecord.id,item.id],{}).then(
                                    function(resultDeal){

                                    console.log("=================================");
                                    console.log(resultDeal);
                                    var pos = resultDeal.indexOf('deal'); // 0
                                    if ( pos !== -1 ) {
                                        console.log('Found at location ' + pos);
                                        var orderline_id = resultDeal.split('-')[1]
                                        var deal = resultDeal.split('-')[0]
                                        if(deal == 'deal' ){
                                            setwizard(item.id,orderline_id,self.field_manager.datarecord.id);
                                        }
                                    }
                                    self.view.on_button_save();

                                    }
                                    );

                                }
                            );
                        });
                           
                    });
        }

            function setItems(key){
                 
                $(".product-container").html('');
                console.log(data[key]);
                var sorted_array=[];
                _.each(data[key],function(item){
                    sorted_array.push(item);
                });
                sorted_array.sort(function(a, b){
                    console.log(a);
                    console.log("=================================");
                    console.log(b);
                    return b.name.split(' - ')[1] - a.name.split(' - ')[1];
                });

                _.each(sorted_array,function(item){

                    var button = $(QWeb.render('ProductWidget', {widget: item}));
                    $(".product-container").append(button);
                        button.click(
                            function() {
                                
                                orders.call('add_product_line',[self.field_manager.datarecord.id,item.id],{}).then(
                                function(resultDeal){
                                    console.log("=================================");
                                    console.log(resultDeal);
                                    var pos = resultDeal.indexOf('deal'); // 0
                                    if ( pos !== -1 ) {
                                        console.log('Found at location ' + pos);
                                        var orderline_id = resultDeal.split('-')[1]
                                        var deal = resultDeal.split('-')[0]
                                        if(deal == 'deal' ){
                                            setwizard(item.id,orderline_id,self.field_manager.datarecord.id);
                                        }
                                    }
                                    self.view.on_button_save();
                                });
                        }
                    );
                });
            }

            function setwizard(item,orderline_id,so){
                var action_manager = new ActionManager(self);
                var line_id = parseInt(orderline_id, 10);
                    action_manager.do_action({
                        'name': 'Deal Drinks',
                        type: 'ir.actions.act_window',
                        res_model: 'pos.deal.wizard',
                        view_mode: 'form',
                        view_type: 'form',
                        views: [[false, 'form']],
                        context: {'default_check':true,'default_product_id':item,'default_sale_id':so,'default_sale_order_line_id':line_id},
                        target: 'new',
                    });

            }
 
    },
    

});

core.form_custom_registry.add('products_widget', ProductsWidget);

});
