# -*- coding: utf-8 -*-
from openerp import models, fields, api
from datetime import timedelta,datetime,date
from odoo.exceptions import Warning, ValidationError
import re

class custom_drink(models.Model):
	_name = 'custom.drink'
	_rec_name = 'rec_field'

	product_id = fields.Many2one('product.product', string="Drink")
	qty = fields.Integer(string="Quantity")
	rec_field = fields.Char(string="Rec Field")

	@api.onchange('product_id','qty')
	def get_rec_field(self):
		self.rec_field = str(self.product_id.name)+"("+str(self.qty)+")"


class sale_order_line_extend(models.Model):
	_inherit="sale.order.line"


	kot_check = fields.Boolean(string="KOT Printed")
	void_chk = fields.Boolean(string="Void Check")
	void_reason = fields.Many2one('void.reason')
	invoice_check = fields.Boolean(string="Invoice Printed")
	kot = fields.Boolean(string="Kot")
	allow_minus = fields.Boolean(string="Allow Minus" , default=True)
	void_qty = fields.Integer()
	drink_detail = fields.Many2many('custom.drink', string="Drink Detail")
	drinks_field = fields.Char(string="Drinks")


	@api.multi
	def set_name_field(self):
		temp_string = ""
		if self.product_id.is_deal:
			for x in self.drink_detail:
				entity_recs = self.env['psc.entity'].search([])
				if len(entity_recs) > 1:
					for y in x.product_id.sync_tree:
						if y.branch.id == self.order_id.psc_entity.id:
							temp_string = temp_string + str(y.master_id) +";"+ str(x.qty)+","
				else:
					temp_string = temp_string + str(x.product_id.master_id) +";"+ str(x.qty)+","

			self.drinks_field = temp_string

	
	@api.multi
	def set_many2many_drinks(self,temp):
		# self.drink_detail = None
		if self.drinks_field:
			if self.product_id.is_deal:
				main_string = self.drinks_field.split(',')
				temp_list = []
				for x in main_string:
					if len(x) > 1:
						sub_string = x.split(';')
						rec = self.env['custom.drink'].create({
							'product_id':int(sub_string[0]),
							'qty':int(sub_string[1]),
							})

						temp_list.append(rec.id)
						rec.get_rec_field()

				self.drink_detail = [(6,0,temp_list)]
		return True

	@api.multi
	def minus_product(self):
		if self.order_id.so_state == 'draft' or self.order_id.so_state == 'kot':
			if self.kot == False and self.void_chk == False:
				if self.product_uom_qty > 0:
					self.product_uom_qty = self.product_uom_qty - 1
					self.reduce_drinks(self.product_id)
					if self.kot_check == True:
						self.void_qty = self.void_qty + 1
			if self.kot == True and self.void_chk == True and self.allow_minus == True:
				if self.product_uom_qty > 0:
					self.product_uom_qty = self.product_uom_qty - 1
					self.reduce_drinks(self.product_id)
					self.void_qty = self.void_qty + 1
					self.allow_minus = False
					# self.void_chk = False
			self.order_id.calculate_discount_amnount()


	def reduce_drinks(self,product):
		pass
		# if product.is_deal and product.deal_id:
		# 	for x in product.deal_id:
		# 		if x.product_id.drinks:
		# 			drinks = self.env['pos.drinks.tree'].search([('drinks_tree.id','=',self.order_id.id),('drinks_id.drink_size.id','=',x.product_id.drink_size.id),('product_id.id','=',product.id)])



		# 			# Jaffar Code start
		# 			for y in drinks:
		# 				if y.product_id.is_deal:
		# 					for z in y.product_id.deal_id:
		# 						if z.product_id.drinks:
		# 							if y.qty > 0:
		# 								y.qty = y.qty - 1
		# 							return
		# 			temp = len(drinks)
		# 			if drinks[temp-1].qty >0:
		# 				drinks[temp-1].qty = drinks[temp-1].qty -1
					# Jaffar Code end



					# if drinks:
					# 	drinks.qty = drinks.qty - 1


	@api.multi
	def void_product(self):
		if self.product_uom_qty > 0:
			if self.void_reason:
				check = True
			else:
				check = False
			# if self.kot_check == True and self.order_id.so_state == "kot" and not self.void_reason:
			# 	return {'name': 'Void Product',
			# 			'domain': [],
			# 			'res_model': 'pos.void.wizard',
			# 			'type': 'ir.actions.act_window',
			# 			'view_mode': 'form',
			# 			'context': {'default_pass_id' : None},
			# 			'view_type': 'form',
			# 			'target': 'new', }
			
			# void wizard open in sale order line first time 
			if self.kot == True and self.order_id.so_state == "kot" and self.void_chk == False and not self.void_reason:
				return {'name': 'Void Product',
						'domain': [],
						'res_model': 'pos.void.wizard',
						'type': 'ir.actions.act_window',
						'view_mode': 'form',
						'context': {'default_pass_id' : None},
						'view_type': 'form',
						'target': 'new', }
			
			# void wizard open in sale order line 2ndtime and so on time 
			if self.kot == True and self.order_id.so_state == "kot" and  self.void_reason and self.allow_minus == False:
				return {'name': 'Void Product',
						'domain': [],
						'res_model': 'pos.void.wizard',
						'type': 'ir.actions.act_window',
						'view_mode': 'form',
						'view_type': 'form',
						'context': {'default_resson_already_add':check , 'default_pass_id' : None},
						'target': 'new', }


	@api.multi 
	def unlink(self):
		if self.order_id.so_state not in ('done','reject'):
			if self.kot == True:
				raise ValidationError("Product Connot Be Deleted")
		new_record = super(sale_order_line_extend,self).unlink()
		return new_record



	@api.multi
	def write(self, vals):


		"""" check if write in this form if write call self function  """
		before=self.write_date
		super(sale_order_line_extend, self).write(vals)
		after = self.write_date
		if before != after:
			if self.order_id.master_id > 0:
				pass
			else:
				self.set_name_field()
			# self.set_name_field()

		return True


	@api.model 
	def create(self, vals):
		if self.product_id.is_deal and self.order_id.so_state == 'draft':
			# return False
			raise ValidationError("Can't Delete Deal. You can only minus.")
			return False
			
		new_record = super(sale_order_line_extend,self).create(vals)

		drinks_list = []
		if new_record.product_id:
			if new_record.product_id.is_deal:
				for x in new_record.product_id.deal_id:
					if x.product_id.drinks:
						custom_drink_rec = self.env['custom.drink'].create({
							'product_id':x.product_id.id,
							'qty':x.quantity,
							# 'rec_field':str(x.product_id.id)+"-"+str(1),
							})
						custom_drink_rec.get_rec_field()
						drinks_list.append(custom_drink_rec.id)
		new_record.drink_detail = [(6, 0, drinks_list)]

		if new_record.order_id.master_id > 0:
			pass
		else:
			new_record.set_name_field()
		# new_record.set_name_field()
		return new_record


class sale_order_extend(models.Model):
	_inherit = "sale.order"
	_order = 'seq_id'
	# _inherit = ['ir.needaction_mixin','sale.order']
	# _inherit = ['mail.thread', 'ir.needaction_mixin', 'ir.needaction_mixin']

	sequence_table = fields.Many2one('ecube.table', string="Sequence Table")
	effective_date = fields.Date(string="Effective Date")
	sequence = fields.Char(string="Table No.")
	new_sequence = fields.Char(string="Table No.")
	sequence_prv = fields.Char(string="Previous")
	seq_id = fields.Integer(string="Seq Order")
	discount_percent = fields.Float(string="Discount%" ,track_visibility='onchange')
	discount_amount = fields.Float(string="Discount Amount" ,track_visibility='onchange')
	deiscount_allowed = fields.Boolean(string="Discount Allowed")
	session = fields.Many2one('ecube.session', string="Session")
	cash = fields.Float(string="Cash:")
	change = fields.Float(string="Change:")

	mobile = fields.Char()
	address = fields.Many2one('ecube.address', string="Address" ,track_visibility='onchange')
	cust_name = fields.Char('Customer Name' ,track_visibility='onchange')

	# fields only available in foodpanda
	je = fields.Many2one('account.move', string="Journal Entry")
	je_delivery = fields.Many2one('account.move', string="Delivery Entry")
	je_no_cash = fields.Many2one('account.move', string="No Cash Entry")
	je_comm = fields.Many2one('account.move', string="Commission Entry")
	delievery = fields.Many2one('res.partner', string="Delievery")
	delivery_vendor = fields.Many2one('res.partner', string="Delivered By" ,track_visibility='onchange')
	delivery_instruct = fields.Char(string="Delivered Instruction")
	sale_vendor = fields.Many2one('res.partner', string="Charging Party" ,track_visibility='onchange')
	delivery_act = fields.Many2one('account.account', string="Delivery Account")
	comm_act = fields.Many2one('account.account', string="Commission Account")
	dis_act = fields.Many2one('account.account', string="Discount Account")
	sale_act = fields.Many2one('account.account', string="Sale Account")
	delivery = fields.Boolean()
	no_cash = fields.Boolean()
	delivery_charges = fields.Float(string="Delivery Charges")
	cash_back = fields.Float(string="Cash Back")
	credit_acc = fields.Many2one('account.account', string="Crdt Account")
	je_name = fields.Char(string="JE Name")
	kot_time = fields.Datetime(track_visibility='onchange')
	inv_time = fields.Datetime(track_visibility='onchange')
	done_time = fields.Datetime()
	order_time = fields.Datetime()
	rec_time = fields.Datetime()
	reject_time = fields.Datetime()
	color = fields.Char('Color Index', compute="change_colore_on_kanban")
	waiter = fields.Many2one('hr.employee', string="Waiter" ,track_visibility='onchange')
	rider_id = fields.Many2one('hr.employee', string="Rider" ,track_visibility='onchange')
	rider = fields.Many2one('res.partner', string="Rider" ,track_visibility='onchange')
	kot_counter = fields.Integer()
	sale_counter = fields.Integer()
	allowed = fields.Boolean(string="Allowed Edit")
	kot_button = fields.Boolean(string="Kot hide" )
	inv_button = fields.Boolean(string="Inv hide")
	rider_chk = fields.Boolean(string="Rider hide")
	charged_to = fields.Boolean(string="Charged To Foodpanda")
	no_cash = fields.Boolean(string="No Cash")

	rej_type = fields.Many2one('rej.type', string="Rejection Type")
	charge_type = fields.Many2one('charging.type', string="Charging Type")
	rej_reason = fields.Text('Reason')
	rej_approval = fields.Char('Approved')
	voucher = fields.Char('Voucher')
	voucher_amount = fields.Float(string="Voucher Amount")
	drinks_id = fields.One2many('pos.drinks.tree','drinks_tree')
	block_cust = fields.Boolean(string="Block Customer")
	block_reason = fields.Char(string="Block Reason")
	preview = fields.Html('Report Preview')
	salepreview = fields.Html('Report Preview')

	no_cash_desc = fields.Text(string="Description")



	master_id = fields.Integer(string="Master ID")
	last_update = fields.Datetime(string="Last Update")
	online_order = fields.Boolean(string="Online Order")
	order_address = fields.Char(string="Order Address")
	clone_sync_bool = fields.Boolean(string="Update Parent SO")
	parent_db_created = fields.Boolean(string="Parent DB Check")
	rej_submit = fields.Boolean(string="Rejection Submited")

	master_id_same = fields.Char(string="master id same")
	call_center_user = fields.Char(string="Call Center User")
	branch_user = fields.Char(string="Branch User")

	hide_rider_check = fields.Boolean(string="Hide Rider Check")
	rejected_table = fields.Boolean(string="Rejected Table")
	pre_reject_stage = fields.Char(string="Pre Rejected")

	token = fields.Char(string="Token")
	code = fields.Char(string="Code")
	foodpanda_restaurant_id = fields.Many2one('foodpanda.restaurant', string="Foodpanda Restaurant ID")
	foodpanda_restaurant_id_char = fields.Char(string="Foodpanda Restaurant ID")
	login_token = fields.Char(string="Login Token")

	status = fields.Selection([
		('sync', 'Synched'),
		('no_sync', 'Not Synched'),
		], string="Status", default="no_sync")

	so_state = fields.Selection([
		('draft', 'Draft'),
		('accepted', 'Order'),
		('received', 'Received'),
		('kot', 'KOT'),
		('invoiced', 'Invoiced'),
		('done', 'Settle'),
		('cancel', 'Cancel'),
		('reject', 'Reject'),
		], default="draft", string="SO State" ,track_visibility='onchange')

	so_type = fields.Selection([
		('so', 'Sale Order'),
		('foodpanda', 'Foodpanda'),
		('cc', 'Call Center'),
		('take_away', 'Take Away'),
		], string="SO Type")

	payment_type = fields.Selection([
		('cod', 'COD'),
		('online_pay', 'Online Payment'),
		], string="Payment Type",default='cod')

	journal_id = fields.Many2one('account.journal')
	receipe_id = fields.Many2one('stock.picking',copy=False,readonly=True)
	order_date = fields.Date(string="Date Ordered")


	purchase_type = fields.Selection([
		('growers', 'Growers'),
		('center', 'Center'),
		],string = "Type")

	psc_entity = fields.Many2one('psc.entity' , string="Entity",required=True)


	state = fields.Selection([
		('draft', 'Draft'),
		# ('sent', 'Quotation Sent'),
		('sale', 'Sales Order'),
		('done', 'Locked'),
		('cancel', 'Cancelled'),
		], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')


	amount_total = fields.Float(string='Total', store=True, readonly=True, compute='_amount_all', track_visibility='always' , digits=(19,0))
	payment_check = fields.Boolean(string="Payment delivery check")

	delivery_time = fields.Char(string="Delivery (Minutes)")
	address_line1 = fields.Char(string="Address 1")
	customer_from_address = fields.Char(string="Customer Address")
	address_building = fields.Char(string="Building")
	fp_delivery_time = fields.Datetime(string="FP Delivery Time")

	@api.onchange('payment_type')
	def update_tax(self):
		if self.order_line:
			if self.payment_type == 'online_pay':
				if self.so_type in ['so','cc','take_away']:
					for x in self.order_line:
						if x.product_id.other_tax:
							print x.product_id.other_tax
							tax_list = []
							x.tax_id = False
							for tax in x.product_id.other_tax:
								tax_list.append(tax.id)
							x.tax_id = [(6, 0, tax_list)]
			else:
				if self.so_type in ['so','cc','take_away']:
					for x in self.order_line:
						if x.product_id.taxes_id:
							print x.product_id.taxes_id
							tax_list = []
							x.tax_id = False
							for tax in x.product_id.taxes_id:
								tax_list.append(tax.id)
							x.tax_id = [(6, 0, tax_list)]
				print "else print start"

	def readonly_payment_type(self):
		pass
		# if self.so_type == "cc":
		# 	if self.charge_type.name == "Self Delivery" or self.charge_type.name == "Eat Mubarik":
		# 		self.payment_type = "cod"
		# 		self.payment_check = True
		# 	else:
		# 		self.payment_check = False

	@api.onchange('payment_type')
	def onchange_of_payment_type(self):
		pass
		# if self.so_type == "cc":
		# 	if self.payment_type == "online_pay":
		# 		if self.charge_type.name == "Self Delivery" or self.charge_type.name == "Eat Mubarik":
		# 			self.payment_type = "cod"
		# 			return {'value':{},'warning':{'title':
		# 			'warning','message':"Please Set The Delivery Type First"}}


	@api.multi
	def rejection_submit(self):
		self.rej_submit = True

	@api.onchange('no_cash')
	def change_sale_vendor(self):
		if self.no_cash == False:
			self.sale_vendor = False
			self.rej_approval = False


	
	@api.onchange('charge_type')
	def show_hide_rider(self):
		if self.charge_type.name == "Eat Mubarik" or self.charge_type.name == "Food Panda":
			self.hide_rider_check = True
		else:
			self.hide_rider_check = False
		self.readonly_payment_type()

		

	# to be comented once run
	@api.multi
	def adjust_seq_id(self):
		# so_recs = self.env['sale.order'].search([('so_state','!=','done')])
		# for x in so_recs:
		# 	x.new_sequence = str(x.sequence)



		pass
		# so_recs = self.env['sale.order'].search([])
		# for x in so_recs:

		# 	temp = str(x.sequence)

		# 	if len(temp) == 4:
		# 		print temp[-2:]
		# 		x.seq_id = abs(int(temp[-2:]))

		# 	elif len(temp) == 3:
		# 		print temp[-1]
		# 		x.seq_id = abs(int(temp[-1]))


	@api.multi
	def adjust_deal_drinks(self):

		order_unique_deals = {}
		qty_list = []
		for x in self.order_line:
			if x.product_id.id not in order_unique_deals:
				order_unique_deals[x.product_id.id] = x.product_uom_qty
			else:
				order_unique_deals[x.product_id.id] = order_unique_deals[x.product_id.id] + x.product_uom_qty


		drink_unique_deals = {}
		qty_list = []
		for x in self.drinks_id:
			if x.product_id.id not in drink_unique_deals:
				drink_unique_deals[x.product_id.id] = x.qty
			else:
				drink_unique_deals[x.product_id.id] = drink_unique_deals[x.product_id.id] + x.qty


		for x in self.order_line:
			if x.product_id.id in order_unique_deals and x.product_id.id in drink_unique_deals:
				if order_unique_deals[x.product_id.id] > drink_unique_deals[x.product_id.id]:

					temp = order_unique_deals[x.product_id.id] - drink_unique_deals[x.product_id.id]
					deal_drink_id = None
					if x.product_id.is_deal:
						for z in x.product_id.deal_id:
							if z.product_id.drinks:
								deal_drink_id = z.product_id.id
						# for y in range(int(temp)):
						self.drinks_id.create({
							'product_id':x.product_id.id,
							'drinks_id':deal_drink_id,
							'qty':temp,
							'drinks_tree':self.id,
							})
						drink_unique_deals[x.product_id.id] = order_unique_deals[x.product_id.id]

			if (len(order_unique_deals) > 0 and len(drink_unique_deals) == 0):
				deal_drink_id = None
				if x.product_id.is_deal:
					for z in x.product_id.deal_id:
						if z.product_id.drinks:
							deal_drink_id = z.product_id.id
					self.drinks_id.create({
						'product_id':x.product_id.id,
						'drinks_id':deal_drink_id,
						'qty':x.product_uom_qty,
						'drinks_tree':self.id,
						})
					drink_unique_deals[x.product_id.id] = order_unique_deals[x.product_id.id]

				# order_unique_deals.append(x.product_id.id)
				# qty_list.append(0)
		# temp_dict = {i:0 for i in order_unique_deals}






	# @api.multi
	# def do_toggle_done_button(self):
	# 	""" NoT Use  """
	# 	url='http://yasir:8069/web?debug#view_type=kanban&model=sale.order&action=501'
	# 	return {
	# 		'name'     : 'Go to website',
	# 		'res_model': 'ir.actions.act_url',
	# 		'type'     : 'ir.actions.act_url',
	# 		'target'   : 'current',	
	# 		'url'		: url,
	# 	}
	


	# @api.multi
	# def select_rider(self):
	# 	if not self.so_state == "done":
	# 		return {'name': 'Select Rider',
	# 				'domain': [],
	# 				'res_model': 'pos.rider.wizard',
	# 				'type': 'ir.actions.act_window',
	# 				'view_mode': 'form',
	# 				'view_type': 'form',
	# 				'target': 'new', }

	# @api.multi
	# def select_waiter(self):
	# 	if not self.so_state == "done":
	# 		return {'name': 'Select Waiter',
	# 				'domain': [],
	# 				'res_model': 'pos.waiter.wizard',
	# 				'type': 'ir.actions.act_window',
	# 				'view_mode': 'form',
	# 				'view_type': 'form',
	# 				'target': 'new', }

	# @api.multi
	# def select_kot(self):
	# 	html = self.env['report']
	# 	print html
	# 	print "ssssssssssssss"
	# 	print "ssssssssssssss"

		# self.write({'preview': html})

		# return True

	@api.multi
	def insert_cust(self):
		if not self.so_state == "done":
			return {'name': 'Add Customer Info',
					'domain': [],
					'res_model': 'pos.custinfo.wizard',
					'type': 'ir.actions.act_window',
					'view_mode': 'form',
					'view_type': 'form',
					'context': {
					'default_mobile':self.mobile,
					'default_cust_name':self.cust_name,
					'default_address':self.address.id,
					'default_charge_type':self.charge_type.id,
					'default_payment_type':self.payment_type,
					'default_delivery_instruct':self.delivery_instruct,
					'default_voucher':self.voucher,
					'default_voucher_amount':self.voucher_amount,
					'default_block_cust':self.block_cust,
					'default_so_type':self.so_type},
					'target': 'new', }

	@api.multi
	def cancel_order(self):
		if self.rejected_table == True:
			user_id = self.env['res.users'].search([('id','=',self._uid)])
			self.rej_approval = user_id.name
		self.so_state = 'cancel'
		
		self.child_db_order_create()


	@api.multi
	def write(self, vals):


		"""" check if write in this form if write call self function  """
		before=self.write_date
		super(sale_order_extend, self).write(vals)
		after = self.write_date
		if before != after:
			if not self.so_state == 'done':
				self.get_mobile()
				self.show_hide_rider()

		return True


	@api.multi
	def create(self, vals):


		rec = super(sale_order_extend, self).create(vals)
		rec.show_hide_rider()

		return rec

	@api.one
	def change_colore_on_kanban(self):
		for record in self:
			color = ""
			if record.so_state == 'kot':
				color = 'orange'
			if record.so_state == 'received':
				color = 'green'
			record.color = color
			if record.so_state == 'cancel':
				color = 'darkred'
			record.color = color
			if record.so_state == 'accepted':
				color = 'pink'
			record.color = color
			if record.so_state == 'invoiced':
				color = 'gray'
			record.color = color
			if record.so_state == 'reject':
				color = 'yellow'
			record.color = color

	@api.multi
	def reject(self):
		self.reject_time = datetime.now()
		self.rejected_table = True
		self.pre_reject_stage = self.so_state
		self.so_state = 'reject'


	def create_entry_lines(self,account,debit,credit,entry_id,label,partner):
		self.env['account.move.line'].create({
		'account_id':account,
		'partner_id':partner,
		'name':label,
		'debit':debit,
		'credit':credit,
		'move_id':entry_id,
		})

	def get_mobile(self):
		if self.address:
			self.address.mobile = self.mobile


	@api.multi
	def online_payment(self):
		if not self.je:
			move_id = self.env['account.move'].create({
				'journal_id':self.journal_id.id,
				'psc_entity':self.psc_entity.id,
				})

			self.je = move_id.id

			payment_debit = self.create_entry_lines(self.delievery.property_account_payable_id.id, self.amount_total, 0, move_id.id, "Online Payment Debit",None)
			payment_credit = self.create_entry_lines(self.credit_acc.id, 0, self.amount_total, move_id.id, "Online Payment Credit",None)
			self.je_name = self.je.name
		else:
			self.je.button_cancel()
			self.je_name = self.je.name
			self.je.line_ids.unlink()

			payment_debit = self.create_entry_lines(self.delievery.property_account_payable_id.id, self.amount_total, 0, self.je.id,"Online Payment Debit",None)
			payment_credit = self.create_entry_lines(self.credit_acc.id, 0, self.amount_total, self.je.id, "Online Payment Credit",None)

		self.je.post()


	@api.multi
	def online_delivery(self):

		if not self.je_delivery:
			move_id = self.env['account.move'].create({
				'journal_id':self.journal_id.id,
				'date':self.date_order,
				})

			self.je_delivery = move_id.id

			payment_debit = self.create_entry_lines(self.delivery_act.id,self.delivery_charges, 0, move_id.id, "Delivery Payment",self.delivery_vendor.id)
			payment_credit = self.create_entry_lines(self.delivery_vendor.property_account_payable_id.id, 0, self.delivery_charges, move_id.id,"Delivery Payment",self.delivery_vendor.id)

		if not self.je_comm:
			if self.delivery_vendor.commission and self.delivery_vendor.commission_amt:
				move_id_comm = self.env['account.move'].create({
					'journal_id':self.journal_id.id,
					'date':self.date_order,
					})

				self.je_comm = move_id_comm.id

				amount = (self.delivery_vendor.commission_amt/100 ) * self.amount_total


				comm_debit = self.create_entry_lines(self.comm_act.id,amount, 0, move_id_comm.id, "Commission Payment",self.delivery_vendor.id)
				comm_credit = self.create_entry_lines(self.delivery_vendor.property_account_payable_id.id, 0, amount, move_id_comm.id,"Commission Payment",self.delivery_vendor.id)
	
	

	@api.multi
	def online_nocash(self):

		if not self.je_no_cash:
			move_id = self.env['account.move'].create({
				'journal_id':self.journal_id.id,
				'date':self.date_order,
				})

			self.je_no_cash = move_id.id
			
			payment_credit = self.create_entry_lines(self.sale_act.id, 0, self.amount_total, move_id.id,"No Cash Payment",self.sale_vendor.id)
			if self.discount_amount:
				payment_debit = self.create_entry_lines(self.sale_vendor.property_account_payable_id.id,self.amount_total-self.discount_amount, 0, move_id.id, "No Cash Payment",self.sale_vendor.id)
				payment_dis = self.create_entry_lines(self.dis_act.id,self.discount_amount, 0, move_id.id, "No Cash Discount",self.sale_vendor.id)
			else:
				payment_debit = self.create_entry_lines(self.sale_vendor.property_account_payable_id.id,self.amount_total, 0, move_id.id, "No Cash Payment",self.sale_vendor.id)


	# @api.onchange('mobile')
	# def get_last_cust(self):
	# 	if self.mobile:
	# 		so = self.env['sale.order'].search([('mobile','=',self.mobile)])
	# 		if so:
	# 			last_so = so[0]
	# 			self.cust_name = last_so.cust_name
	# 	else:
	# 		self.cust_name = False


	@api.onchange('charge_type')
	def get_rider_chk(self):
		if self.charge_type:
			if self.charge_type.name == 'Self Delivery':
				self.rider_chk = True
			else:
				self.rider_chk = False
				self.rider_id = False
		else:
			self.rider_chk = False
			self.rider_id = False


	@api.onchange('charged_to')
	def get_charged_to(self):
		if self.charged_to:
			self.sale_vendor = None



	# # Calculating change
	# @api.onchange('cash', 'amount_total')
	# def calculate_change(self):
	# 	# self.calculate_discount_percent()
	# 	if self.cash:
	# 		self.change = self.cash - self.amount_total
	# 	else:
	# 		self.change = 0

	# @api.onchange('partner_id')
	# def set_dafault_customer(self):
	# 	walkin = self.env['res.partner'].search([('name','=','Walkin')],limit=1)
	# 	if walkin:
	# 		self.partner_id = walkin.id
	# 	self.onchange_partner_id()

	@api.depends('order_line.price_total')
	def _amount_all(self):
		"""
		Compute the total amounts of the SO.
		"""
		for order in self:
			amount_untaxed = amount_tax = 0.0
			for line in order.order_line:
				amount_untaxed += line.price_subtotal
				# FORWARDPORT UP TO 10.0
				if order.company_id.tax_calculation_rounding_method == 'round_globally':
					price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
					taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=order.partner_shipping_id)
					amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
				else:
					amount_tax += line.price_tax
			order.update({
				'amount_untaxed': order.pricelist_id.currency_id.round(amount_untaxed),
				'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
				'amount_total': amount_untaxed + amount_tax + self.delivery_charges - (self.discount_amount + self.cash_back + self.voucher_amount),
			})


	@api.onchange('discount_percent', 'amount_untaxed','delivery_charges','cash_back','voucher_amount','order_line')
	def calculate_discount_amnount(self):
		self.discount_amount = (self.discount_percent * self.amount_untaxed)/100

		self.amount_total = (self.amount_untaxed + self.amount_tax + self.delivery_charges) - (self.discount_amount + self.cash_back + self.voucher_amount)

	
	@api.onchange('discount_amount', 'amount_untaxed','delivery_charges','cash_back','voucher_amount','order_line')
	def calculate_discount_percent(self):
		if self.amount_untaxed:
			self.discount_percent = (self.discount_amount * 100)/self.amount_untaxed
		self.amount_total = (self.amount_untaxed + self.amount_tax + self.delivery_charges) - (self.discount_amount + self.cash_back + self.voucher_amount) 

	@api.multi
	def check_discount_allowed(self):
		self.deiscount_allowed = True

	@api.multi
	def accepted(self):
		if self.order_line:
			if self.so_type == 'cc' or self.so_type == 'foodpanda' or  self.so_type == 'take_away':
				self.order_time = datetime.now()
				self.allowed = True
				self.so_state = 'accepted'
		else:
			raise ValidationError("Nothing in order line.")



	@api.multi
	def received(self):
		self.adjust_deal_drinks()
		if self.order_line:
			if self.so_type == 'cc' or self.so_type == 'foodpanda' or self.so_type == 'take_away':
				self.allowed = True
				self.rec_time = datetime.now()
				rec = self.env['ecube.session'].search([('user','=',self._uid),('state','=','open'),('psc_entity.id','=',self.psc_entity.id)])
				# if self.so_type == 'take_away':
				# 	if not self.waiter:
				# 		raise ValidationError("Please Select Waiter.")
				if rec:
					self.session = rec.id
					self.effective_date = self.session.session_date
					self.so_state = 'received'
					self.stop_sound()
				else:
					raise ValidationError("No open session for this user.")

		else:
			raise ValidationError("Nothing in order line.")


		self.clone_sync_bool = False
		branch_user = self.env['res.users'].search([('id','=',self._uid)])
		self.branch_user = branch_user.name


	@api.multi
	def accept_rec(self):
		self.adjust_deal_drinks()
		if self.order_line:
			if self.so_type == 'so' or self.so_type == 'take_away':
				self.allowed = True
				self.rec_time = datetime.now()
				self.order_time = datetime.now()
				rec = self.env['ecube.session'].search([('user','=',self._uid),('state','=','open'),('psc_entity.id','=',self.psc_entity.id)])
				if not self.waiter:
					raise ValidationError("Please Select Waiter.")
				if rec:
					self.session = rec.id
					self.effective_date = self.session.session_date
					self.so_state = 'received'
				else:
					raise ValidationError("No open session for this user.")
		else:
			raise ValidationError("Nothing in order line.")

		self.clone_sync_bool = False
		branch_user = self.env['res.users'].search([('id','=',self._uid)])
		self.branch_user = branch_user.name


	@api.multi
	def done(self):

		if self.charge_type.name == "Self Delivery":
			if not self.rider_id:
				raise ValidationError("Rider is not selected.") 

		if self.cash < self.amount_total:
			if self.payment_type == 'online_pay' or self.no_cash == True:
				pass
			else:
				raise ValidationError("Cash is less than actual amount.")
		
		if self.rejected_table == True:
			user_id = self.env['res.users'].search([('id','=',self._uid)])
			self.rej_approval = user_id.name
		self.so_state = 'done'
		self.allowed = True
		self.update_session()
		self.done_time = datetime.now()
		self.child_db_order_create()
			

		if self.so_type == 'so':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Dine In',
				'res_model': 'sale.order',
				'view_type': 'form',
				'domain': [('so_state', 'not in',('done','cancel')),('so_type','=','so')],
				'view_mode': 'kanban,form',
				'target': 'main',
				
			}

		if self.so_type == 'foodpanda':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Food Panda',
				'res_model': 'sale.order',
				'view_type': 'form',
				'domain': [('so_state', 'not in',('done','cancel')),('so_type','=','foodpanda')],
				'view_mode': 'kanban,form',
				'target': 'main',
				
			}

		if self.so_type == 'cc':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Call Center',
				'res_model': 'sale.order',
				'view_type': 'form',
				'domain': [('so_state', 'not in',('done','cancel')),('so_type','=','cc')],
				'view_mode': 'kanban,form',
				'target': 'main',
				
			}

		if self.so_type == 'take_away':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Take Away',
				'res_model': 'sale.order',
				'view_type': 'form',
				'domain': [('so_state', 'not in',('done','cancel')),('so_type','=','take_away')],
				'view_mode': 'kanban,form',
				'target': 'main',
				
			}

		# if not self.cash:

		# 	if self.sale_vendor:

		# 		if self.sale_vendor:
		# 			move_id_cash = self.env['account.move'].create({
		# 				'journal_id':self.journal_id.id,
		# 				'date':self.date_order,
		# 				})

		# 			self.je_no_cash = move_id_cash.id


		# 			cash_debit = self.create_entry_lines(self.sale_vendor.property_account_payable_id.id,self.amount_total, 0, move_id_cash.id,self.sequence,self.delivery_vendor.id)
		# 			cash_credit = self.create_entry_lines(self.credit_acc.id,self.amount_total, amount, move_id_cash.id,self.sequence,self.delivery_vendor.id)

		# 		else:

		# 			raise ValidationError("Select Charging Party.")

		# if not self.je_delivery:
		# 	move_id = self.env['account.move'].create({
		# 		'journal_id':self.journal_id.id,
		# 		'date':self.date_order,
		# 		})

		# 	self.je_delivery = move_id.id

		# 	payment_debit = self.create_entry_lines(self.delivery_act.id,self.delivery_charges, 0, move_id.id, "Delivery Payment",self.delivery_vendor.id)
		# 	payment_credit = self.create_entry_lines(self.delivery_vendor.property_account_payable_id.id, 0, self.delivery_charges, move_id.id,"Delivery Payment",self.delivery_vendor.id)

		# if not self.je_comm and self.so_type == 'foodpanda':
		# 	foodpanda = self.env['res.partner'].search([('foodpanda','=',True)],limit=1)
		# 	if foodpanda:
		# 		if self.delivery_vendor.commission and self.delivery_vendor.commission_amt:
		# 			move_id_comm = self.env['account.move'].create({
		# 				'journal_id':self.journal_id.id,
		# 				'date':self.date_order,
		# 				})

		# 			self.je_comm = move_id_comm.id

		# 			comm_debit = self.create_entry_lines(self.comm_act.id,self.amount_untaxed, 0, move_id_comm.id, "Commission Payment",self.delivery_vendor.id)
		# 			comm_credit = self.create_entry_lines(foodpanda.property_account_payable_id.id, 0, self.amount_untaxed, move_id_comm.id,"Commission Payment",self.delivery_vendor.id)

		# 	else:

		# 		raise ValidationError("Create vendor of foodpanda.")







	# @api.multi
	# def done_order(self):

	# 	self.session.t_sales = self.session.t_sales + self.amount_total

	@api.onchange('date_order')
	def get_order_date(self):
		if self.date_order:
			self.order_date = self.date_order


	@api.one
	@api.depends('order_line.invoice_lines.invoice_id.state')
	def _compute_invoice(self):
		new_record = super(sale_extend, self)._compute_invoice()
		self.invoice_count = self.env['account.invoice'].search_count([('type','=','out_invoice'),('origin','=',self.name)])

		return new_record

	@api.multi
	def action_view_invoice(self):
		super(sale_extend, self).action_view_invoice()
		

		action = self.env.ref('account.action_invoice_tree1').read()[0]
		action ['domain'] = [('type','=','out_invoice'),('origin','=',self.name)]
		# action['context'] = {'type': 'in_invoice', 'default_source_document': self.name}

		return action


	@api.multi
	def action_confirm(self):
		super(sale_extend, self).action_confirm()

		stock_pick = self.env['stock.picking'].search([('origin','=',self.name),('type','=','delivery'),('state','!=','cancel')],limit=1)
		stock_pick.receiving_date = self.date_order
		stock_pick.desc = str('Branch Sale Finshed Good')

		for x in self.order_line:
			if x.product_id.deal_id:
				stock_move = self.env['stock.move'].search([('product_id.id','=',x.product_id.id),('picking_id.origin','=',self.name)])
				stock_move.state = 'draft'
				stock_move.unlink()
				for y in x.product_id.deal_id:
					stock_pro = self.env['stock.move'].search([('product_id.id','=',y.product_id.id),('picking_id.origin','=',self.name)])
					if stock_pro:
						stock_pro.product_uom_qty = stock_pro.product_uom_qty + (x.product_uom_qty * y.quantity)
					else:
						create_tree = self.env['stock.move'].create({
								'product_id': y.product_id.id,
								'product_uom_qty': x.product_uom_qty*y.quantity,
								'product_uom': y.product_id.uom_id.id,
								'location_id': stock_pick.location_id.id,
								'name': y.product_id.name,
								'location_dest_id': stock_pick.location_dest_id.id,
								'picking_id': stock_pick.id
								})
		stock_pick.action_assign()
		stock_pick.action_confirm()
		stock_pick.force_assign()
		for lines in stock_pick.pack_operation_product_ids:
			lines.qty_done = lines.product_qty

		stock_pick.do_new_transfer()

		return True


	@api.multi
	def c_picking(self):
		# sale_count = self.env['sale.order'].search([('state','not in',('draft','cancel')),('receipe_id','=',False)])
		# print sale_count
		# print "444444444444444444444"
		# unique_entity = []
		# for x in sale_count:
		#     if x.psc_entity:
		#         if x.psc_entity not in unique_entity:
		#             unique_entity.append(x.psc_entity)
		cust_loc = self.env['stock.location'].search([('usage','=','customer')],limit=1)
		current_date = fields.date.today()

		# print unique_entity
		# print "777777777777777777777777"
		# for ent in unique_entity:
		#     print ent.stock_location.id
		#     print ent.name
		#     print "333333333333333333333333"

		create_recorder = self.env['stock.picking'].create({
			'min_date':self.date_order,
			'location_dest_id': cust_loc.id,
			'location_id': self.psc_entity.stock_location.id,
			'picking_type_id':self.psc_entity.picking_type.id,
			'type':'branch_consump',
			'psc_entity':self.psc_entity.id,
			'origin':self.name,
			'desc':"Branch Sale Receipy Consumption",
			'receiving_date':self.date_order,
		})
		create_recorder.type = 'branch_consump'

		# sale_count = self.env['sale.order.line'].search([('order_id.state','not in',('draft','cancel')),('order_id.receipe_id','=',False),('order_id.psc_entity.id','=',ent.id)])

		self.receipe_id = create_recorder.id

		for z in self.order_line:
			if z.product_id.recipe_id:
				for y in z.product_id.recipe_id:
					if y.product_id:
						issuance_now = self.env['stock.move'].search([('picking_id.id','=',create_recorder.id),('product_id.id','=',y.product_id.id)])
						if issuance_now:
							issuance_now.product_uom_qty = issuance_now.product_uom_qty + (z.product_uom_qty*y.quantity)
						else:
							if y.product_id:
								create_tree = self.env['stock.move'].create({
									'product_id': y.product_id.id,
									'product_uom_qty': z.product_uom_qty*y.quantity,
									'product_uom': y.product_id.uom_id.id,
									'location_id': self.psc_entity.stock_location.id,
									'name': y.product_id.name,
									'location_dest_id': cust_loc.id,
									'picking_id': create_recorder.id
									})

			elif z.product_id.deal_id:
				for j in z.product_id.deal_id:
					if j.product_id.recipe_id:
						for y in j.product_id.recipe_id:
							if y.product_id:
								issuance_now = self.env['stock.move'].search([('picking_id.id','=',create_recorder.id),('product_id.id','=',y.product_id.id)])
								if issuance_now:
									issuance_now.product_uom_qty = issuance_now.product_uom_qty + ((z.product_uom_qty*j.quantity)*y.quantity)
								else:
									if y.product_id:
										create_tree = self.env['stock.move'].create({
											'product_id': y.product_id.id,
											'product_uom_qty': ((z.product_uom_qty*j.quantity)*y.quantity),
											'product_uom': y.product_id.uom_id.id,
											'location_id': self.psc_entity.stock_location.id,
											'name': y.product_id.name,
											'location_dest_id': cust_loc.id,
											'picking_id': create_recorder.id
											})

					else:
						raise ValidationError('Product ' + j.product_id.name + " receipe is not set.")

			else:
				raise ValidationError('Product ' + z.product_id.name + " receipe or " + z.product_id.name +" deal is not set.")
				


		create_recorder.action_assign()
		create_recorder.action_confirm()
		create_recorder.force_assign()
	   
		for lines in create_recorder.pack_operation_product_ids:
			lines.qty_done = lines.product_qty

		create_recorder.do_new_transfer()
		create_recorder.min_date = current_date


	@api.model
	def _needaction_count(self, dom):
		context = {}
		if (dom):
			dom.append(('so_state','=','accepted'))
			rest = self.search(dom)
			res = len(self.search(dom))

		else:
			res = super(sale_order_extend, self)._needaction_count(dom)
		return res

	def update_session(self):
		if self.session:
			if not self.session.state == 'open':
				raise ValidationError('Session is not open state please contact your administrator')
			self.effective_date = self.session.session_date
			if self.no_cash == False:
				if not self.payment_type == 'online_pay':
					""" This section cash on delivery """
					self.session.cash_collected = self.session.cash_collected + self.amount_total
					self.session.cash_sales = self.session.cash_sales + self.amount_untaxed
					self.session.tax_amount = self.session.tax_amount + self.amount_tax
					self.session.cash_dis = self.session.cash_dis + self.discount_amount
					self.session.voucher_amount_cod = self.session.voucher_amount_cod + self.voucher_amount
					self.session.cash_delivery_charges = self.session.cash_delivery_charges + self.delivery_charges
					self.session.cash_net = self.session.cash_net + self.amount_untaxed + self.amount_tax + self.delivery_charges - self.voucher_amount
				else:
					""" This section online payment """
					self.session.cr_sales = self.session.cr_sales + self.amount_untaxed
					self.session.cr_tax_amount = self.session.cr_tax_amount + self.amount_tax
					self.session.cr_dis = self.session.cr_dis + self.discount_amount
					self.session.voucher_amount = self.session.voucher_amount + self.voucher_amount
					self.session.credit_deliver_charges = self.session.credit_deliver_charges + self.delivery_charges
					self.session.cr_net = self.session.cr_net + self.amount_untaxed + self.delivery_charges
			else:
				if not self.payment_type == 'online_pay':
					self.session.amount_charged_cash = self.session.amount_charged_cash + self.amount_total
				else:
					self.session.amount_charged_online = self.session.amount_charged_online + self.amount_total
			self.session.get_expenses()
			self.session.get_close()
			self.session.get_diff()
			self.session.get_total_fp_voucher()
			self.session.get_total_untaxed_amount()
			self.session.get_total_sale_tax()
			self.session.get_total_delivery_charges()
			self.session.get_tot_net()



		else:
			raise ValidationError('Session is not open state please contact your administrator')

		

class ecube_table(models.Model):
	_name = "ecube.table"
	_rec_name = 'sequence'

	sequence = fields.Char(string="Sequence")
	order_from = fields.Integer(string="Order From")
	order_to = fields.Integer(string="Order To")
	psc_entity = fields.Many2one('psc.entity', string="Entity")
	so_type = fields.Selection([
		('so', 'Sale Order'),
		('foodpanda', 'Foodpanda'),
		('cc', 'Call Center'),
		('take_away', 'Take Away'),
		])

	@api.multi
	def create_sos(self):
		charging_id = 0
		if self.so_type == 'cc':
			charging_rec = self.env['charging.type'].search([('name','=','Self Delivery'),('so_type','=','cc')],limit=1)
			charging_id = charging_rec.id
		if self.so_type == 'foodpanda':
			charging_rec = self.env['charging.type'].search([('name','=','Food Panda'),('so_type','=','foodpanda')],limit=1)
			charging_id = charging_rec.id
		temp = self.order_to - self.order_from
		
		cust_rec = self.env['res.partner'].search([('ref','=','Walkin')],limit=1)
		pricelist_rec = self.env['product.pricelist'].search([('id','=',1)])
		
		delivery_charges_amount = 0
		if self.so_type == 'cc' or self.so_type == 'foodpanda':
			delivery_charges_amount = self.psc_entity.delivery_charges
		for x in range(temp+1):
			temp2 =  str(x + self.order_from)
			self.env['sale.order'].create({
				'partner_id':cust_rec.id,
				'pricelist_id':pricelist_rec.id,
				'so_type':self.so_type,
				# 'sequence_table':self.id,
				'seq_id':int(temp2),
				'psc_entity':self.psc_entity.id,
				'charge_type':charging_id,
				'rider_chk':True,
				# 'seq_id':int(x)+1,
				'delivery_charges':delivery_charges_amount,
				'sequence':self.sequence+"-"+temp2,
				})


class respartner_extend (models.Model):
	_inherit = "res.partner"
	
	emp_id = fields.Char(readonly=True)
	delivery = fields.Boolean()
	foodpanda = fields.Boolean()
	show_pos = fields.Boolean()
	eat_mubarak = fields.Boolean()
	eat_mubarak_delivery_charges = fields.Float('per delivery charges')
	foodpanda_commision = fields.Float(string="Foodpanda Commission")
	foodpanda_delivery_expense = fields.Float(string="Foodpanda Delivery Expense")
	commission = fields.Boolean()
	rider = fields.Boolean()
	commission_amt = fields.Float(string="Commission Value In Per")
	psc_entity = fields.Many2one('psc.entity', string="Entity")
	


class rider_payment(models.Model):
	_name = "rider.payment"
	_rec_name = 'sequence'

	sequence = fields.Char(string="Sequence")
	date = fields.Date(required=False, default=datetime.today())
	amount = fields.Float(string="Amount Paid")
	balance = fields.Float(string="Balance")
	vendor = fields.Many2one('hr.employee', string="Rider",required=True)
	pay_act = fields.Many2one('account.account', string="Account")
	entry_id = fields.Many2one('account.move', string="Entry")
	state = fields.Selection([
		('draft', 'Draft'),
		('validate', 'Validate'),
		],default="draft")

	@api.model
	def create(self, vals):
		vals['sequence'] = self.env['ir.sequence'].next_by_code('rider.payment.seq')
		new_record = super(rider_payment, self).create(vals)
		new_record.get_balance()

		return new_record


	@api.multi
	def write(self, vals):


		"""" check if write in this form if write call self function  """
		before=self.write_date
		super(rider_payment, self).write(vals)
		after = self.write_date
		if before != after:
			self.get_balance()
		return True

	@api.onchange('vendor','date')
	def get_balance(self):
		if self.vendor:
			rider_amount = 0
			already_given_amount = 0
			so_order = self.env['sale.order'].search([('rider_id','=',self.vendor.id),('effective_date','=',self.date),('so_state','=','done')])
			rider_payment_rec = self.env['rider.payment'].search([('vendor.id','=',self.vendor.id),('date','=',self.date),('state','=','validate')])
			entity = self.env['psc.entity'].search([])
			for rec in rider_payment_rec:
				already_given_amount = already_given_amount + rec.amount
			rider_amount = len(so_order) * int(entity.mobile)
			self.balance = rider_amount - already_given_amount

	@api.multi
	def goto_rider_order(self):
		rec = self.env['sale.order'].search([('rider_id','=',self.vendor.id),('effective_date','=',self.date),('so_state','=','done')]).ids
		if rec:
			domain = [('id','in',rec)]


			return {
			'type': 'ir.actions.act_window',
			'name': ('Quotations'),
			'res_model': 'sale.order',
			'view_type': 'form',
			'view_mode': 'pivot',
			'view_id ref="sale.view_sale_order_pivot"': '',
			'target': 'current',
			'domain': domain,
			}		
	@api.multi
	def create_pay(self):
		if not self.amount:
			raise ValidationError("Enter The Amount.")
		
		if self.amount > self.balance:
			raise ValidationError("Amount is greater than rider balance.")
		journal = self.env['account.journal'].search([('type','=','cash')],limit=1)
		if self.amount <= self.balance:
			if not self.entry_id:
				move_id = self.env['account.move'].create({
					'journal_id':journal.id,
					'date':self.date,
					})

				self.entry_id = move_id.id

				payment_debit = self.create_entry_lines(self.vendor.property_account_payable_id.id,self.amount,0,"Rider Payment",self.vendor.id)
				payment_credit = self.create_entry_lines(self.pay_act.id,0,self.amount,"Rider Payment",self.vendor.id)

			self.state = 'validate'

		else:

			raise ValidationError("Amount is greater than payable amount.")


	@api.multi
	def validate(self):
		if not self.amount:
			raise ValidationError("Enter The Amount.")
		
		if self.amount > self.balance:
			raise ValidationError("Amount is greater than rider balance.")
		self.state = 'validate'


	def create_entry_lines(self,account,debit,credit,label,partner):
		self.env['account.move.line'].create({
			'account_id':account,
			'partner_id':partner,
			'name':label,
			'debit':debit,
			'credit':credit,
			'move_id':self.entry_id.id,
			})

	@api.multi 
	def unlink(self):
		if self.state == 'validate' :
			raise ValidationError("Cannot be Deleted.")
		new_record = super(rider_payment,self).unlink()
		return new_record

class address_ecube(models.Model):
	_name = "ecube.address"


	mobile = fields.Char(string="Mobile")
	name = fields.Char(string="Address")

class rejection_ecube(models.Model):
	_name = "rej.type"

	name = fields.Char(string="Name")

class charging_ecube(models.Model):
	_name = "charging.type"

	name = fields.Char(string="Name")
	active = fields.Boolean(string="Active" , default=True)
	so_type = fields.Selection([
		('foodpanda', 'Foodpanda'),
		('cc', 'Call Center'),
		], string="SO Type")


class deal_drinks_class(models.Model):
	_name = "pos.drinks.tree"
	
	product_id = fields.Many2one('product.product',string='Deal')
	drinks_id = fields.Many2one('product.product',string='Drinks')
	qty = fields.Float(string='Quantity')
	drinks_tree = fields.Many2one('sale.order')

class rider_wizard_class(models.TransientModel):
	""" Not use this class """
	_name = "pos.rider.wizard"

	rider_id = fields.Many2one('hr.employee',string='Rider',required=True)


	@api.multi
	def save_close(self):
		if self.rider_id:
			active_class = self.env['sale.order'].browse(self._context.get('active_ids'))
			active_class.rider_id = self.rider_id.id


class waiter_wizard_class(models.TransientModel):
	""" Not use this class """
	_name = "pos.waiter.wizard"


	waiter_id = fields.Many2one('hr.employee',string='Waiter',required=True)


	@api.multi
	def save_close(self):
		if self.waiter_id:
			active_class = self.env['sale.order'].browse(self._context.get('active_ids'))
			active_class.waiter = self.waiter_id.id

class void_wizard_class(models.TransientModel):
	_name = "pos.void.wizard"

	pass_id = fields.Char(string='Password', default=None,required=True)
	fake_password = fields.Char(string='fake_password', default=None)
	allow_void = fields.Boolean(string="Allow Void")
	resson_already_add = fields.Boolean(string="Resson Already Add")
	check_password_true = fields.Boolean(string="Password True")
	name = fields.Many2one('void.reason',string='Resaon')

	@api.multi
	def save_close(self):
		if self.resson_already_add == False:
			if self.name and self.allow_void:
				active_class = self.env['sale.order.line'].browse(self._context.get('active_ids'))
				active_class.void_reason = self.name.id
				active_class.void_chk = True
				sale_order = self.env['sale.order'].search([('id','=',active_class.order_id.id)])
				for sale in sale_order.order_line:
					sale.kot_check = False
				sale_order.kot_button = False
				active_class.minus_product()
				return {
				'type': 'ir.actions.client',
				'tag': 'reload',
				}
			else:
				raise ValidationError("Wrong password or proper reason is not given.")
		else:
			if self.check_password_true == False:
				raise ValidationError("Wrong password.")
			active_class = self.env['sale.order.line'].browse(self._context.get('active_ids'))
			sale_order = self.env['sale.order'].search([('id','=',active_class.order_id.id)])
			for sale in sale_order.order_line:
				sale.kot_check = False
			sale_order.kot_button = False
			active_class.allow_minus = True
			active_class.minus_product()
			return {
			'type': 'ir.actions.client',
			'tag': 'reload',
			}
		


	@api.onchange('pass_id')
	def get_pass(self):
		if self.pass_id:
			pass_user = self.env['res.users'].search([('id','=',self._uid)])
			if pass_user.pass_id == self.pass_id:
				self.check_password_true = True
				if self.resson_already_add == False:
					self.allow_void = True
			else:
				self.check_password_true = False
				self.allow_void = False

		else:
			self.allow_void = False



class void_reason_class(models.Model):
	_name = "void.reason"

	name = fields.Char(string='Resaon',required=True)




class customer_info_wizard_class(models.TransientModel):
	_name = "pos.custinfo.wizard"

	mobile = fields.Char()
	address = fields.Many2one('ecube.address', string="Address" )

	customer_from_address = fields.Char('Address' )
	cust_name = fields.Char('Customer Name' )
	delivery_instruct = fields.Char(string="Delivered Instruction")
	payment_type = fields.Selection([
		('cod', 'COD'),
		('online_pay', 'Online Payment'),
		], string="Payment Type",default='cod')
	block_cust = fields.Boolean(string="Block Customer")
	block_reason = fields.Char(string="Block Reason")
	so_type = fields.Selection([
		('so', 'Sale Order'),
		('foodpanda', 'Foodpanda'),
		('cc', 'Call Center'),
		('take_away', 'Take Away'),
		], string="SO Type")
	charge_type = fields.Many2one('charging.type', string="Charging Type")
	voucher = fields.Char('Voucher')
	voucher_amount = fields.Float(string="Voucher Amount")

	@api.multi
	def save_close(self):
		if not self.block_cust:
			active_class = self.env['sale.order'].browse(self._context.get('active_ids'))
			active_class.mobile = self.mobile
			# active_class.address = self.address.id
			active_class.customer_from_address = self.customer_from_address
			active_class.cust_name = self.cust_name
			active_class.delivery_instruct = self.delivery_instruct
			active_class.payment_type = self.payment_type
			if self.so_type == 'foodpanda' or self.so_type == 'cc':
				active_class.charge_type = self.charge_type
				active_class.get_rider_chk()
			if self.so_type == 'foodpanda':
				active_class.voucher = self.voucher
				active_class.voucher_amount = self.voucher_amount
				active_class.calculate_discount_amnount()
				active_class.calculate_discount_percent()
			if self.address:
				self.address.mobile = self.mobile
		else:
			raise ValidationError("Cannot punch info of block customer.")

	@api.onchange('mobile')
	def get_address(self):

		if self.mobile:
			if len(str(self.mobile)) !=11 or not self.mobile.isdigit():
				self.mobile=""
				self.cust_name=""
				self.address=False
				self.block_cust=False
				return {'value':{},'warning':{'title':
				'warning','message':"Invalid phone number"}}
		else:
			self.mobile=""
			self.cust_name=""
			self.address=False
			self.block_cust=False

		
		if self.mobile:
			active_block = self.env['sale.order'].search([('mobile','=',self.mobile),('block_cust','=',True)],limit=1,order="write_date desc")
			if active_block:
				self.block_cust = True
				self.address = active_block.address.id
				self.cust_name = active_block.cust_name
				self.block_reason = active_block.block_reason
			else:
				try:
					active_mobile = self.env['sale.order'].search([('mobile','=',self.mobile)],order="write_date desc")[0]
					if active_mobile:
						self.address = active_mobile.address.id
						self.cust_name = active_mobile.cust_name
				except Exception, e:
					print "Error ON SEARCH CUSTOMER MOBILE NO...{}".format(e)

class drinks_ecube_class(models.Model):
	_name = "ecube.drink"

	name = fields.Char(string="Name")



# class deal_wizard_class(models.Model):
# 	_name = "pos.deal.wizard"

# 	check = fields.Boolean(string="Check")
# 	wizard_deal_id = fields.One2many('pos.deal.tree','wizard_deal_tree')


# 	@api.onchange('check')
# 	def get_data(self):
# 		print "wizar bolan true check"
# 		print "ddddddddddddddddddddddd"
# 		print "ddddddddddddddddddddddd"


# class deal_wizard_class(models.Model):
# 	_name = "pos.deal.tree"

# 	sr_no = fields.Char(string="Sr No.")
# 	product_id = fields.Many2one('product.product',string='Drinks')
# 	drink_size = fields.Many2one('ecube.drink',string='Size')
# 	wizard_deal_tree = fields.Many2one('pos.deal.wizard')

