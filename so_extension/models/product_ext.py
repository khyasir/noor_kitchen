from datetime import timedelta
from odoo import api, fields, models
from odoo.tools.float_utils import float_round
import logging
_logger = logging.getLogger(__name__)

class ProductTemplate(models.Model):
    _inherit = "product.product"

    @api.model
    def get_all_products(self):

        data = {}
        uniqu_categ = []
        prod_env = self.env['product.product'].search([('finish','=',True),('pos_categ','!=',False)])
        for x in prod_env:
            if x.pos_categ.name not in uniqu_categ:
                uniqu_categ.append(x.pos_categ.name)

        vals = {}
        for cat in uniqu_categ:
            product_dict = {}
            products = self.env['product.product'].search([('pos_categ.name','=',cat)])
            for pro in products:
                product_dict[pro.name] = {
                                'name':str(pro.name)+' - '+str(pro.lst_price),
                                'category':pro.pos_categ.name,
                                'price':500,
                                'seq': pro.pos_categ.no,
                                'id':pro.id,
                                
                                }
            vals[cat] = product_dict

        return vals



    @api.onchange('drinks')
    def drink_size_change(self):
        if not self.drinks:
            self.drink_size = None


    @api.multi
    def write(self, vals):

        before=self.write_date
        super(ProductTemplate, self).write(vals)
        after = self.write_date
        if before != after:
            self.drink_size_change()

        return True



    # @api.model    
    # def entity_waiter(self):
        
    #     waiter_rec  = self.env['hr.employee'].search([])
    #     waiter = []
    #     for w in waiter_rec:
    #         waiter.append({
    #         'id' : w.id,
    #         'name' : w.name
    #         }
    #         )
    #     return waiter
            

        # vals = []
        # products = self.env['product.product'].search([])
        # for product in products:
        #     vals.append({
        #         'id': product.id,
        #         'name': product.display_name,
        #     })
        # return vals
