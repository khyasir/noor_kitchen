from datetime import timedelta
from odoo import api, fields, models
from odoo.tools.float_utils import float_round
import logging
from odoo.exceptions import Warning, ValidationError


_logger = logging.getLogger(__name__)


class SaleOrderEXT(models.Model):
	_inherit = "sale.order"

	@api.model
	def add_product_line(self, id, product_id):
		order = self.env['sale.order'].search([('id', "=", id)])
		# order.open_deal()
		if order.so_state in ['draft','kot'] :
			product = self.env['product.product'].search([('id', "=", product_id)])

			prod_line = self.env['sale.order.line'].search([('order_id.id', "=", order.id),('product_id.id','=',product_id),('kot_check','=',False),('void_chk','=',False)],limit=1)
			if order.kot_button == True:
				order.kot_button = False

			if prod_line.product_id.is_deal == False:
				if prod_line:
					prod_line.product_uom_qty = prod_line.product_uom_qty + 1
				else:
					vals = {
						'name': product.name,
						'product_id': product.id,
						'product_uom_qty': int(1),
						'price_unit': product.lst_price,
						'product_uom': product.uom_id.id,
						'order_id': order.id,

					}
					line = self.env['sale.order.line'].sudo().create(vals)
			else:
				vals = {
					'name': product.name,
					'product_id': product.id,
					'product_uom_qty': int(1),
					'price_unit': product.lst_price,
					'product_uom': product.uom_id.id,
					'order_id': order.id,

				}
				line = self.env['sale.order.line'].sudo().create(vals)

			# order.write({'order_line': [(6, 0,line.id)]})

			_logger.info(str(id) + " " + str(product_id))

			print product
			print product.is_deal
			if product.is_deal:
				if product.deal_id:
					for deal in product.deal_id:
						if deal.product_id.drinks:
							deal= 'deal-'+str(line.id)
							return deal

		else:
			raise ValidationError("Cannot create order lines at this stage.")

		if order.discount_percent:
			order.calculate_discount_amnount()
		
		return 'Simple'
	@api.model    
	def entity_waiter(self):
		user = self.env['res.users'].search([('id','=',self._uid)])
		waiter_rec  = self.env['hr.employee'].search([('waiter','=',True),('entity','=',user.psc_entity.id)])
		waiter = []
		for w in waiter_rec:
			waiter.append({
			'id' : w.id,
			'name' : w.name
			})


		return waiter
	

	@api.model    
	def entity_rider(self):
		user = self.env['res.users'].search([('id','=',self._uid)])
		rider_rec  = self.env['hr.employee'].search([('rider','=',True),('entity','=',user.psc_entity.id)])
		rider = []
		for r in rider_rec:
			rider.append({
			'id' : r.id,
			'name' : r.name
			})


		return rider

	@api.model
	def add_waiter(self, id, waiter_id):
		order = self.env['sale.order'].search([('id', "=", id)])
		try:
			if waiter_id:
				order.waiter = waiter_id
		except Exception, e:
			pass

	@api.model
	def add_rider(self, id, rider_id):
		order = self.env['sale.order'].search([('id', "=", id)])
		try:
			if rider_id:
				order.rider_id = rider_id
		except Exception, e:
			pass

	@api.model
	def cash_update(self, id, cash_value):
		cash_value_pos =  cash_value
		parent_id = id
		self.env.cr.execute("UPDATE sale_order SET cash = "+str(cash_value_pos)+"  WHERE id = "+str(parent_id)+"")
		# order = self.env['sale.order'].search([('id', "=", id)])
		# order.cash = cash_value
		# order.change = order.amount_total - order.cash
		
	# @api.multi
	# def open_deal(self):
	#   print "ddddddddddddddddddanish"
	#   print "ddddddddddddddddddanish"
	#   return {'name': 'Deal Drinks',
	#       'domain': [],
	#       'res_model': 'pos.deal.wizard',
	#       'type': 'ir.actions.act_window',
	#       'view_mode': 'form',
	#       'view_type': 'form',
	#       'context': {'default_check':True},
	#       'target': 'new', }




class deal_wizard_class(models.Model):
	_name = "pos.deal.wizard"

	check = fields.Boolean(string="Check")
	product_id = fields.Many2one('product.product',string='Deal')
	sale_id = fields.Many2one('sale.order',string='So')
	sale_order_line_id = fields.Many2one('sale.order.line',string='SO Line')
	wizard_deal_id = fields.One2many('pos.deal.tree','wizard_deal_tree')



	@api.onchange('check')
	def get_data(self):
		inv = []
		count = 0
		for x in self.product_id.deal_id:
			if x.product_id.drinks:
				temp = int(x.quantity)
				for z in range(temp):
					count = count + 1
					inv.append({
						'sr_no':'Drink'+' '+str(count),
						'drink_size':x.product_id.drink_size.id,
						'product_id': x.product_id.id,
						'wizard_deal_tree':self.id,                  
						})

		self.wizard_deal_id = inv


	@api.multi
	def save_close(self):
		# order_line = self.env['sale.order.line'].search([('order_id.id','=',self.sale_id.id),('product_id.id','=',self.product_id.id),('kot_check','=',False)])
		
		# if order_line:
		#   order_line.product_uom_qty = order_line.product_uom_qty + 1
		# else:
		#   create_order_tree = self.env['stock.move'].create({
		#           'name': self.product_id.name,
		#           'product_id': self.product_id.id,
		#           'product_uom_qty': int(1),
		#           'price_unit': self.product_id.lst_price,
		#           'product_uom': self.product_id.uom_id.id,
		#           'order_id': self.sale_id.id,
		#       })

		if self.wizard_deal_id:
			
			for rec in self.wizard_deal_id:
				print "dddddddddddddddd"
				if rec.sr_no and not rec.product_id:
					raise ValidationError("Please select drinks first.")
				drinks_rec = self.env['pos.drinks.tree'].search([('product_id.id','=',self.product_id.id),('drinks_tree.id','=',self.sale_id.id),('drinks_id.id','=',rec.product_id.id)])
				if drinks_rec:
					drinks_rec.qty = drinks_rec.qty + 1
				else:
					create_deal_tree = self.env['pos.drinks.tree'].create({
							'product_id': self.product_id.id,
							'drinks_id': rec.product_id.id,
							'qty': 1,
							'drinks_tree': self.sale_id.id,
						})

		drink_list = []
		for x in self.wizard_deal_id:
			drink_list.append(x.product_id.id)

		drink_dict = {}
		for x in drink_list:
			if not x in drink_dict:
				drink_dict[x] = 1
			else:
				drink_dict[x] = drink_dict[x] + 1

		drink_list2 = []
		for x in drink_dict:
			cust_drink_rec = self.env['custom.drink'].create({
				'product_id':x,
				'qty':int(drink_dict[x]),
				})
			cust_drink_rec.get_rec_field()
			drink_list2.append(cust_drink_rec.id)


		# if self.sale_order_line_id:
		order_lines = self.env['sale.order.line'].search([('id','=',self.sale_order_line_id.id)])
		order_lines.drink_detail = [(6, 0, drink_list2)]
		# else:
		# 	order_lines = self.env['sale.order.line'].search([('order_id','=',self.sale_id.id)])
		# 	order_lines[len(order_lines)-1].drink_detail = [(6, 0, drink_list2)]


class deal_wizard_class(models.Model):
	_name = "pos.deal.tree"

	sr_no = fields.Char(string="Drinks No.")
	product_id = fields.Many2one('product.product',string='Drinks')
	drink_size = fields.Many2one('ecube.drink',string='Size')
	wizard_deal_tree = fields.Many2one('pos.deal.wizard')

