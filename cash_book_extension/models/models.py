# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import Warning, ValidationError
import collections

class account_bank_extension(models.Model):
	_inherit = 'account.bank.statement'

	@api.onchange('cash','bank')
	def cash_journal(self):
		if self.cash == True:
			journal = self.env['account.journal'].search([('code','=',"CASH")])
			self.journal_id = journal.id
			return {'domain':{'account_id':[('cash','=',True )]}}
		if self.bank == True:
			journal = self.env['account.journal'].search([('code','=',"BANK")])
			self.journal_id = journal.id

			return {'domain':{'account_id':[('bank','=',True ),('cash','=',False )]}}

	account_id = fields.Many2one('account.account', string = "Account Head", domain = [('cash','=',True)])
	account_m2m = fields.Many2many('account.account', string = "Accounts M2M")
	payment_journal = fields.Many2one('account.journal', string = "Payment Journal" , default = cash_journal)

	journal_id = fields.Many2one('account.journal', default = cash_journal)
	state = fields.Selection([('open', 'New'), ('in_progress', 'In Progress'),('confirm', 'Validated')], string='Status', required=True, readonly=True, copy=False, default='open')
	current_balance = fields.Float(string = "Current Balance", compute = "compute_balance")
	cash = fields.Boolean(string = "Cash")
	bank = fields.Boolean(string = "Bank")
	five_thousand = fields.Integer('5000')
	one_thousand = fields.Integer('1000')
	five_hundred = fields.Integer('500')
	one_hundred = fields.Integer('100')
	fifty = fields.Integer('50')
	twenty = fields.Integer('20')
	ten = fields.Integer('10')
	five = fields.Integer('5')
	two = fields.Integer('2')
	one = fields.Integer('1')
	total = fields.Integer('Total Cash')
	related_user = fields.Many2one('res.users',string="Related User")

	@api.onchange('cash')
	def get_account_id(self):
		account_rec = self.env['account.account'].search([('cash','=',True),('related_user','=',self._uid)])
		if account_rec:
			self.account_id = account_rec.id

		

	@api.onchange('account_id')
	def get_related_user(self):
		if self.cash == True:
			self.related_user = self.account_id.related_user.id
		
		# account_list = []
		# if self._uid != 1:
		# 	accounts = self.env['account.account'].search([('account_show','!=',True)])
		# else:
		# 	accounts = self.env['account.account'].search([])

		# for account in accounts:
		# 	account_list.append(account.id)

		# self.account_m2m = [(6,0,account_list)]



	@api.constrains('line_ids')
	def check_cheque_repeatetion(self):
		items= []
		flag = 0
		for x in self.line_ids:
			if x.cheque_book:
				items.append(x.cheque_book.id)
		counter=collections.Counter(items)
		for x in counter.values():
			if x > 1:
				flag = 1
		if flag == 1:
			raise ValidationError('Same Cheque exists multiple times')

	@api.onchange('account_id')
	def compute_name(self):
		if self.account_id:
			self.name = str(self.account_id.code) + " - " + str(self.account_id.name)

	@api.onchange('five_thousand','one_thousand','five_hundred','one_hundred','fifty','twenty','ten','five','two','one')
	def get_tot(self):
		five_thousand = 0
		one_thousand = 0
		five_hundred = 0
		one_hundred = 0
		fifty = 0
		twenty = 0
		ten = 0
		five = 0
		two = 0
		one = 0
		five_thousand = 5000 * self.five_thousand
		one_thousand = 1000 * self.one_thousand
		five_hundred = 500 * self.five_hundred
		one_hundred = 100 * self.one_hundred
		fifty = 50 * self.fifty
		twenty = 20 * self.twenty
		ten = 10 * self.ten
		five = 5 * self.five
		two = 2 * self.two
		one = 1 * self.one
		self.total = five_thousand + one_thousand + five_hundred + one_hundred + fifty + twenty + ten + five + two + one

	@api.model
	def create(self, vals):
		self.env['ir.rule'].clear_cache()
		new_record = super(account_bank_extension, self).create(vals)
		new_record.check_single_date()

		return new_record

	@api.multi
	def write(self,vals):
		
		super(account_bank_extension, self).write(vals)
		self.check_single_date()
		return True
		
	@api.one
	def compute_balance(self):
		debit = 0
		credit = 0
		entries = self.env['account.move.line'].search([('account_id','=',self.account_id.id),('date','<=',self.date)])
		for amounts in entries:
			debit = debit + amounts.debit
			credit = credit + amounts.credit
		self.current_balance = debit - credit

	@api.multi
	def in_progress(self):
		self.state = "in_progress"

	@api.multi
	def validate_entries(self):
		self.post()
		for x in self.line_ids:
			if self.cash == True:
				x.journal_entry_id.ref = "Cash"
			if self.bank == True:
				x.journal_entry_id.ref = "Bank"
			x.journal_entry_id.post()
			if x.head_wise_id:
				x.head_wise_id.lock = True
		self.state = "confirm"
			
	@api.multi
	def reset_progress(self):
		self.state = "in_progress"
		for lines in self.line_ids:
			lines.journal_entry_id.button_cancel()
			if lines.head_wise_id:
				lines.head_wise_id.lock = False

	def check_single_date(self):
		transactions = self.env['account.bank.statement'].search([('date','=',self.date),('account_id','=',self.account_id.id),('id','!=',self.id)])
		if len(transactions) > 0:
			raise  ValidationError('Multiple accounts in same date are not allowed')
			
	@api.multi
	def post(self):
		
		journal_entries = self.env['account.move']
		journal_entries_lines = self.env['account.move.line']
		for x in self.line_ids:
			if not x.account_id and not x.head_wise_id:
				raise  ValidationError('Please associate Account head')
			x.check_validity()
			if not x.journal_entry_id:
				ref_ecube = ""
				if self.cash == True:
					ref_ecube = "Cash"
				if self.bank == True:
					ref_ecube = "Bank"
				create_journal = journal_entries.create({
					'journal_id': self.journal_id.id,
					'date':x.date,
					'ref':ref_ecube,
				})
				create_journal = create_journal.id
				x.journal_entry_id = create_journal
			else:
				create_journal = x.journal_entry_id	
				create_journal.button_cancel()
				create_journal = create_journal.id

			for lines in x.journal_entry_id.line_ids:
				lines.unlink()
				
			if x.head_wise_id:
				if x.paid > 0:
					for y in x.head_wise_id.head_wise_tree:
						create_debit = self.create_entry_lines(y.account_id.id,y.paid,0,create_journal,x.name)
					create_credit = self.create_entry_lines(self.account_id.id,0,x.paid,create_journal,x.name)
				if x.received > 0:
					for y in x.head_wise_id.head_wise_tree:
						create_credit = self.create_entry_lines(y.account_id.id,0,y.received,create_journal,x.name)
					create_debit = self.create_entry_lines(self.account_id.id,x.received,0,create_journal,x.name)
			else:

				if x.amount > 0:
					create_debit = self.create_entry_lines(self.account_id.id,abs(x.amount),0,create_journal,x.name)
					create_credit = self.create_entry_lines(x.account_id.id,0,abs(x.amount),create_journal,x.name)
				if x.amount < 0:
					create_debit = self.create_entry_lines(x.account_id.id,abs(x.amount),0,create_journal,x.name)
					create_credit = self.create_entry_lines(self.account_id.id,0,abs(x.amount),create_journal,x.name) 
			create_journal = x.journal_entry_id

	def create_entry_lines(self,account,debit,credit,entry_id,name):
		self.env['account.move.line'].create({
			'account_id':account,
			'name':name,
			'debit':debit,
			'credit':credit,
			'move_id':entry_id,
		})

	@api.multi
	def unlink(self):
		if self.state != "open":
			raise  ValidationError('You cannot delete this record')
		super(account_bank_extension, self).unlink()
		return True

class account_bank_extension_line(models.Model):
	_inherit = 'account.bank.statement.line'


	@api.model
	def getUserShowAccount(self):
		print "-----------"
		print self.env.user.show_all_accounts
		if self.env.user.show_all_accounts != True:
			return [('account_show', '=', False)]
		if self.env.user.show_all_accounts == True:
			return None
	account_id =  fields.Many2one('account.account',string="Account" )
	journal_entry_id = fields.Many2one('account.move',string="Journal Id")
	bank = fields.Many2one('account.account',string="Credit")
	cheque_book = fields.Many2one('cheque.book.tree',string="Cheque Number")
	paid = fields.Float(string="Paid")
	received = fields.Float(string="Received")
	cheque_boolean = fields.Boolean()
	head_wise_id = fields.Many2one('head.wise.entries',string='Multiple Heads')

	@api.onchange('head_wise_id')
	def get_total(self):
		if self.head_wise_id:
			received = 0
			paid = 0
			description = ""
			for x in self.head_wise_id.head_wise_tree:
				received = received + x.received
				paid = paid + x.paid
				description = description + "/" + x.desc
				
			self.received = received
			self.paid = paid
			self.name = description[1:]
			
	@api.model
	def create(self, vals):
		new_record = super(account_bank_extension_line, self).create(vals)
		new_record.check_dates()
		new_record.check_repeat_entries()
		if new_record.cheque_book:
			new_record.cheque_book.issued = True
			new_record.cheque_book.pay_ref = new_record.name
			new_record.cheque_book.amount = abs(new_record.amount)
			new_record.cheque_book.date = new_record.date
			new_record.cheque_book.desc = new_record.journal_entry_id.name
					
		return new_record

	@api.multi
	def write(self,vals):
		if self.cheque_book:
			self.cheque_book.issued = False
			self.cheque_book.pay_ref = False
			self.cheque_book.amount = False
			self.cheque_book.date = False
			self.cheque_book.desc = False
			
		super(account_bank_extension_line, self).write(vals)
		if self.cheque_book:
			self.cheque_book.issued = True
			self.cheque_book.pay_ref = self.name
			self.cheque_book.amount = abs(self.amount)
			self.cheque_book.date = self.date
			self.cheque_book.desc = self.journal_entry_id.name
		self.check_dates()
		self.check_repeat_entries()
		
		return True

	@api.multi
	def unlink(self):
		if self.journal_entry_id:
			self.journal_entry_id.unlink()
		if self.cheque_book:
			self.cheque_book.issued = False
			self.cheque_book.pay_ref = False
			self.cheque_book.amount = False
			self.cheque_book.date = False
			self.cheque_book.desc = False
		super(account_bank_extension_line, self).unlink()
		return True

	def check_dates(self):
		if self.date != self.statement_id.date:
			raise  ValidationError('Date is not correct')

	def check_repeat_entries(self):
		entries = self.env['account.bank.statement.line'].search([('statement_id','=',self.statement_id.id),('id','!=',self.id),('name','=',self.name)])
		if entries:
			raise  ValidationError('You can not allow to create or update entry with same discription in single day.')



	@api.onchange('account_id','paid','received')
	def check_cheques(self):
		cheque_books = self.env['cheque.book.maintain'].search([('bank','=',self.statement_id.account_id.id)])
		if cheque_books and self.paid > 0:
			self.cheque_boolean = True
		else:
			self.cheque_boolean = False
			self.cheque_book = False

	@api.onchange('cheque_book')
	def get_cheque_number(self):
		if self.cheque_book:
			previous_label = self.name
			cheque_name = self.cheque_book.tree_cheque_no
			bank_name = self.cheque_book.chk_tree.bank.name
			final_name = previous_label + "  "+ bank_name + " Cheque # " + cheque_name
			self.name = final_name

	@api.multi
	def post(self):

		if self.head_wise_id:
			return {
				'type': 'ir.actions.act_window',
                'view_type': 'form',
                'name': 'Head Wise',
                'view_mode': 'form',
                'res_model': 'head.wise.entries',
                'target': 'new',
                'res_id': self.head_wise_id.id,
            }
		else:
			ctx = dict(
	            default_state_id= self.id,
	        )
			return {
				'type': 'ir.actions.act_window',
				'name': 'Head Wise',
				'res_model': 'head.wise.entries',
				'view_type': 'form',
				'view_mode': 'form',
				'target' : 'new',
				'context':ctx
			}

	def check_validity(self):
		if self.head_wise_id:
			total_paid = 0
			total_received = 0
			for x in self.head_wise_id.head_wise_tree:
				total_paid = total_paid + x.paid
				total_received = total_received + x.received
			print total_paid
			print total_received
			if self.paid != total_paid or self.received != total_received:
				self.paid = total_paid
				self.received = total_received

	@api.onchange('paid')
	def getamount(self):
		if self.paid:
			self.received = 0
			self.amount = self.paid * -1

	@api.onchange('received')
	def getamountrec(self):
		if self.received:
			self.paid = 0
			self.amount = self.received 

class account_move_extend(models.Model):
	_inherit = 'account.move'

	@api.multi
	def assert_balanced(self):
		if not self.ids:
			return True
		prec = self.env['decimal.precision'].precision_get('Account')

		self._cr.execute("""\
			SELECT      move_id
			FROM        account_move_line
			WHERE       move_id in %s
			GROUP BY    move_id
			HAVING      abs(sum(debit) - sum(credit)) > %s
			""", (tuple(self.ids), 10 ** (-max(5, prec))))
		return True

class HeadWiseEntries(models.Model):
	_name = 'head.wise.entries'

	name = fields.Char(default="Multiple Heads")
	line_id = fields.Integer()
	lock = fields.Boolean()
	head_wise_tree = fields.One2many('head.wise.lines','head_wise_id')
	state_id = fields.Many2one('account.bank.statement.line', string = "Statement")

	def update_cashbook(self):
		paid = 0
		received = 0
		for lines in self.head_wise_tree:
			paid = paid + lines.paid
			received = received + lines.received

		cash_lines = self.env['account.bank.statement.line'].search([('id','=',self.state_id.id)])
		
		if cash_lines.paid > 0:
			if received > 0:
				raise  ValidationError('You cannot receive amount in paid entry.')
		if cash_lines.received > 0:
			if paid > 0:
				raise  ValidationError('You cannot pay amount in receive entry.')
		cash_lines.paid = paid
		cash_lines.received = received
		cash_lines.head_wise_id = self.id
		cash_lines.account_id = False

	@api.model
	def create(self, vals):
		new_record = super(HeadWiseEntries, self).create(vals)
		new_record.update_record()			

		return new_record

	@api.multi
	def write(self,vals):
		super(HeadWiseEntries, self).write(vals)
		self.update_record()
		return True

	def update_record(self):
		paid = 0
		received = 0
		description = ""
		for lines in self.head_wise_tree:
			paid = paid + lines.paid
			received = received + lines.received
			description = description + "/" + lines.desc
		if paid != 0 and received != 0:
			raise  ValidationError('You can only pay or receive in one entry')

class HeadWiseEntriesLines(models.Model):
	_name = 'head.wise.lines'

	account_id = fields.Many2one('account.account', string = "Account Head")
	desc = fields.Char('Description', required = True)
	paid = fields.Float(string="Paid")
	received = fields.Float(string="Received")
	head_wise_id = fields.Many2one('head.wise.entries')

	@api.onchange('paid')
	def getamount(self):
		if self.paid:
			self.received = 0
			self.amount = self.paid * -1

	@api.onchange('received')
	def getamountrec(self):
		if self.received:
			self.paid = 0
			self.amount = self.received
