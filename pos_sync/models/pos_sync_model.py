# -*- coding: utf-8 -*-
from openerp import models, fields, api
from datetime import timedelta,datetime,date
from odoo.exceptions import Warning, ValidationError

import requests
import re
import inspect
import xmlrpclib
import webbrowser
# import xmlrpc.client

import os
# from pygame import mixer
# from playsound import playsound
import logging
_logger = logging.getLogger(__name__)
# from playsound import playsound
# # import winsound

# from gtts import gTTS





class EcubeRawAttendance_ext(models.Model):

	_inherit = "ecube.raw.attendance"



	# @api.model
	# def clone_sync_attendance(self):
		
	# 	try:
	# 		check = True
	# 		comp_rec = self.env['res.company'].search([])

	# 		srv, db = comp_rec[0].db_url_ecube, comp_rec[0].db_database
	# 		user , pwd = comp_rec[0].db_user_ecube, comp_rec[0].db_password_ecube
	# 		common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
	# 		common.version()
	# 		uid = common.authenticate(db, user, pwd, {})
	# 		api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)
			
	# 		print "final function start"
	# 		recs = self.env['ecube.raw.attendance'].search([('status','=','no_sync')])
	# 		print recs
	# 		attend_list = []
	# 		for attend in recs:
	# 			print attend

	# 			attend_form  = {
	# 			'machine_id':attend.machine_id,
	# 			'time':attend.time,
	# 			'date':attend.date,
	# 			'emp_code':attend.employee_id.emp_code,
	# 			'machine_entity':attend.entity.master_id,
	# 			'attendance_date':attend.attendance_date,
	# 			'attendence_done':attend.attendence_done,
	# 			'manual':attend.manual,
	# 			'attendance_date_time':attend.attendance_date_time,
	# 			'manual_date':attend.manual_date,
	# 			'attendance_type':attend.attendance_type,
	# 			'child_so_id':attend.id,
	# 			'entity':attend.machine_entity.master_id,
	# 			'id':attend.id,
	# 			}
	# 			attend_list.append(attend_form)

			
	# 		print attend_list
	# 		if len(attend_list) > 0:

	# 			print "//////////////////////"

	# 			master = 8
	# 			temp = api.execute_kw(db, uid, pwd, 'ecube.raw.attendance', 'raw_attendance_create_child_to_parent', [int(master)],{
	# 				'attend_list':attend_list,
	# 				})

	# 			if temp:
	# 				print "order update "
	# 				print len(temp)
	# 				count = 1
	# 				for index in temp:
	# 					print count
	# 					count = count + 1
	# 					so_rec = self.env['ecube.raw.attendance'].search([('id','=',index['child_id'])])
	# 					so_rec.master_id = index['parent_id']
	# 					so_rec.status = 'sync'

	# 	except Exception, e:
	# 		raise ValidationError("Connection Error ...{}".format(e))


	def child_un_sync_attendance(self, master_list):
		recs = self.env['ecube.raw.attendance'].search([('status','=','no_sync')])
		attend_list = []
		for attend in recs:
			print attend

			attend_form  = {
			'machine_id':attend.machine_id,
			'time':attend.time,
			'date':attend.date,
			'emp_code':attend.employee_id.emp_code,
			'machine_entity':attend.entity.master_id,
			'attendance_date':attend.attendance_date,
			'attendence_done':attend.attendence_done,
			'manual':attend.manual,
			'attendance_date_time':attend.attendance_date_time,
			'manual_date':attend.manual_date,
			'attendance_type':attend.attendance_type,
			'child_so_id':attend.id,
			'entity':attend.machine_entity.master_id,
			'id':attend.id,
			}
			attend_list.append(attend_form)

		

		return attend_list

	def child_attendance_status_update(self,temp_list):
		for index in temp_list:
			so_rec = self.env['ecube.raw.attendance'].search([('id','=',index['child_id'])])
			if so_rec:
				so_rec.master_id = index['parent_id']
				so_rec.status = 'sync'
		return True
		
		


# ======== hr.employee sync START =======
class HrEmployeeExt(models.Model):

	_inherit = "hr.employee"

	master_id = fields.Integer(string="Master ID")
	rider = fields.Boolean(string="Rider")
	waiter = fields.Boolean(string="Waiter")
	# active = fields.Boolean(string="Active")
	entity = fields.Many2one('psc.entity', string="Entity")
	emp_code = fields.Char(string="Employee Code")




# ======== hr.employee sync ENDS =======




# ======== res.partner sync START =======
class ResPartnerExt(models.Model):

	_inherit = "res.partner"

	master_id = fields.Integer(string="Master ID")




# ======== res.partner sync ENDS =======



# ======== ecube.address sync START =======
# class EcubeAddress(models.Model):
# 	_inherit = "ecube.address"
# 	master_id = fields.Integer(string="Master ID")

# 	@api.model
# 	def create(self, vals):

# 		rec = super(EcubeAddress, self).create(vals)
# 		check = True
# 		rec.SyncEcubeAddress(check)

# 		return rec


# 	@api.multi
# 	def write(self, vals):

# 		rec = super(EcubeAddress, self).write(vals)
# 		check = False
# 		self.SyncEcubeAddress(check)

# 		return rec

# 	@api.multi
# 	def unlink(self):

# 		rec = super(EcubeAddress, self).unlink()
# 		self.SyncUnlinkEcubeAddress()

# 		return rec

# 	def SyncEcubeAddress(self, check):
# 		try:

# 			company_record=self.env['res.company'].search([])
# 			srv, db = company_record.db_url_ecube, company_record.db_database
# 			user , pwd = company_record.db_user_ecube, company_record.db_password_ecube
# 			common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
# 			common.version()
# 			uid = common.authenticate(db, user, pwd, {})
# 			api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)

# 			if check:
# 				ecube_address_result = api.execute_kw(db, uid, pwd, 'ecube.address', 'create', [{
# 						'name': self.name,
# 						'mobile':self.mobile,
# 					}])
# 				self.master_id = ecube_address_result

# 			else:
# 				ecube_address_result = api.execute_kw(db, uid, pwd, 'ecube.address', 'write',[[self.master_id],{
# 					'name': self.name,
# 					'mobile':self.mobile,
# 				}])
# 			self.master_id = ecube_address_result

# 		except Exception, e:
# 			print "Error...{}".format(e)
# 			raise ValidationError("Error ...{}".format(e))

# 	def SyncUnlinkEcubeAddress(self):
# 		try:

# 			company_record=self.env['res.company'].search([])
# 			srv, db = company_record.db_url_ecube, company_record.db_database
# 			user , pwd = company_record.db_user_ecube, company_record.db_password_ecube
# 			common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
# 			common.version()
# 			uid = common.authenticate(db, user, pwd, {})
# 			api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)

# 			ecube_address_result = api.execute_kw(db, uid, pwd, 'ecube.address', 'unlink',[[self.master_id],{}])

# 		except Exception, e:
# 			print "Error...{}".format(e)
# 			raise ValidationError("Error ...{}".format(e))

# # ======== ecube.address sync ENDS ========

# ======== product.product sync START =======
class ProductProductSyncExtend(models.Model):
	_inherit = "product.product"
	master_id = fields.Integer(string="Master ID")



	def product_create_update(self, prod_list):
		temp_list = []
		for x in prod_list:
			sale_tax_id_list = []
			sale_tax_id_list.append(x['tax_id'])
			
			product_rec = self.env['product.product'].sudo().search([('master_id','=',x['master_id'])])
			if product_rec:
				product_rec.name = x['name']
				product_rec.active = x['active']
				product_rec.pos_categ  = x['pos_categ']
				product_rec.type = x['type']
				product_rec.lst_price = x['lst_price']
				product_rec.is_deal = x['is_deal']
				product_rec.finish = x['finish']
				product_rec.master_id = x['master_id']
				product_rec.taxes_id = [(6, 0, sale_tax_id_list)]
				product_rec.other_tax = x['other_tax']

			else:
				product_rec = self.env['product.product'].create({
					'name': x['name'],
					'pos_categ': x['pos_categ'],
					'type': x['type'],
					'lst_price': x['lst_price'],
					'is_deal': x['is_deal'],
					'finish': x['finish'],
					'master_id': x['master_id'],
					})
			if product_rec:
				if product_rec.deal_id:
					product_rec.deal_id.unlink()
				if product_rec.is_deal == True:
					for deal in x['prod_deal_list']:
						product_deal = self.env['product.deal'].create({
							'product_id': deal['product_id'],
							'quantity': deal['quantity'],
							'deal_tree': product_rec.id,
							})
						
				print "tas,............."

				product_rec.drinks =  x['drinks']
				product_rec.taxes_id = [(6, 0, sale_tax_id_list)]
				product_rec.other_tax = x['other_tax']
				if product_rec.drinks == True:
					product_rec.drink_size = x['drink_size']
			tax_detail = ""
			if product_rec.taxes_id:
				for tax in product_rec.taxes_id:
					tax_detail = tax_detail +" "+ str(tax.name)

			tax_detail_credit_card = ""
			if product_rec.other_tax:
				for tax in product_rec.other_tax:
					tax_detail_credit_card = tax_detail_credit_card +" "+ str(tax.name)

			
			deal_detail = ""
			if product_rec.is_deal == True:
				for deal_id in product_rec.deal_id:
					deal_detail = deal_detail+" "+str(deal_id.product_id.name)+" - "+str(deal_id.quantity)

			temp_dict = {
			'parent_id':product_rec.master_id,
			'child_id':product_rec.id,
			'name':product_rec.name,
			'sale_price':product_rec.lst_price,
			'sale_tax':tax_detail,
			'other_tax':tax_detail_credit_card,
			'status_active':product_rec.active,
			'pos_categ': str(product_rec.pos_categ.name),
			'deal_detail':deal_detail,
			}
			temp_list.append(temp_dict)
		return temp_list



	def product_inactive_update(self, prod_list):
		temp_list = []
		tax_detail = ""
		deal_detail = ""
		for x in prod_list:
			print x['product_id']
			product_rec = self.env['product.product'].sudo().search([('id','=',x['product_id'])])
			print product_rec
			if product_rec:
				product_rec.active = False


				if product_rec.taxes_id:
					for tax in product_rec.taxes_id:
						tax_detail = tax_detail +" "+ str(tax.name)

			
				if product_rec.is_deal == True:
					for deal_id in product_rec.deal_id:
						deal_detail = deal_detail+" "+str(deal_id.product_id.name)+" - "+str(deal_id.quantity)

				temp_dict = {
				'parent_id':product_rec.master_id,
				'child_id':product_rec.id,
				'name':product_rec.name,
				'sale_price':product_rec.lst_price,
				'sale_tax':tax_detail,
				'status_active':product_rec.active,
				'pos_categ': str(product_rec.pos_categ.name),
				'deal_detail':deal_detail,
				}
				temp_list.append(temp_dict)
		return temp_list

# 	@api.model
# 	def create(self, vals):

# 		rec = super(ProductProductSyncExtend, self).create(vals)
# 		check = True
# 		rec.SyncProductProduct(check)

# 		return rec


# 	@api.multi
# 	def write(self, vals):
# 		before=self.write_date
# 		rec = super(ProductProductSyncExtend, self).write(vals)
# 		after = self.write_date
# 		if before != after:
# 			check = False
# 			print "wwwwwwwwwwwwwwwwwwwwwwwwwwww"
# 			self.SyncProductProduct(check)

# 		return rec


# 	@api.multi
# 	def unlink(self):

# 		rec = super(ProductProductSyncExtend, self).unlink()
# 		self.SyncUnlinkProductProduct()

# 		return rec

# 	def SyncProductProduct(self, check):
# 		# try:

# 		company_record=self.env['res.company'].search([])
# 		srv, db = company_record.db_url_ecube, company_record.db_database
# 		user , pwd = company_record.db_user_ecube, company_record.db_password_ecube
# 		common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
# 		common.version()
# 		uid = common.authenticate(db, user, pwd, {})
# 		api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)
# 		print check
# 		print "........................."

# 		if check:
# 			print "!11111111111111111111111111"
# 			ecube_address_result = api.execute_kw(db, uid, pwd, 'product.product', 'create', [{
# 				'name': self.name,
# 				'sale_ok':self.sale_ok,
# 				'purchase_ok':self.purchase_ok,
# 				'can_be_expensed':self.can_be_expensed,
# 				'type':self.type,
# 				'default_code':self.default_code,
# 				'barcode':self.barcode,
# 				'total_prod_qty':self.total_prod_qty,
# 				'lst_price':self.lst_price,
# 				'standard_price':self.standard_price,
# 				'branch_price':self.branch_price,
# 				'inventory_value':self.inventory_value,
# 				'is_deal':self.is_deal,
# 				'finish':self.finish,
# 				'drinks':self.drinks,
# 				'purchase_method':self.purchase_method,
# 				}])
# 			print "22222222222222222222222222222222222"
# 			print ecube_address_result
# 			self.master_id = ecube_address_result

# 		else:
# 			print "444444444444444444444444444444444444"
# 			ecube_address_result = api.execute_kw(db, uid, pwd, 'product.product', 'write',[[self.master_id],{
# 				'name': self.name,
# 				'sale_ok':self.sale_ok,
# 				'purchase_ok':self.purchase_ok,
# 				'can_be_expensed':self.can_be_expensed,
# 				'type':self.type,
# 				'default_code':self.default_code,
# 				'barcode':self.barcode,
# 				'total_prod_qty':self.total_prod_qty,
# 				'lst_price':self.lst_price,
# 				'standard_price':self.standard_price,
# 				'branch_price':self.branch_price,
# 				'inventory_value':self.inventory_value,
# 				'is_deal':self.is_deal,
# 				'finish':self.finish,
# 				'drinks':self.drinks,
# 				'purchase_method':self.purchase_method,
# 			}])
# 			self.master_id = ecube_address_result

# 		# except Exception, e:
# 		# 	print "Error...{}".format(e)
# 		# 	raise ValidationError("Error ...{}".format(e))

# 	def SyncUnlinkProductProduct(self):
# 		try:

# 			company_record=self.env['res.company'].search([])
# 			srv, db = company_record.db_url_ecube, company_record.db_database
# 			user , pwd = company_record.db_user_ecube, company_record.db_password_ecube
# 			common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
# 			common.version()
# 			uid = common.authenticate(db, user, pwd, {})
# 			api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)

# 			ecube_address_result = api.execute_kw(db, uid, pwd, 'product.product', 'unlink',[[self.master_id],{}])

# 		except Exception, e:
# 			print "Error...{}".format(e)
# 			raise ValidationError("Error ...{}".format(e))

# # ======== product.product sync ENDS ========


	
# # ======== res.users sync START =======
# class SyncResUserExtend(models.Model):
# 	_inherit = "res.users"
# 	master_id = fields.Integer(string="Master ID")

# 	@api.model
# 	def create(self, vals):

# 		rec = super(SyncResUserExtend, self).create(vals)
# 		check=True
# 		rec.SyncResUsers(check)

# 		return rec


# 	@api.multi
# 	def write(self, vals):

# 		rec = super(SyncResUserExtend, self).write(vals)
# 		check=False
# 		self.SyncResUsers(check)

# 		return rec

# 	@api.multi
# 	def unlink(self):

# 		rec = super(SyncResUserExtend, self).unlink()
# 		self.SyncUnlinkResUsers(check)

# 		return rec

# 	def SyncResUsers(self, check):
# 		try:

# 			company_record=self.env['res.company'].search([])
# 			srv, db = company_record.db_url_ecube, company_record.db_database
# 			user , pwd = company_record.db_user_ecube, company_record.db_password_ecube
# 			common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
# 			common.version()
# 			uid = common.authenticate(db, user, pwd, {})
# 			api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)

# 			if check:
# 				ecube_address_result = api.execute_kw(db, uid, pwd, 'res.users', 'create', [{
# 						'name': self.name,
# 						'login':self.login,
# 						'phone':self.phone,
# 						'mobile':self.mobile,
# 						'fax':self.fax,
# 					}])
# 				self.master_id = ecube_address_result
# 			else:
# 				ecube_address_result = api.execute_kw(db, uid, pwd, 'res.users', 'write', [[self.master_id],{
# 						'name': self.name,
# 						'login':self.login,
# 						'phone':self.phone,
# 						'mobile':self.mobile,
# 						'fax':self.fax,
# 					}])
# 				self.master_id = ecube_address_result

# 		except Exception, e:
# 			print "Error...{}".format(e)
# 			raise ValidationError("Error ...{}".format(e))


# 	def SyncUnlinkResUsers(self):
# 		try:

# 			company_record=self.env['res.company'].search([])
# 			srv, db = company_record.db_url_ecube, company_record.db_database
# 			user , pwd = company_record.db_user_ecube, company_record.db_password_ecube
# 			common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
# 			common.version()
# 			uid = common.authenticate(db, user, pwd, {})
# 			api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)

# 			ecube_address_result = api.execute_kw(db, uid, pwd, 'res.users', 'unlink',[[self.master_id],{}])

# 		except Exception, e:
# 			print "Error...{}".format(e)
# 			raise ValidationError("Error ...{}".format(e))


# ======== res.users sync ENDS ========


# ======== pos.category.ecube sync STARTS ========
class PosCategoryEcubeExt(models.Model):
	_inherit = 'pos.category.ecube'

	master_id = fields.Integer(string="Master ID")
# ======== pos.category.ecube sync ENDS ========


# ======== ecube.drink sync STARTS ========
class PosCategoryEcubeExt(models.Model):
	_inherit = 'ecube.drink'

	master_id = fields.Integer(string="Master ID")
# ======== ecube.drink sync ENDS ========


# ======== psc.entity sync STARTS ========
class PscEntityExt(models.Model):
	_inherit = 'psc.entity'

	master_id = fields.Integer(string="Master ID")
# ======== psc.entity sync ENDS ========


# ======== account.tax sync STARTS ========
class PscEntityExt(models.Model):
	_inherit = 'account.tax'

	master_id = fields.Integer(string="Master ID")
# ======== account.tax sync ENDS ========


# ======== void.reason sync STARTS ========
class PscEntityExt(models.Model):
	_inherit = 'void.reason'

	master_id = fields.Integer(string="Master ID")
# ======== void.reason sync ENDS ========


# ======== ecube.expense.type sync STARTS ========
class EcubeExpenseTypeExt(models.Model):
	_inherit = 'ecube.expense.type'

	master_id = fields.Integer(string="Master ID")
# ======== ecube.expense.type sync ENDS ========



# ======== res.company sync STARTS ========
class ResCompanyExt(models.Model):
	_inherit = 'res.company'

	db_url_ecube = fields.Char(string="URL")
	db_database = fields.Char(string="Database")
	db_user_ecube = fields.Char(string="User")
	db_password_ecube = fields.Char(string="Password")
	test_bool = fields.Boolean(string="Testing Boolean")

	def update_admin_credential_online_db(self , update_list):
		create_list = []
		rec = self.env['res.company'].search([])
		url = ""
		user = ""
		admin = ""
		password = ""
		for x in update_list:

			url = x['db_url_ecube']
			user = x['db_database']
			admin = x['db_user_ecube']
			password = x['db_password_ecube']

			rec.db_url_ecube = url
			rec.db_database = user
			rec.db_user_ecube = admin
			rec.db_password_ecube = password

		create_tree = {
		'url' : url,
		'database' : user,
		'user' : admin,
		'password' : password,
		}
		create_list.append(create_tree)
		return create_list
# ======== res.company sync ENDS ========


# ======== ecube.session sync STARTS ========
class EcubeSessionExt(models.Model):
	_inherit = 'ecube.session'

	master_id = fields.Char(string="Master ID")
	last_update = fields.Datetime(string="Last Update")
	parent_db_status = fields.Char(string="Parent db status")
	status = fields.Selection([
		('sync', 'Synched'),
		('no_sync', 'Not Synched'),
		], string="Status")



	# @api.multi
	# def write(self, vals):

	# 	before=self.write_date
	# 	rec = super(EcubeSessionExt, self).write(vals)
	# 	after = self.write_date
	# 	if before != after:
	# 		check = False
	# 		print "write function call"
	# 		self.SyncEcubeSession()

	# 	return rec

	def child_expense_tree(self , master_list):
		expense_tree = []
		data = {}
		for x in master_list:
			session = self.env['ecube.session'].search([('id','=',x['master_id'])])
			if session:
				if session.tree_link:
					for line in session.tree_link:
						data = {
						'expense_type' : line.expense_type.master_id,
						'descrip' : line.descrip,
						'amount' : line.amount,
						}
						expense_tree.append(data)
		if expense_tree:
			return expense_tree
					
		return False

	@api.multi
	def open_pressed(self):
		rec = self.env['ecube.session'].search([('user.id','=',self.user.id),('state','=','open'),('psc_entity.id','=',self.psc_entity.id)])
		if rec:
			raise ValidationError("Already a session is open with the same user. Close that first")
		else:
			self.state = 'open'

		sale_order = self.env['sale.order'].search([('so_state','in',['received','kot','invoiced']),('psc_entity','=',self.psc_entity.id)])
		if sale_order:
			for s in sale_order:
				s.session = self.id
		print date.today()
		print date.today()
		print " 111111111111111111111"
		print datetime.today()
		print datetime.today()
		self.start_date = datetime.today()
		
		DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
		date_ecube = datetime.strptime(self.start_date, DATETIME_FORMAT)
		self.session_date = date_ecube + timedelta(hours=5,minutes=1)
		self.session_date = date.today()
		self.SyncEcubeSession()
		self.get_rec_name()


	@api.multi
	def sync_session(self):
		self.SyncEcubeSession()


	@api.multi
	def update_session_all_order(self):
		self.SyncEcubeSession()
		self.clone_sync_session()

		
	def SyncEcubeSession(self):
		if self.master_id and self.status == "sync":
			pass
		else:
			try:

				comp_rec = self.env['res.company'].search([])

				srv, db = comp_rec[0].db_url_ecube, comp_rec[0].db_database
				user , pwd = comp_rec[0].db_user_ecube, comp_rec[0].db_password_ecube
				common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
				common.version()
				uid = common.authenticate(db, user, pwd, {})
				api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)

			except Exception, e:
				print "Connection Error...{}".format(e)
				return False

			try:
				master = int(self.id)
				master_id = int(self.master_id)

				ecube_session_rec = api.execute_kw(db, uid, pwd, 'ecube.session', 'search',[[['master_id','=',master],['id','=',master_id],['psc_entity','=',self.psc_entity.master_id]]])

				print ecube_session_rec
				print "111111111111111111"
				if not ecube_session_rec:
					print "create "
					ecube_address_result = api.execute_kw(db, uid, pwd, 'ecube.session', 'create', [{
							'opening_balance': self.opening_balance,
							'cash_in_hand': self.cash_in_hand,
							'branch_user': self.user.name,
							'branch_seq': self.sequence,
							'session_date': self.session_date,
							'start_date': self.start_date,
							'end_date': self.end_date,
							'psc_entity': self.psc_entity.master_id,
							'state': self.state,
							'master_id': int(self.id),
						}])
					if ecube_address_result:
						self.parent_db_status = self.state
						self.master_id = ecube_address_result
						self.status = "sync"
						self.last_update = self.write_date
						return True
				else:
					self.master_id = int(ecube_session_rec[0])
					self.status = "sync"
					return True



			except Exception, e:
				print "Error...{}".format(e)
				return False



	def clone_sync_session(self):
		try:
			
			print "final function start"
			recs_1 = self.env['sale.order'].search([('session','=',self.id),('so_state','in',('done','reject'))])
			recs_2 = self.env['sale.order'].search([('status','!=','sync'),('so_state','=','reject')])
			recs = recs_1 + recs_2
			print recs_1
			print recs_2
			print recs
			if recs:
				check = True
				comp_rec = self.env['res.company'].search([])

				srv, db = comp_rec[0].db_url_ecube, comp_rec[0].db_database
				user , pwd = comp_rec[0].db_user_ecube, comp_rec[0].db_password_ecube
				common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
				common.version()
				uid = common.authenticate(db, user, pwd, {})
				api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)
			so_list = []
			# cust_rec = api.execute_kw(db, uid, pwd, 'res.partner', 'search',[[['ref','=','Walkin']]])
			for so in recs:
				print so
				if so.so_state == 'reject':
					if so.status == 'sync':
						continue
					if so.rej_submit == False:
						continue
				order_line_list = []
				if so.session.master_id and so.session.status == 'sync':
					pass
				else:
					result = so.session.sync_session()
					if result == False:
						continue

				print "check 1"
				for x in so.order_line:
					x.set_name_field()
					order_line_list.append({
						'product_id': int(x.product_id.master_id),
						'product_uom_qty': x.product_uom_qty,
						'price_unit': x.price_unit,
						'price_subtotal': x.price_subtotal,
						'drinks_field': x.drinks_field,
						'kot': x.kot,
						'kot_check': x.kot_check,
						'void_chk': x.void_chk,
						'void_qty': x.void_qty,
						'invoice_check': x.invoice_check,
						'void_reason': int(x.void_reason.master_id),
						'order_id': int(so.master_id),
						})
				print "check 2"
				so_form  = {
				'master_id_so_id':so.master_id,
				'token':so.token,
				'online_order':so.online_order,
				'code':so.code,
				'foodpanda_restaurant_id_char':so.foodpanda_restaurant_id_char,
				'login_token':so.login_token,
				'rej_reason':so.rej_reason,
				'kot_time':so.kot_time,
				'inv_time':so.inv_time,
				'done_time':so.done_time,
				'order_time':so.order_time,
				'rec_time':so.rec_time,
				'reject_time':so.reject_time,
				'rejected_table':so.rejected_table,
				'pre_reject_stage':so.pre_reject_stage,
				'charge_type':so.charge_type.id,
				'rider_id':so.rider_id.master_id or False,
				'waiter':so.waiter.master_id,
				'mobile':so.mobile,
				'payment_type':so.payment_type,
				'amount_total':so.amount_total,
				'amount_untaxed':so.amount_untaxed,
				'discount_percent':so.discount_percent,
				'discount_amount':so.discount_amount,
				'delivery_charges':so.delivery_charges,
				'amount_tax':so.amount_tax,
				'order_date':so.order_date,
				'effective_date':so.effective_date,
				'seq_id':so.seq_id,
				'sequence':so.sequence,
				'order_time':so.order_time,
				# 'partner_id':cust_rec[0],
				'session':int(so.session.master_id),
				'no_cash':so.no_cash,
				'voucher_amount':so.voucher_amount,
				'psc_entity':so.psc_entity.master_id,
				'no_cash_desc':so.no_cash_desc,
				'cust_name':so.cust_name,
				'branch_user':so.branch_user,
				'so_state':so.so_state,
				'so_type':so.so_type,
				'child_so_id':so.id,
				'delivery_instruct':so.delivery_instruct or "",
				'address_line1':so.address_line1,
				'address_building':so.address_building,
				'customer_from_address':so.customer_from_address,
				'order_line_list':order_line_list,
				}
				so_list.append(so_form)

			print "lenght of list"
			print len(so_list)
			if len(so_list) > 0:

				master = 8

				print "function call child db to main db start"
				temp = api.execute_kw(db, uid, pwd, 'sale.order', 'child_settle_order_update', [int(master)],{
					'so_list':so_list,
					})

				print "function call return return child db to main db start"

				if temp:
					print "order update "
					print len(temp)
					count = 1
					for index in temp:
						print count
						count = count + 1
						# so_rec = self.env['sale.order'].search([('id','=',index['child_id'])])
						# so_rec.master_id = index['parent_id']
						# so_rec.status = 'sync'
						parent_id = index['child_id']
						master_id_ecube = index['parent_id']
						self.env.cr.execute("UPDATE sale_order SET master_id = "+str(master_id_ecube)+" WHERE id = "+str(parent_id)+"")
						self.env.cr.execute("UPDATE sale_order SET status = 'sync' WHERE id = "+str(parent_id)+"")


		
		except Exception, e:
			raise ValidationError("Connection Error ...{}".format(e))

	# def SyncUnlinkEntity(self):
	# 	try:
	# 		for x in self.sync_tree:
	# 			srv, db = x.branch.db_url_ecube, x.branch.db_database
	# 			user , pwd = x.branch.db_user_ecube, x.branch.db_password_ecube
	# 			common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
	# 			common.version()
	# 			uid = common.authenticate(db, user, pwd, {})
	# 			api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)
	# 			master = int(x.master_id)

	# 			ecube_address_result = api.execute_kw(db, uid, pwd, 'sale.order', 'unlink',[[master],{}])

	# 	except Exception, e:
	# 		print "Error...{}".format(e)
	# 		raise ValidationError("Error ...{}".format(e))



# ======== ecube.session sync ENDS ========




# ======== sale.order START =======
class SaleOrderExt(models.Model):
	_inherit = "sale.order"






	# def _generate_order_by(self, order_spec, query):


	# 	# my_order = "CASE WHEN state='draft'  THEN 0   WHEN state = 'assigned'  THEN 1 ELSE 2 END,  date desc"
	# 	print self     
	# 	# my_order = ""            
	# 	# if order_spec:
	# 	# print my_order
	# 	# print type(self.sequence)
	# 	# print self.sequence
	# 	# print self.sequence[-2:-1]
	# 	print query
	# 	print order_spec
	# 	seq_order = super(SaleOrderExt, self)._generate_order_by(order_spec, query)
	# 	# print seq_order
	# 	print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
	# 	return seq_order
			# return super(sale_order, self)._generate_order_by(order_spec, query) + ", " + my_order
		# else:
		# 	return " order by " + my_order



	# def _generate_order_by(self, order_spec, query):
	#     "correctly orders state field if state is in query"
	#     order_by = super(this_table_class, self)._generate_order_by(order_spec, query)
	#     if order_spec and 'state ' in order_spec:
	#         state_column = self._columns['state']
	#         state_order = 'CASE '
	#         for i, state in enumerate(state_column.selection):
	#             state_order += "WHEN %s.state='%s' THEN %i " % (self._table, state[0], i)
	#         state_order += 'END '
	#         order_by = order_by.replace('"%s"."state" ' % self._table, state_order)
	#     return order_by

	# is_synched = fields.Boolean(string="Is Synched")
	online_address = fields.Char('Online Address')

	# @api.multi
	# def accepted(self):
	# 	print "11111111111111111111111111"

	@api.onchange('so_state')
	def change_clone_sync_bool(self):
		self.clone_sync_bool = False

	# Sound thing start
	@api.model
	def create(self, vals):

		rec = super(SaleOrderExt, self).create(vals)
		
		# x = re.search("[a-zA-Z]", rec.sequence)
		# comp_rec = self.env['res.company'].search([])
		# if comp_rec.test_bool==True:
		# 	if x:
		# 		print rec.sequence
		# 	else: 
		# 		print inspect.stack()[3][3]
		# 		print inspect.stack()[2][3]
		# 		print inspect.stack()[1][3]
		# 		print inspect.stack()[0][3]

		# 		raise ValidationError("This Table does not contains Alphabat.."+str(rec.sequence)+"\n"+"Functions list\n"+"-->"+inspect.stack()[3][3]+"-->"+inspect.stack()[2][3]+"-->"+inspect.stack()[1][3]+"-->"+inspect.stack()[0][3])
		rec.duplicate_same_order()


		# branch_user = self.env['res.users'].search([('id','=',self._uid)])
		# rec.branch_user = branch_user.name
		# rec.play_sound()

		return rec

	def duplicate_same_order(self):
		if self.master_id !=0:
			so = self.env['sale.order'].search([('master_id','=',self.master_id),('id','!=',self.id)])
			if so:
				pass
				# raise ValidationError('this order is already created.')


	@api.multi
	def write(self, vals):

		before=self.write_date
		before_state = self.so_state
		
		# if 'so_state' in vals:
		# 	if self.so_state == 'done':
		# 		raise ValidationError('you cannot change the state of so , please contact your administrator')

		rec = super(SaleOrderExt, self).write(vals)
		after=self.write_date
		after_state = self.so_state
		
		if before != after:
			print "write function"
			print before_state
			if before_state == 'done':
				raise ValidationError('you cannot edit the table at settle stage, please contact your administrator')
			if after_state!=before_state:
				self.clone_sync_bool = False
			

		return rec


	def play_sound(self):
		print "play_sound function is strat"
		# try:
		# 	print "start try code."
		# 	file_path = str('/odoo/odoo-server')+"/"+str("order_get_sound.mp3")
		# 	playsound(file_path)
		# except Exception, e:
		# 	print "play sound function not working"
	
	def stop_sound(self):
		print "stop audio start"
		pass


	@api.model
	def play_sound_cron(self):
		sale_order_satat = self.env['sale.order'].search([('so_state','=','accepted')])
		if sale_order_satat:
			self.play_sound()
			# dirpath = os.getcwd()
			# file_path = str('/odoo/odoo-server')+"/"+str("order_get_sound.mp3")
			# playsound(file_path)









	@api.model
	def miss_data_on_main_db(self):
		print "00000000000000000000000000000000000000000000000000"
		print "00000000000000000000000000000000000000000000000000"
		try:
			check = True
			comp_rec = self.env['res.company'].search([])

			srv, db = comp_rec[0].db_url_ecube, comp_rec[0].db_database
			user , pwd = comp_rec[0].db_user_ecube, comp_rec[0].db_password_ecube
			common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
			common.version()
			uid = common.authenticate(db, user, pwd, {})
			api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)
			
			print "final function start"
			recs = self.env['sale.order'].search([('session','!=',False),('so_state','in',('done','reject'))])
			print len(recs)
			so_list = []
			count = 1
			for so in recs:
				print count
				print so
				count = count + 1
				if so.so_state == 'reject':
					if so.rej_submit == False:
						continue
				order_line_list = []
				cust_rec = api.execute_kw(db, uid, pwd, 'res.partner', 'search',[[['ref','=','Walkin']]])
				if so.session.master_id and so.session.status == 'sync':
					pass
				else:
					result = so.session.sync_session()
					if result == False:
						continue

				# for x in so.order_line:
				# 	x.set_name_field()
				# 	order_line_list.append({
				# 		'product_id': int(x.product_id.master_id),
				# 		'product_uom_qty': x.product_uom_qty,
				# 		'price_unit': x.price_unit,
				# 		'price_subtotal': x.price_subtotal,
				# 		'drinks_field': x.drinks_field,
				# 		'kot': x.kot,
				# 		'kot_check': x.kot_check,
				# 		'void_chk': x.void_chk,
				# 		'void_qty': x.void_qty,
				# 		'invoice_check': x.invoice_check,
				# 		'void_reason': int(x.void_reason.master_id),
				# 		'order_id': int(so.master_id),
				# 		})
				so_form  = {
				# 'rej_reason':so.rej_reason,
				# 'kot_time':so.kot_time,
				# 'inv_time':so.inv_time,
				# 'done_time':so.done_time,
				# 'order_time':so.order_time,
				# 'rec_time':so.rec_time,
				# 'reject_time':so.reject_time,
				# 'rejected_table':so.rejected_table,
				# 'pre_reject_stage':so.pre_reject_stage,
				# 'charge_type':so.charge_type.id,
				# 'rider_id':so.rider_id.master_id or False,
				# 'waiter':so.waiter.master_id,
				# 'mobile':so.mobile,
				# 'payment_type':so.payment_type,
				# 'amount_total':so.amount_total,
				# 'amount_untaxed':so.amount_untaxed,
				# 'discount_percent':so.discount_percent,
				# 'discount_amount':so.discount_amount,
				# 'delivery_charges':so.delivery_charges,
				# 'amount_tax':so.amount_tax,
				# 'order_date':so.order_date,
				# 'effective_date':so.effective_date,
				# 'seq_id':so.seq_id,
				# 'sequence':so.sequence,
				# 'order_time':so.order_time,
				# 'partner_id':cust_rec[0],
				# 'session':int(so.session.master_id),
				# 'no_cash':so.no_cash,
				# 'voucher_amount':so.voucher_amount,
				'psc_entity':so.psc_entity.master_id,
				# 'no_cash_desc':so.no_cash_desc,
				# 'cust_name':so.cust_name,
				# 'branch_user':so.branch_user,
				'so_state':so.so_state,
				'so_type':so.so_type,
				'child_so_id':so.id,
				# 'order_line_list':order_line_list,
				}
				so_list.append(so_form)
				so.status = 'no_sync'

			if len(so_list) > 0:

				master = 8
				temp = api.execute_kw(db, uid, pwd, 'sale.order', 'child_settle_order_update_miss_order', [int(master)],{
					'so_list':so_list,
					})
				print "=========="
				print temp
				if temp:
					for index in temp:
						so_rec = self.env['sale.order'].search([('id','=',index['child_id'])])
						# so_rec.master_id = index['parent_id']
						so_rec.status = 'sync'


		
		except Exception, e:
			raise ValidationError("Connection Error ...{}".format(e))









	# @api.model
	# def clone_sync_function(self):
	# 	# try:
	# 	check = True
	# 	comp_rec = self.env['res.company'].search([])

	# 	srv, db = comp_rec[0].db_url_ecube, comp_rec[0].db_database
	# 	user , pwd = comp_rec[0].db_user_ecube, comp_rec[0].db_password_ecube
	# 	common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
	# 	common.version()
	# 	uid = common.authenticate(db, user, pwd, {})
	# 	api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)
		
	# 	print "final function start"
	# 	recs = self.env['sale.order'].search([('status','=','no_sync'),('session','!=',False),('so_state','in',('done','reject'))],limit=1000)
	# 	so_list = []
	# 	print "limit 1000 table only selected............."
	# 	print len(recs)
	# 	for so in recs:
	# 		print so
	# 		if so.so_state == 'reject':
	# 			if so.rej_submit == False:
	# 				continue
	# 		order_line_list = []
	# 		cust_rec = api.execute_kw(db, uid, pwd, 'res.partner', 'search',[[['ref','=','Walkin']]])
	# 		if so.session.master_id and so.session.status == 'sync':
	# 			pass
	# 		else:
	# 			result = so.session.sync_session()
	# 			if result == False:
	# 				continue

	# 		for x in so.order_line:
	# 			x.set_name_field()
	# 			order_line_list.append({
	# 				'product_id': int(x.product_id.master_id),
	# 				'product_uom_qty': x.product_uom_qty,
	# 				'price_unit': x.price_unit,
	# 				'price_subtotal': x.price_subtotal,
	# 				'drinks_field': x.drinks_field,
	# 				'kot': x.kot,
	# 				'kot_check': x.kot_check,
	# 				'void_chk': x.void_chk,
	# 				'void_qty': x.void_qty,
	# 				'invoice_check': x.invoice_check,
	# 				'void_reason': int(x.void_reason.master_id),
	# 				'order_id': int(so.master_id),
	# 				})
	# 		so_form  = {
	# 		'rej_reason':so.rej_reason,
	# 		'kot_time':so.kot_time,
	# 		'inv_time':so.inv_time,
	# 		'done_time':so.done_time,
	# 		'order_time':so.order_time,
	# 		'rec_time':so.rec_time,
	# 		'reject_time':so.reject_time,
	# 		'rejected_table':so.rejected_table,
	# 		'pre_reject_stage':so.pre_reject_stage,
	# 		'charge_type':so.charge_type.id,
	# 		'rider_id':so.rider_id.master_id or False,
	# 		'waiter':so.waiter.master_id,
	# 		'mobile':so.mobile,
	# 		'payment_type':so.payment_type,
	# 		'amount_total':so.amount_total,
	# 		'amount_untaxed':so.amount_untaxed,
	# 		'discount_percent':so.discount_percent,
	# 		'discount_amount':so.discount_amount,
	# 		'delivery_charges':so.delivery_charges,
	# 		'amount_tax':so.amount_tax,
	# 		'order_date':so.order_date,
	# 		'effective_date':so.effective_date,
	# 		'seq_id':so.seq_id,
	# 		'sequence':so.sequence,
	# 		'order_time':so.order_time,
	# 		'partner_id':cust_rec[0],
	# 		'session':int(so.session.master_id),
	# 		'no_cash':so.no_cash,
	# 		'voucher_amount':so.voucher_amount,
	# 		'psc_entity':so.psc_entity.master_id,
	# 		'no_cash_desc':so.no_cash_desc,
	# 		'cust_name':so.cust_name,
	# 		'branch_user':so.branch_user,
	# 		'so_state':so.so_state,
	# 		'so_type':so.so_type,
	# 		'child_so_id':so.id,
	# 		'order_line_list':order_line_list,
	# 		}
	# 		so_list.append(so_form)

	# 	if len(so_list) > 0:

	# 		master = 8
	# 		temp = api.execute_kw(db, uid, pwd, 'sale.order', 'child_settle_order_update', [int(master)],{
	# 			'so_list':so_list,
	# 			})

	# 		print temp
	# 		print "fucntion return data start"
	# 		if temp:
	# 			for index in temp:
	# 				print index
	# 				print "                  "
	# 				so_rec = self.env['sale.order'].search([('id','=',index['child_id'])])
	# 				so_rec.master_id = index['parent_id']
	# 				so_rec.status = 'sync'
	# 		print "done table sync "
	# 		print "done table sync "
	# 		print "done table sync =================="


		
		# except Exception, e:
		# 	raise ValidationError("Connection Error ...{}".format(e))


			
	# @api.multi
	# def SyncSaleOrder(self):


	# 	try:
	# 		check = True
	# 		comp_rec = self.env['res.company'].search([])

	# 		srv, db = comp_rec[0].db_url_ecube, comp_rec[0].db_database
	# 		user , pwd = comp_rec[0].db_user_ecube, comp_rec[0].db_password_ecube
	# 		common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
	# 		common.version()
	# 		uid = common.authenticate(db, user, pwd, {})
	# 		api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)
	# 	except Exception, e:
	# 		raise ValidationError("Connection Error ...{}".format(e))

	# 	# search walik in customer in parent DB
	# 	cust_rec = api.execute_kw(db, uid, pwd, 'res.partner', 'search',[[['ref','=','Walkin']]])

	# 	# branch_user = self.env['res.users'].search([('id','=',self._uid)])


	# 	# if self.online_order == True:

	# 	parent_so = api.execute_kw(db, uid, pwd, 'sale.order', 'search',[[['master_id','=',self.id],['psc_entity','=',self.psc_entity.master_id]]])
	# 	print parent_so
	# 	print "uuuuuuuuuuuuuuuuuuuuuuuuuuuuu"
	# 	if parent_so:
	# 		print "write fucntion"
	# 		# if self.parent_db_created == True:

	# 		master = int(self.master_id)

	# 		# parent_so = api.execute_kw(db, uid, pwd, 'sale.order', 'search',[[['master_id','=',self.id],['psc_entity','=',self.psc_entity.master_id]]])
	# 		master = int(parent_so[0])
	# 		try:

	# 			session_id = api.execute_kw(db, uid, pwd, 'ecube.session', 'search',[[['master_id','=',self.session.id],['psc_entity','=',self.psc_entity.master_id]]])


	# 			# check if session not synched with main DB, Synch that first
	# 			if not session_id:
	# 				self.session.sync_session()

	# 			sess_id = int(session_id[0])


	# 			rider_master_id = api.execute_kw(db, uid, pwd, 'hr.employee', 'search',[[['id','=',self.rider_id.master_id]]])
	# 			if rider_master_id:
	# 				print "55555555555555555"
	# 				master_rider_id = int(rider_master_id[0])
	# 			else:
	# 				print "66666666666666666666666"
	# 				master_rider_id = False

	# 			""" waiter update on main db """
	# 			waiter_master_id = api.execute_kw(db, uid, pwd, 'hr.employee', 'search',[[['id','=',self.waiter.master_id]]])
	# 			if waiter_master_id:
	# 				waiter_master_id = int(waiter_master_id[0])
	# 			else:
	# 				waiter_master_id = False

	# 			ecube_address_result = api.execute_kw(db, uid, pwd, 'sale.order', 'write', [[master],{
	# 					# 'delivery_instruct': self.delivery_instruct,
	# 					# 'charge_type':self.charge_type.id,
	# 					'rej_reason':self.rej_reason,
	# 					'kot_time':self.kot_time,
	# 					'inv_time':self.inv_time,
	# 					'order_time':self.order_time,
	# 					'rec_time':self.rec_time,
	# 					'reject_time':self.reject_time,
	# 					'rejected_table':self.rejected_table,
	# 					'pre_reject_stage':self.pre_reject_stage,
	# 					'rej_approval':self.rej_approval,
	# 					'mobile':self.mobile,
	# 					'payment_type':self.payment_type,
	# 					'amount_total':self.amount_total,
	# 					'cash':self.cash,
	# 					'change':self.change,
	# 					'charge_type':self.charge_type.id,
	# 					'amount_untaxed':self.amount_untaxed,
	# 					'discount_percent':self.discount_percent,
	# 					'discount_amount':self.discount_amount,
	# 					'delivery_charges':self.delivery_charges,
	# 					'amount_tax':self.amount_tax,
	# 					'order_date':self.order_date,
	# 					# 'branch_user':branch_user.name,
	# 					'branch_user':self.branch_user,
	# 					'session':sess_id,
	# 					'rider_id':master_rider_id,
	# 					'waiter':waiter_master_id,
	# 					'effective_date':self.session.session_date,
	# 					# 'confirmation_date':self.confirmation_date,
	# 					'seq_id':self.seq_id,
	# 					# 'deiscount_allowed':self.deiscount_allowed,
	# 					# 'sale_counter':self.sale_counter,
	# 					# 'kot_counter':self.kot_counter,
	# 					'order_time':self.order_time,
	# 					# 'rec_time':self.rec_time,
	# 					# 'reject_time':self.reject_time,
	# 					# 'done_time':self.done_time,
	# 					# 'color':self.color,
	# 					# 'partner_id':cust_rec[0],
	# 					# 'allowed':self.allowed,
	# 					# 'kot_button':self.kot_button,
	# 					# 'inv_button':self.inv_button,
	# 					# 'block_cust':self.block_cust,
	# 					# 'session':self.session.master_id,
	# 					'no_cash':self.no_cash,
	# 					'voucher_amount':self.voucher_amount,
	# 					'no_cash_desc':self.no_cash_desc,
	# 					'so_state':self.so_state,
	# 					'so_type':self.so_type,
	# 					'master_id':self.id,
	# 					# 'session':self.session.master_id,
	# 					# 'drink_size':drink_size,
	# 				}])

	# 			if not ecube_address_result:
	# 				raise ValidationError("Order Not updation Failed.")

	# 			# Deleting order_lines from main_db start

	# 			temp_id = int(master)
	# 			sync_rec = api.execute_kw(db, uid, pwd, 'sale.order.line', 'search',[[['order_id','=',temp_id]]])
	# 			if sync_rec:
	# 				for x in sync_rec:
	# 					temp_child_ids = int(x)
	# 					so_line_unsync_result = api.execute_kw(db, uid, pwd, 'sale.order.line', 'unlink',[[temp_child_ids],{}])
			
	# 			# Deleting order_lines from main_db ends

	# 			for x in self.order_line:
	# 				print x.drinks_field
	# 				x.set_name_field()
	# 				print x.drinks_field
	# 				print "=========================="
	# 				sale_order_line_result = api.execute_kw(db, uid, pwd, 'sale.order.line', 'create', [{
	# 					'product_id': int(x.product_id.master_id),
	# 					'product_uom_qty': x.product_uom_qty,
	# 					'price_unit': x.price_unit,
	# 					'price_subtotal': x.price_subtotal,
	# 					'order_id': int(master),
	# 					'kot': x.kot,
	# 					'drinks_field': x.drinks_field,
	# 					'kot_check': x.kot_check,
	# 					'void_chk': x.void_chk,
	# 					'void_qty': x.void_qty,
	# 					'invoice_check': x.invoice_check,
	# 					'void_reason': int(x.void_reason.master_id),
	# 					# 'drink_size':drink_size,
	# 				}])

	# 				print "oooooooooooooooooooooooooooooooo"
	# 				temp = api.execute_kw(db, uid, pwd, 'sale.order.line', 'set_many2many_drinks', [int(sale_order_line_result)],{
	# 					'temp':100,
	# 					})
	# 				print "ppppppppppppppppppppp"

	# 			self.last_update = datetime.today()
	# 			self.status = 'sync'

	# 			return True

	# 		except Exception, e:
	# 			raise ValidationError("Update Error ...{}".format(e))


			

	# 	else:
	# 		try:
	# 			print "create function"

	# 			session_id = api.execute_kw(db, uid, pwd, 'ecube.session', 'search',[[['master_id','=',self.session.id],['psc_entity','=',self.psc_entity.master_id]]])
	# 			print "11111111111111111111"


	# 			# check if session not synched with main DB, Synch that first
	# 			if not session_id:
	# 				print "2222222222222222222222"
	# 				self.session.sync_session()

	# 			print "333333333333333333"
	# 			sess_id = int(session_id[0])
	# 			print "444444444444444444"


	# 			rider_master_id = api.execute_kw(db, uid, pwd, 'hr.employee', 'search',[[['id','=',self.rider_id.master_id]]])
	# 			if rider_master_id:
	# 				print "55555555555555555"
	# 				master_rider_id = int(rider_master_id[0])
	# 			else:
	# 				print "66666666666666666666666"
	# 				master_rider_id = False

	# 			""" waiter update on main db """
	# 			waiter_master_id = api.execute_kw(db, uid, pwd, 'hr.employee', 'search',[[['id','=',self.waiter.master_id]]])
	# 			if waiter_master_id:
	# 				waiter_master_id = int(waiter_master_id[0])
	# 			else:
	# 				waiter_master_id = False
	# 			print "0000000000000000000000000000000000000"
	# 			print self.seq_id
	# 			so_result = api.execute_kw(db, uid, pwd, 'sale.order', 'create', [{
	# 					# 'delivery_instruct': self.delivery_instruct,
	# 					'rej_reason':self.rej_reason,
	# 					'kot_time':self.kot_time,
	# 					'inv_time':self.inv_time,
	# 					'order_time':self.order_time,
	# 					'rec_time':self.rec_time,
	# 					'reject_time':self.reject_time,
	# 					'rejected_table':self.rejected_table,
	# 					'pre_reject_stage':self.pre_reject_stage,
	# 					'rej_approval':self.rej_approval,
	# 					'charge_type':self.charge_type.id,
	# 					'rider_id':master_rider_id,
	# 					'waiter':waiter_master_id,
	# 					'mobile':self.mobile,
	# 					'payment_type':self.payment_type,
	# 					'amount_total':self.amount_total,
	# 					'cash':self.cash,
	# 					'change':self.change,
	# 					'amount_untaxed':self.amount_untaxed,
	# 					'discount_percent':self.discount_percent,
	# 					'discount_amount':self.discount_amount,
	# 					'delivery_charges':self.delivery_charges,
	# 					'amount_tax':self.amount_tax,
	# 					'order_date':self.order_date,
	# 					'effective_date':self.session.session_date,
	# 					# 'confirmation_date':self.confirmation_date,
	# 					'seq_id':self.seq_id,
	# 					# 'deiscount_allowed':self.deiscount_allowed,
	# 					# 'sale_counter':self.sale_counter,
	# 					# 'kot_counter':self.kot_counter,
	# 					# 'kot_time':self.kot_time,
	# 					# 'inv_time':self.inv_time,
	# 					'order_time':self.order_time,
	# 					# 'rec_time':self.rec_time,
	# 					# 'reject_time':self.reject_time,
	# 					# 'done_time':self.done_time,
	# 					# 'color':self.color,
	# 					'partner_id':cust_rec[0],
	# 					# 'allowed':self.allowed,
	# 					# 'kot_button':self.kot_button,
	# 					# 'inv_button':self.inv_button,
	# 					# 'block_cust':self.block_cust,
	# 					# 'session':self.session.master_id,
	# 					'session':sess_id,
	# 					'no_cash':self.no_cash,
	# 					'voucher_amount':self.voucher_amount,
	# 					'psc_entity':self.psc_entity.master_id,
	# 					'no_cash_desc':self.no_cash_desc,
	# 					'cust_name':self.cust_name,
	# 					'branch_user':self.branch_user,
	# 					'so_state':self.so_state,
	# 					'sequence':self.sequence,
	# 					'so_type':self.so_type,
	# 					'master_id':self.id,
	# 					# 'session':self.session.master_id,
	# 					# 'drink_size':drink_size,
	# 				}])
	# 			print "================++++++++++++++++++++++++++++++++++++"
	# 			self.master_id = so_result

	# 			print "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
	# 			print self.master_id

	# 			for x in self.order_line:
	# 				print x
	# 				x.set_name_field()
	# 				print "bbbbbbbbbbbbbbbbbbbbbbbbbbb"

	# 				sale_order_line_result = api.execute_kw(db, uid, pwd, 'sale.order.line', 'create', [{
	# 					'product_id': int(x.product_id.master_id),
	# 					'product_uom_qty': x.product_uom_qty,
	# 					'price_unit': x.price_unit,
	# 					'price_subtotal': x.price_subtotal,
	# 					'kot': x.kot,
	# 					'drinks_field': x.drinks_field,
	# 					'kot_check': x.kot_check,
	# 					'void_chk': x.void_chk,
	# 					'void_qty': x.void_qty,
	# 					'invoice_check': x.invoice_check,
	# 					'void_reason': int(x.void_reason.master_id),
	# 					'order_id': int(self.master_id),
	# 				}])
	# 				temp = api.execute_kw(db, uid, pwd, 'sale.order.line', 'set_many2many_drinks', [int(sale_order_line_result)],{
	# 					'temp':100,
	# 					})


	# 			self.parent_db_created = True
	# 			self.last_update = datetime.today()
	# 			self.status = 'sync'



	# 			return True

	# 		except Exception, e:
	# 			raise ValidationError("SO Creation Error ...{}".format(e))







	@api.multi
	def reject_order(self):
		self.reject_time = datetime.now()
		self.rejected_table = True
		self.pre_reject_stage = self.so_state
		self.so_state = 'reject'
		print "reject "
		if self.online_order == True:
			self.rej_reason = "This is not my branch order"
			self.rej_submit = True
			""" online true only when so order create through online db to child db. """
			self.stop_sound()
			self.reject_sync_call()

	@api.multi
	def reject_sync_call(self):
		try:
			comp_rec = self.env['res.company'].search([])

			srv, db = comp_rec[0].db_url_ecube, comp_rec[0].db_database
			user , pwd = comp_rec[0].db_user_ecube, comp_rec[0].db_password_ecube
			common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
			common.version()
			uid = common.authenticate(db, user, pwd, {})
			api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)
			

			so_rec = api.execute_kw(db, uid, pwd, 'sale.order', 'search',[[['id','=',self.master_id],['psc_entity','=',self.psc_entity.master_id]]])
			if not so_rec:
				raise ValidationError('SO not found in Main Branch.')
			if so_rec:
				if len(so_rec) >1 :
					raise ValidationError('Many So Found.')

				master = int(so_rec[0])
				so_result = api.execute_kw(db, uid, pwd, 'sale.order', 'write', [[master],{
					'so_state': self.so_state,
					'rej_reason': self.rej_reason,
					'branch_user': self.branch_user,
					}])
				if not so_result:
					raise ValidationError('SO state not updated.')
		except Exception, e:
			raise ValidationError("Connection Error. Please try in while...{}".format(e))


	@api.multi
	def order_fetch(self):
		try:
			comp_rec = self.env['res.company'].search([])

			srv, db = comp_rec[0].db_url_ecube, comp_rec[0].db_database
			user , pwd = comp_rec[0].db_user_ecube, comp_rec[0].db_password_ecube
			common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
			common.version()
			uid = common.authenticate(db, user, pwd, {})
			api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)
			

			# so_rec = api.execute_kw(db, uid, pwd, 'sale.order', 'search',[[['order_not_feteh','=',True],['so_state','=','accepted'],['psc_entity','=',self.psc_entity.master_id]]])
			entity_id = []
			create_dict = {
				'entity_id':self.psc_entity.master_id,
			}

			entity_id.append(create_dict)			

			master = 8
			print "function call child to main"
			temp = api.execute_kw(db, uid, pwd, 'sale.order', 'order_sync_dict', [int(master)],{
				'entity_id':entity_id,
				})

			print "----------"
			print "----------"
			print "----------"
			print "----------"
			if not temp == False:
				self.online_order_create(temp)
				print "order done"
		except Exception, e:
			print "order fetch function error in connection"
			return



	def update_table_status_on_main_db_ecube(self , emphty_list):

		print emphty_list

		print "function start "
		so_recs = self.env['sale.order'].search([('so_state','in',['received','kot','invoiced']),('clone_sync_bool','=',False)])
		print so_recs
		print so_recs
		print so_recs
		temp_list = []
		create_list = []
		print so_recs
		print so_recs
		for x in so_recs:

			print "loop "

			if x.master_id:


				master = int(x.master_id)
				temp_dict = {
					'master_id':master,
					'so_state': x.so_state,
					'order_time': x.order_time,
					'rec_time': x.rec_time,
					'kot_time': x.kot_time,
					'inv_time': x.inv_time,
				}



				temp_list.append(temp_dict)

				print temp_list

			# if not created on main DB, Create it
			# else:


			# 	order_list = []
			# 	for y in x.order_line:
			# 		order_list.append({
			# 			'product_id': int(y.product_id.master_id),
			# 			'product_uom_qty': y.product_uom_qty,
			# 			'price_unit': y.price_unit,
			# 			'drinks_field': y.drinks_field,
			# 			'price_subtotal': y.price_subtotal,
			# 			})


			# 	create_dict = {
			# 		'mobile':x.mobile,
			# 		'payment_type':x.payment_type,
			# 		'amount_total':x.amount_total,
			# 		'cash':x.cash,
			# 		'rider_id': int(x.rider_id.master_id),
			# 		'change':x.change,
			# 		'charge_type':x.charge_type.id,
			# 		'amount_untaxed':x.amount_untaxed,
			# 		'discount_percent':x.discount_percent,
			# 		'discount_amount':x.discount_amount,
			# 		'delivery_charges':x.delivery_charges,
			# 		'amount_tax':x.amount_tax,
			# 		'order_date':x.order_date,
			# 		'order_time':x.order_time,
			# 		'effective_date':x.session.session_date,
			# 		'sequence':x.sequence,
			# 		'seq_id':x.seq_id,
			# 		'entity':x.psc_entity.master_id,
			# 		'session':x.session.id,
			# 		'no_cash':x.no_cash,
			# 		'no_cash_desc':x.no_cash_desc,
			# 		'cust_name':x.cust_name,
			# 		'branch_user':x.branch_user,
			# 		'so_state':x.so_state,
			# 		'so_type':x.so_type,
			# 		'master_id':x.id,
			# 		'order_list':order_list,
			# 	}

			# 	create_list.append(create_dict)
			# x.clone_sync_bool = True


		print "11111111111111111111"
		print "22222222222222222222"

		return temp_list


		# if len(temp_list) > 0:
		# 	print "update the status"
		# 	master = 8
		# 	temp = api.execute_kw(db, uid, pwd, 'sale.order', 'get_data', [int(master)],{
		# 		'temp_list':temp_list,
		# 		})

		# if len(create_list) > 0:
			
		# 	print "create the product first time"
			
		# 	master = 8
		# 	print "function call child to main"
		# 	temp = api.execute_kw(db, uid, pwd, 'sale.order', 'c_sync_sos', [int(master)],{
		# 		'create_list':create_list,
		# 		})

		# 	for index in temp:
		# 		so_rec = self.env['sale.order'].search([('id','=',index['child_id'])])
		# 		so_rec.master_id = index['parent_id']
		# 		so_rec.clone_sync_bool = True
		# 		so_rec.parent_db_created = True




	@api.model
	def clone_sync_status_update_func(self):
		pass

		# try:
		# 	comp_rec = self.env['res.company'].search([])

		# 	srv, db = comp_rec[0].db_url_ecube, comp_rec[0].db_database
		# 	user , pwd = comp_rec[0].db_user_ecube, comp_rec[0].db_password_ecube
		# 	common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
		# 	common.version()
		# 	uid = common.authenticate(db, user, pwd, {})
		# 	api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)
		# except Exception, e:
		# 	print "error in connection"
		# 	return

		

		# so_recs = self.env['sale.order'].search([('so_state','in',['received','kot','invoiced']),('clone_sync_bool','=',False)])
		# temp_list = []
		# create_list = []
		# print so_recs
		# print so_recs
		# for x in so_recs:

		# 	# search if SO exist in parent DB
		# 	entity_master_id = int(x.psc_entity.master_id)
		# 	so_id = int(x.id)
		# 	# cust_rec = api.execute_kw(db, uid, pwd, 'sale.order', 'search',[[['master_id','=',so_id],['psc_entity','=',entity_master_id],['so_state','!=','done']]])
			
		# 	# If SO already created on main, just update status
		# 	if x.master_id:


		# 		# master = int(cust_rec[0])
		# 		master = int(x.master_id)
		# 		temp_dict = {
		# 			'master_id':master,
		# 			'so_state': x.so_state,
		# 			'order_time': x.order_time,
		# 			'rec_time': x.rec_time,
		# 			'kot_time': x.kot_time,
		# 			'inv_time': x.inv_time,
		# 		}


		# 		# updating clone sync bool
		# 		x.clone_sync_bool = True


		# 		temp_list.append(temp_dict)

		# 	# if not created on main DB, Create it
		# 	else:

		# 		if x.session.master_id and x.session.status == 'sync':
		# 			pass
		# 		else:
		# 			result = x.session.sync_session()
		# 			print result
		# 			if result == False:
		# 				continue

		# 		# session_id = api.execute_kw(db, uid, pwd, 'ecube.session', 'search',[[['master_id','=',x.session.id],['psc_entity','=',x.psc_entity.master_id]]])


		# 		# check if session not synched with main DB, Synch that first
		# 		# if not session_id:
		# 		# 	x.session.sync_session()

		# 		order_list = []
		# 		for y in x.order_line:
		# 			order_list.append({
		# 				'product_id': int(y.product_id.master_id),
		# 				'product_uom_qty': y.product_uom_qty,
		# 				'price_unit': y.price_unit,
		# 				'drinks_field': y.drinks_field,
		# 				'price_subtotal': y.price_subtotal,
		# 				})



		# 		# rider_master_id = api.execute_kw(db, uid, pwd, 'hr.employee', 'search',[[['id','=',x.rider_id.master_id]]])
		# 		# if rider_master_id:
		# 		# 	master_rider_id = int(rider_master_id[0])
		# 		# else:
		# 		# 	master_rider_id = False


		# 		create_dict = {
		# 			'mobile':x.mobile,
		# 			'payment_type':x.payment_type,
		# 			'amount_total':x.amount_total,
		# 			'cash':x.cash,
		# 			'rider_id': int(x.rider_id.master_id),
		# 			'change':x.change,
		# 			'charge_type':x.charge_type.id,
		# 			'amount_untaxed':x.amount_untaxed,
		# 			'discount_percent':x.discount_percent,
		# 			'discount_amount':x.discount_amount,
		# 			'delivery_charges':x.delivery_charges,
		# 			'amount_tax':x.amount_tax,
		# 			'order_date':x.order_date,
		# 			'order_time':x.order_time,
		# 			'effective_date':x.session.session_date,
		# 			'sequence':x.sequence,
		# 			'seq_id':x.seq_id,
		# 			'entity':x.psc_entity.master_id,
		# 			'session':x.session.id,
		# 			'no_cash':x.no_cash,
		# 			'no_cash_desc':x.no_cash_desc,
		# 			'cust_name':x.cust_name,
		# 			'branch_user':x.branch_user,
		# 			'so_state':x.so_state,
		# 			'so_type':x.so_type,
		# 			'master_id':x.id,
		# 			'order_list':order_list,
		# 		}

		# 		create_list.append(create_dict)


		# if len(temp_list) > 0:
		# 	print "update the status"
		# 	master = 8
		# 	temp = api.execute_kw(db, uid, pwd, 'sale.order', 'get_data', [int(master)],{
		# 		'temp_list':temp_list,
		# 		})

		# if len(create_list) > 0:
			
		# 	print "create the product first time"
			
		# 	master = 8
		# 	print "function call child to main"
		# 	temp = api.execute_kw(db, uid, pwd, 'sale.order', 'c_sync_sos', [int(master)],{
		# 		'create_list':create_list,
		# 		})

		# 	for index in temp:
		# 		so_rec = self.env['sale.order'].search([('id','=',index['child_id'])])
		# 		so_rec.master_id = index['parent_id']
		# 		so_rec.clone_sync_bool = True
		# 		so_rec.parent_db_created = True



	def SyncUnlinkProductProduct(self):
		try:
			for x in self.sync_tree:
				srv, db = x.branch.db_url_ecube, x.branch.db_database
				user , pwd = x.branch.db_user_ecube, x.branch.db_password_ecube
				common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
				common.version()
				uid = common.authenticate(db, user, pwd, {})
				api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)
				master = int(x.master_id)

				ecube_address_result = api.execute_kw(db, uid, pwd, 'sale.order', 'unlink',[[master],{}])

		except Exception, e:
			print "Error...{}".format(e)
			raise ValidationError("Error ...{}".format(e))

	def update_rejected_table_from_maindb(self, data_list):
		for x in data_list:
			so_record = self.env['sale.order'].search([('id','=',int(x['master_id']))])
			if so_record:
				if len(so_record) > 1:
					raise ValidationError("Many So Found.")

				so_record.rej_approval = x['rej_approval']
				so_record.so_state = x['so_state']
				so_record.child_db_order_create()
				return True
			else:
				raise ValidationError("So Not Found.")

		

	def child_db_order_create(self):
		self.new_sequence= self.sequence
		if self.so_state in ['cancel','done']:
			delivery_charges_amount = 0
			if self.so_type == 'cc' or self.so_type == 'foodpanda':
				delivery_charges_amount = self.psc_entity.delivery_charges
			charging_id = 0
			if self.so_type == 'cc':
				charging_rec = self.env['charging.type'].search([('name','=','Self Delivery'),('so_type','=','cc')],limit=1)
				charging_id = charging_rec.id
			if self.so_type == 'foodpanda':
				charging_rec = self.env['charging.type'].search([('name','=','Self Delivery'),('so_type','=','foodpanda')],limit=1)
				charging_id = charging_rec.id
			if self.online_order == False:
				so_find = self.env['sale.order'].search([('so_state','=','draft'),('so_type','=',self.so_type),('psc_entity','=',self.psc_entity.id),('sequence','=',self.sequence)])
				print so_find
				print self.sequence
				
				x = re.search("[a-zA-Z]", self.sequence)
				comp_rec = self.env['res.company'].search([])
				# if comp_rec.test_bool==True:
				# if x:
				# 	print self.sequence
				# else: 
				# 	print inspect.stack()[3][3]
				# 	print inspect.stack()[2][3]
				# 	print inspect.stack()[1][3]
				# 	print inspect.stack()[0][3]


					# if self.so_type=='y':
					# 	if self.sequence== False:
					# 		self.sequence= "T.A-1"
					# 	if self.sequence:
					# 		self.sequence = self.sequence-1

				if not so_find:
					
					print so_find
					print self.sequence
					print self.new_sequence
					self.create({
						'partner_id':self.partner_id.id,
						'pricelist_id':self.pricelist_id.id,
						'so_type':self.so_type,
						'psc_entity':self.psc_entity.id,
						'sequence':self.new_sequence,
						'new_sequence':self.new_sequence,
						'sequence_prv':self.new_sequence,
						'seq_id':self.seq_id,
						'charge_type':charging_id,
						'rider_chk':True,
						'delivery_charges':delivery_charges_amount,
						})

	def online_order_create(self, create_list):
		temp_list = []
		cust_rec = self.env['res.partner'].search([('ref','=','Walkin')])
		cust_id = cust_rec.id
		for x in create_list:
			# session_id = self.env['ecube.session'].search([('master_id','=',x['session']),('psc_entity','=',x['entity'])],limit=1)
			print x['payment_type']
			print "payment type................"
			print x['token']
			print x['code']
			print x['foodpanda_restaurant_id_char']
			print x['login_token']
			so_rec = self.create({
				'token':x['token'],
				'code':x['code'],
				'foodpanda_restaurant_id_char':x['foodpanda_restaurant_id_char'],
				'login_token':x['login_token'],
				'address_line1':x['address_line1'],
				'address_building':x['address_building'],
				'customer_from_address':x['customer_from_address'],
				'delivery_instruct':x['delivery_instruct'],
				'charge_type':x['charge_type'],
				'cust_name':x['cust_name'],
				'mobile':x['mobile'],
				'payment_type':x['payment_type'],
				'charged_to':x['charged_to'],
				'voucher':x['voucher'],
				'amount_total':x['amount_total'],
				'amount_untaxed':x['amount_untaxed'],
				'discount_percent':x['discount_percent'],
				'discount_amount':x['discount_amount'],
				'sequence':x['sequence'],
				'voucher_amount':x['voucher_amount'],
				'online_address':x['online_address'],
				'amount_tax':x['amount_tax'],
				
				'delivery_charges':x['delivery_charges'],
				'amount_tax':x['amount_tax'],
				'order_date':x['order_date'],
				'order_time':x['order_time'],
				'confirmation_date':x['confirmation_date'],
				'call_center_user':x['call_center_user'],
				'sequence':x['sequence'],
				'seq_id':x['seq_id'],
				'color':x['color'],
				'allowed':x['allowed'],
				'online_order':x['online_order'],
				'partner_id':cust_id,
				# 'session':session_id.id,
				'psc_entity':x['psc_entity'],
				'so_state':x['so_state'],
				'so_type':x['so_type'],
				'master_id':x['master_id'],
				# 'order_list'[order_list]
				})
			""" payment type auto select COD idon' Know """
			so_rec.payment_type = x['payment_type']

			order_line_list = []
			for y in x['order_line_list']:
				order_line = {
					'product_id': y['product_id'],
					'product_uom_qty': y['product_uom_qty'],
					'price_unit': y['price_unit'],
					'price_subtotal': y['price_subtotal'],
					'drinks_field': y['drinks_field'],
					'order_id': so_rec.id,
				}
				order_line_list.append(order_line)
			temp_dict = {
			'parent_id':so_rec.master_id,
			'child_id':so_rec.id,
			}
			temp_list.append(temp_dict)
			so_rec.order_line = order_line_list
			so_rec.update_tax()
			for index in so_rec.order_line:
				index.set_many2many_drinks(1000)
		
		self.play_sound()
		return temp_list

# ======== sale.order ENDS ========


# ======== sale.order.line START ========
class SaleOrderLineExt(models.Model):
	_inherit = 'sale.order.line'


	@api.multi 
	def unlink(self):
		if self.kot == True:
			raise ValidationError("Product Connot Be Deleted")
		new_record = super(SaleOrderLineExt,self).unlink()
		return new_record
# ======== sale.order.line ENDS ========



	