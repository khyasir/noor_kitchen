# -*- coding: utf-8 -*-
{
    'name': "Ecube POS Sync",

    'summary': """
        Ecube""",
    'author': "Jaffar Raza",
    'website': "http://www.ecube.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','so_extension','psc_entity','ecube_session','sale','pos_category','create_biometric_users'],

    # always loaded
    'data': [
        'views/pos_sync_view.xml',
    ],

}
