{ 
    'name': 'POS Reports Menu',

    'summary': 'POS Reports Menu', 

    'description': 'POS Reports Menu', 

    'author': 'Rana Rizwan',

    'website': "http://www.bcube.com",

    'depends': ['base'], 

    'application': True, 

    'data': [
    	'views/view.xml',
    	'menu.xml',
    ], 
}