# -*- coding: utf-8 -*- 
from odoo import models, fields, api
from odoo import models, fields
from odoo.exceptions import Warning, ValidationError


class product_extension_for_pos_categ(models.Model): 
	_inherit = 'product.product'

	pos_categ = fields.Many2one('pos.category.ecube',string='POS Category')



class PosCategoryEcube(models.Model): 
	_name = 'pos.category.ecube'

	name = fields.Char(string='Name')
	no = fields.Char(string='Sequence')
	