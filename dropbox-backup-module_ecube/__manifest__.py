# -*- coding: utf-8 -*-
{
    'name': "Manual Database Backup to Dropbox",

    'summary': """
        Database Backup to Dropbox Folder""",

    'description': """
        Backup your database using this module directly to Dropbox.


Main Features
-------------
* Automatically generate database backups.
* Save backup file to Dropbox.
* Automatically clean up old backups.
    """,

    'author': "Hamza Azeem Qureshi ,Enterprise Cube (pvt) ltd",
    'website': "https://ecube.pk",
    'sequence': '10',
    'category': 'Backup',
    'version': '0.1',
 

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'views/dropbox_backup.xml',
    ],

    # only loaded in demonstration mode
    'demo': [

    ],

    'auto_install': False,
    'installable': True,
    # 'images': ['static/description/storage.JPG'],
}
