
from odoo import models, fields,service ,api, _
from odoo.exceptions import Warning
import os.path
import logging
from datetime import timedelta,datetime,date
import xmlrpclib
import dropbox


class Configuration(models.Model):


	_name = 'dropbox.configure'

	name = fields.Char(String='Name', required=True, store=True)
	auth_secret = fields.Char(
		String='User Authentication Secret', required=True)

	destination = fields.Char(String="Destination Location",
							  default="/wapdb", help="this will the Drive bucket folder location")


	@api.model
	def submit_values(self):
		recs = self.env['dropbox.configure'].search([])
		for x in recs:
			response=x.upload_files()
			print response
			if response == True:
				self.create_entity_status()


	def create_entity_status(self):
		try:

			comp_rec = self.env['res.company'].search([])
			srv, db = comp_rec[0].db_url_ecube, comp_rec[0].db_database
			user , pwd = comp_rec[0].db_user_ecube, comp_rec[0].db_password_ecube
			common =  xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % srv)
			common.version()
			uid = common.authenticate(db, user, pwd, {})
			api = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % srv)

			entity = self.env['psc.entity'].search([],limit=1)
			date_time = datetime.today()
			get_date = str(date_time).split(' ')
			date = get_date[0]
			backup_status = []
			date_time = str(date_time)
			backup_status.append({
					'date': get_date[0],
					'date_time': date_time,
					'psc_entity': entity.master_id,
					})

			if len(backup_status) > 0:

				master = 8
				temp = api.execute_kw(db, uid, pwd, 'backup.status', 'entity_backup_status_crearte', [int(master)],{
					'backup_status':backup_status,
					})
		except Exception, e:
			print "Connection Error...{}".format(e)
			return False
	

	@api.multi
	def upload_files(self):

		dbx = dropbox.Dropbox(self.auth_secret)
		path, content = self.get_path_and_content()
		try:
			dbx.files_upload(content.read(), path)

		except Exception as e:
			logging.error(e)
			return False
		return True
				
	
	def get_path_and_content(self):
		"""
		Get the Path with Filename and the DB-backup-content
		:return: tuple(path: string, content: binary)
		"""
		filename, content = self.get_backup()
		path = os.path.join(self.destination, filename)
		return path, content

	def get_backup(self, dbname=None, backup_format='zip'):
		"""
		Get backup with content
		:param dbname: string
		:param backup_format: string
		:return: tuple(filename: string, dump_data: binary)
		"""
		if dbname is None:
			dbname = self._cr.dbname
		ts = datetime.utcnow().strftime("%Y-%m-%d_%H-%M-%S")
		filename = "%s_%s.%s" % (dbname, ts, backup_format)
		dump_stream = service.db.dump_db(dbname, None, backup_format)
		return filename, dump_stream
# pip install dropbox