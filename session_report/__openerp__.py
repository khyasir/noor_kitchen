# -*- coding: utf-8 -*-
{
    'name': "Session Report",

    'summary': "Session Report",

    'description': "Session Report",

    'author': "Yasir Iqbal",
    'website': "http://www.ecube.pk",

    # any module necessary for this one to work correctly
    'depends': ['base'],
    # always loaded
    'data': [
        'template.xml',
        'views/module_report.xml',
    ],
    'css': ['static/src/css/report.css'],
}
