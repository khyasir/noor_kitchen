# -*- coding: utf-8 -*-

from odoo import models, fields, api
from openerp.exceptions import ValidationError

class data_handling(models.Model):
    _name = 'data.handling'
    _rec_name = 'from_date'

    dec_delete = fields.Char(string="Description",required=True)
    dec_delete_dummy = fields.Char(string="Description")
    from_date = fields.Date(string="From Date")
    to_date = fields.Date(string="To Date")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('deleted', 'Deleted'),
    ],copy=False,default="draft")

    @api.multi
    def del_so(self):
        if self.state == "draft":
            if self.from_date and self.to_date:


                sessions = self.env['ecube.session'].search([('session_date', '>=', self.from_date), ('session_date', '<=', self.to_date), ('status', '=', 'sync'), ('state', '=', 'closed'), ('master_id', '!=', False)])

                for session in sessions:

                    del_sale_order = self.env.cr.execute("DELETE FROM sale_order WHERE session = %s", (session.id,))
                    del_session = self.env.cr.execute("DELETE FROM ecube_session WHERE id = %s", (session.id,))
                self.state = "deleted"
            else:
                raise ValidationError('Please add date first.')


    @api.multi
    def del_so_draft(self):
        if self.state == "draft":
            sessions = self.env['ecube.session'].search([('state', '=', 'draft')])
            for session in sessions:
                del_sale_order_draft = self.env.cr.execute("DELETE FROM sale_order WHERE session = %s", (session.id,))
                del_session_draft = self.env.cr.execute("DELETE FROM ecube_session WHERE id = %s", (session.id,))
            self.state = "deleted"

    @api.multi
    def unlink(self):
        if self.state == 'deleted':
            raise ValidationError('You cannot delete the record.')
        super(data_handling, self).unlink()
        return True







