# -*- coding: utf-8 -*- 
from odoo import models, fields, api
from openerp.exceptions import Warning, ValidationError, UserError


class struct_user_extend(models.Model):
	_inherit  = 'res.users'

	psc_entity = fields.Many2one ('psc.entity',string="Entity")
	stock_location = fields.Many2one('stock.location', string='Location',required=False)
	pass_id = fields.Char(string='Password', default='')
	


class branchAAA(models.Model):
	_name = 'psc.entity'

	name = fields.Char(string="Name",required=True)
	phone = fields.Char(string="Phone")
	address = fields.Char(string="Address")
	mobile = fields.Char(string="Rider payment per delivery")
	code = fields.Char(string="ST # No")
	branch_code = fields.Integer(string="Entity")
	warehouse = fields.Many2one('stock.warehouse',string='Warehouse')
	stock_location = fields.Many2one('stock.location', string='Location',required=False)
	internal_location = fields.Many2one('stock.location', string='Internal Location',required=False)
	demand_received_location = fields.Many2one('stock.location', string='Demand Received Location',required=False)
	picking_type = fields.Many2one('stock.picking.type', string='Picking type',required=False)
	picking_type_po = fields.Many2one('stock.picking.type', string='Picking type PO',required=False)
	brand = fields.Many2one('ecube.brand', string='Brand',required=False)
	city = fields.Many2one('pak.city', string='City')
	entity_sequence  = fields.Many2one('ir.sequence')
	machine_ip = fields.Char(string="IP")
	delivery_charges = fields.Float(string="Delivery Charges")
	manual_allow = fields.Boolean(string="Manual Allow")
	gst = fields.Boolean(string="Gst")
	# pos_journal_id = fields.Many2one('account.journal',string="journal Id")
	# stock_cr = fields.Many2one('account.account',string="Stock")
	# Expense_db = fields.Many2one('account.account',string="Expense")
	entity_rec = fields.Many2one('account.account',string="Entity Receivable")
	entity_pay = fields.Many2one('account.account',string="Entity Payable")
	entity_cash = fields.Many2one('account.account',string="Entity Cash")
	entity_brand_whata = fields.Many2one('account.account',string="Whata Brand Account")

	partner_id = fields.Many2one('res.partner', string='Partner ID')

	@api.model
	def create(self, vals):
		new_record = super(branchAAA, self).create(vals)
		if new_record.manual_allow == False:
			pass
			# raise  ValidationError('Cannot Create Record Please Contact Your Admin.')
		else:
			res_partner = self.env['res.partner']
			create_partner = res_partner.create({
			'name': new_record.name,
			'fucntion':new_record.id,
			})
			new_record.partner_id = create_partner.id
			new_record.get_account_partner()

		return new_record


	@api.onchange('partner_id')
	def get_account_partner(self):
		if self.partner_id:
			self.entity_rec = self.partner_id.property_account_receivable_id.id
			self.entity_pay = self.partner_id.property_account_payable_id.id
		else:
			self.entity_rec = None
			self.entity_pay = None


	@api.multi
	def write(self, vals):
		before=self.write_date
		super(branchAAA, self).write(vals)
		after = self.write_date
		if before != after:
			if self.partner_id:
				self.partner_id.name = self.name

		return True

	@api.multi
	def unlink(self):
		for x in self:
			if x.manual_allow == False:
				raise  ValidationError('Cannot Delete Record Please Contact Your Admin.')
	
		return super(branchAAA,self).unlink()

class Ecubebrand(models.Model):
    _name = 'ecube.brand'

    name = fields.Char()

class EcubeCity(models.Model):
    _name = 'pak.city'

    name = fields.Char()
